package gui;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import networkOverlay.*;
import java.util.*;

public class ChordGUI extends JFrame implements WindowListener{
	public static final long serialVersionUID = 1;
	
	public ChordGUI gui = this;
	
	//menu bar stuff
	private JMenuBar menuBar = new JMenuBar();
	private JMenu fileMenu = new JMenu("File");
	private JMenuItem shareFileItem = new JMenuItem("Publish");
	private JMenuItem retractFileItem = new JMenuItem("Retract");
	private JMenuItem associateKeywordItem = new JMenuItem("Associate");
	private JMenuItem unassociateKeywordItem = new JMenuItem("Unassociate");
	private JMenuItem lookupKeywordItem = new JMenuItem("Look up keyword");
	private JMenuItem exitItem = new JMenuItem("Exit");
	
	private JMenu helpMenu = new JMenu("Help");
	private JMenuItem helpItem = new JMenuItem("Help");
	private JMenuItem aboutItem = new JMenuItem("About");
	
	//buttons on the left
	private JPanel buttonPanel = new JPanel();
	private JPanel buttonPanelGrid = new JPanel();
	private JButton shareButton = new JButton("Publish");
	private JButton retractButton = new JButton("Retract");
	private JButton lookUpButton = new JButton("Look up");
	//exists separately for each entry
	private JButton associateButton = new JButton("Associate");
	private JButton unassociateButton = new JButton("Unassociate");
	private JButton lookUpKeywordButton = new JButton("Look up keyword");
	private JButton printButton = new JButton("Print");
	
	//upload/download list stuff
	private JTabbedPane fileTabbedPane = new JTabbedPane();
	private JScrollPane shareScrollPane;
	private JPanel sharePanel = new JPanel();
	private DownloadPanel downloadPanel = new DownloadPanel(this);
	private JScrollPane downloadScrollPane;	
	private JList shareList = new JList();
	private Vector<FileInfo> shareListData = new Vector<FileInfo>();
	
	//to interact with network and files
	private JFileChooser shareFileChooser = new JFileChooser();
//	private JFileChooser downloadFileChooser = new JFileChooser();
	private JFileChooser keywordFileChooser = new JFileChooser();
	private JFileChooser retractFileChooser = new JFileChooser();
	private Services services;
	
	public ChordGUI(Services services){
		//default stuff
		super("Chord Network GUI");
		setSize(500, 350);
		setLocationRelativeTo(null);//centers window
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
		this.services = services;
		
		//set up all the components
		//I split this into separate methods for readability
		setupMenuBar();
		setupFilePanels();
		setupButtonPanel();
		
		this.addWindowListener(this);
		
		setVisible(true);
	}
	
	private void setupMenuBar(){
		//setup what the menu items do
		shareFileItem.addActionListener(new PublishListener());
		retractFileItem.addActionListener(new RetractListener());
		associateKeywordItem.addActionListener(new AssociateListener());
		unassociateKeywordItem.addActionListener(new UnassociateListener());
		lookupKeywordItem.addActionListener(new LookUpKeywordListener());
		exitItem.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				System.exit(0);
			}
		});
		helpItem.addActionListener(new HelpListener());
//		helpItem.setEnabled(false);
		aboutItem.addActionListener(new AboutListener());
//		aboutItem.setEnabled(false);
		
		//add everything
		fileMenu.add(shareFileItem);
		fileMenu.add(retractFileItem);
		fileMenu.add(new JSeparator());
		fileMenu.add(associateKeywordItem);
		fileMenu.add(unassociateKeywordItem);
		fileMenu.add(lookupKeywordItem);
		fileMenu.add(new JSeparator());
		fileMenu.add(exitItem);
		helpMenu.add(helpItem);
		helpMenu.add(new JSeparator());
		helpMenu.add(aboutItem);
		menuBar.add(fileMenu);
		menuBar.add(helpMenu);
		setJMenuBar(menuBar);
	}
	
	private void setupFilePanels(){
		sharePanel.setLayout(new BorderLayout());
		sharePanel.add(shareList, BorderLayout.CENTER);
		shareScrollPane = new JScrollPane(sharePanel,
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
//		downloadPanel.add(downloadTable);
		
		downloadScrollPane = new JScrollPane(downloadPanel,
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		fileTabbedPane.addTab("Share File", shareScrollPane);
		fileTabbedPane.addTab("Download File", downloadScrollPane);
//		fileTabbedPane.addTab("Messages Received", messageScrollPane);
		add(fileTabbedPane, BorderLayout.CENTER);
		
		//I wanted to update the list contents last, since in the actual
		//GUI it may take a while to do this over the network and I would
		//like to maybe make a separate thread if I need to.
		updateShareList();
//		updateDownloadList();
	}
	
	private void updateShareList(){
		//do we want to keep track of user's preferences for share files?
//		shareList.setListData(client2.getLocalFiles());
		Vector<String> newListData = new Vector<String>();
		for(FileInfo fileInfo : shareListData){
			newListData.add(fileInfo.getName());
		}
		shareList.setListData(newListData);
	}
//	
//	private void updateDownloadList(){
//		//it is possible this may take a while, maybe need a new thread
//		//or we may want to cut down the number if there are, say, 10,000 files
//		
//		//actually we will do nothing for now
////		downloadList.setListData(client2.getForeignFiles());
//	}
	
	private void setupButtonPanel(){
		shareButton.addActionListener(new PublishListener());
		retractButton.addActionListener(new RetractListener());
//		lookUpButton.addActionListener(new LookupListener());
//		lookUpButton.setEnabled(false);
		associateButton.addActionListener(new AssociateListener());
		unassociateButton.addActionListener(new UnassociateListener());
		lookUpKeywordButton.addActionListener(new LookUpKeywordListener());
		
		printButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				services.getNetworkController().getFingerTable2().printFingerTable();
			}
		});
		
		buttonPanelGrid.setLayout(new GridLayout(7,1));
		buttonPanelGrid.add(shareButton);
		buttonPanelGrid.add(retractButton);
		buttonPanelGrid.add(associateButton);
		buttonPanelGrid.add(unassociateButton);
		buttonPanelGrid.add(lookUpKeywordButton);
		
		buttonPanel.add(buttonPanelGrid);
		add(buttonPanel, BorderLayout.WEST);
	}
	
	/* ***************** WindowListener methods ****************/
	//this is the only one we care about
	//it happens when user is about to end program
	//it makes sure we end all the threads
	public void windowClosing(WindowEvent e){
		try{
			setVisible(false);
			services.initiateExitSequence();
			//JOptionPane.showMessageDialog(null, "Exiting...");
		}
		finally{
			System.exit(0);
		}
	}

	public void windowActivated(WindowEvent e){}
	public void windowClosed(WindowEvent e){}
	public void windowDeactivated(WindowEvent e){}
	public void windowDeiconified(WindowEvent e){}
	public void windowIconified(WindowEvent e){}
	public void windowOpened(WindowEvent e){}
	
	public Services getServices(){return services;}
	public JTabbedPane getFileTabbedPane(){return fileTabbedPane;}
	
	
	
	/* *************************************************************
	 * Here I put all the inner classes to handle buttons and such
	 */
	
	/**
	 * PublishListener
	 * listener for publishing files
	 */
	private class PublishListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			
			int returnValue = shareFileChooser.showOpenDialog(downloadScrollPane);
			if(returnValue != JFileChooser.APPROVE_OPTION) return;
			
			
			//start up thread to call method in services
			PublishThread publishThread = new PublishThread(shareFileChooser.getSelectedFile());
			publishThread.execute();
		}
		
		/**this class is for handling publishing interaction with Services*/
		private class PublishThread extends SwingWorker<Void, Void>{
			private File file = null;
			private FileInfo fileInfo = null;
			
			public PublishThread(File file){this.file = file;}

			@Override
			public Void doInBackground(){
				//necessary to make this thread to call this method since it might take a while
				if (file.exists()) 
					fileInfo = services.publishFile(file);
				else{
					JOptionPane.showMessageDialog(null, "The file you tried to publish does not exist", "Publish Error",0);
				}
				return null;
			}
			
			@Override
			//this is called on the AWT event thread
			public void done(){
				if(fileInfo != null){
					//add to GUI view					
					shareListData.add(fileInfo);
					updateShareList();
					//make it select the index of the shared file panel in the tabbed pane
					fileTabbedPane.setSelectedIndex(0);
				}
			}
		}
	}
	
	/**
	 * RetractListener
	 * for retracting files we have published
	 */
	public class RetractListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			int selectedIndex = shareList.getSelectedIndex();
			FileInfo fileInfo;
			if(selectedIndex != -1){
				fileInfo = shareListData.remove(selectedIndex);
				updateShareList();
				
				//perform actions in separate thread in case services is slow
				RetractThread retractThread = new RetractThread(fileInfo);
				retractThread.execute();
			}
			else{//otherwise ask the user for a file to retract
				int returnValue = retractFileChooser.showOpenDialog(gui);
				if(returnValue == JFileChooser.APPROVE_OPTION){
					System.out.println(retractFileChooser.getSelectedFile());
					File file = retractFileChooser.getSelectedFile();
					
					if (file.exists()){
						//perform actions in separate thread in case services is slow
						RetractThread retractThread = new RetractThread(new FileInfo(file));
						retractThread.execute();
					}
					else{
						JOptionPane.showMessageDialog(null, "The file you tried to retract does not exist", "Retract Error",0);
					}
					
				}
			}
		}
		
		private class RetractThread extends SwingWorker<Void, Integer>{
			private FileInfo fileInfo = null;
			
			public RetractThread(FileInfo fileInfo){this.fileInfo = fileInfo;}
			
			public Void doInBackground(){
				services.retractFile(fileInfo.getID());
				return null;
			}
			@Override
			public void done(){
				fileTabbedPane.setSelectedIndex(0);				
			}
		}
	}
	
	public class AssociateListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			System.out.println("AssociateListener");			

			//perform actions in separate thread in case services is slow
			SwingWorker<Void, Void> associateKeywordThread = 
					new SwingWorker<Void, Void>(){
				public Void doInBackground(){		//first get the keyword
					String keyword = JOptionPane.showInputDialog("What is the keyword to associate?");
					
					//null means user hit "cancel", make sure it's not empty too
					if(keyword != null && !keyword.trim().isEmpty()){
						keyword = keyword.trim();
						//now get the file
						int returnValue = keywordFileChooser.showOpenDialog(gui);
						if(returnValue == JFileChooser.APPROVE_OPTION){
							File file = keywordFileChooser.getSelectedFile();
							if (file.exists())
								services.associateKeyword(keyword, file);
							else
								JOptionPane.showMessageDialog(null, "The file with which you are trying to associate the keyword does not exist", "Keyword Association Error",0);
						}
					}
					return null;
				}
			};
			associateKeywordThread.execute();
		}
	}
	
	public class UnassociateListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			System.out.println("UnassociateListener");			

			//perform actions in separate thread in case services is slow
			SwingWorker<Void, Void> unassociateKeywordThread = 
					new SwingWorker<Void, Void>(){
				public Void doInBackground(){
					//first get the keyword
					String keyword = JOptionPane.showInputDialog("What is the keyword to unassociate?");

					//null means user hit "cancel", make sure it's not empty too
					if(keyword != null && !keyword.trim().isEmpty()){
						//now get the file
						int returnValue = keywordFileChooser.showOpenDialog(gui);
						if(returnValue == JFileChooser.APPROVE_OPTION){
							File file = keywordFileChooser.getSelectedFile();
							if (file.exists())
								services.unassociateKeyword(keyword, file);
							else
								JOptionPane.showMessageDialog(null, "The file with which you are trying to unassociate the keyword does not exist", "Keyword Unassociation Error",0);
						}
					}
					return null;
				}
			};
			unassociateKeywordThread.execute();
		}
	}
	
	public class LookUpKeywordListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			System.out.println("LookUpKeywordListener");
			
			//perform actions in separate thread in case services is slow
			SwingWorker<Void, FileInfo> lookupKeywordThread = 
					new SwingWorker<Void, FileInfo>(){
				public Void doInBackground(){
					//first get the keyword
					String keyword = JOptionPane.showInputDialog("What is the keyword to look up?");
					
					if(keyword != null && !keyword.trim().isEmpty()){//null means user canceled the input
						keyword = keyword.trim();
						
					
						
						//now look up the files in network
						Vector<FileInfo> fileInfos = services.lookUpKeyword(keyword);
						System.out.println("GUI: finished looking up");
						
						if(fileInfos != null && fileInfos.size() > 0){//as long as there was something for the keyword
							//convert to something readable
							Vector<String> fileNames = new Vector<String>();
							for(int i = 0; i < fileInfos.size(); i++){
								fileNames.add(fileInfos.get(i).getName());
							}
			//				JOptionPane.showMessageDialog(fileTabbedPane, new JList(fileNames));
							JList fileKeywordList = new JList(fileNames);
							fileKeywordList.setSelectedIndex(0);//make it select first one by default
							fileKeywordList.setBorder(BorderFactory.createLineBorder(Color.black));
							JPanel fileKeywordPanel = new JPanel();
							fileKeywordPanel.setLayout(new BorderLayout());
							fileKeywordPanel.add(fileKeywordList, BorderLayout.CENTER);
							fileKeywordPanel.add(new JLabel("Select a file with keyword \"" + keyword + "\":"),
									BorderLayout.NORTH);
							
							int option = JOptionPane.showOptionDialog(null, fileKeywordPanel, 
									"Files with keyword", JOptionPane.OK_CANCEL_OPTION,
									JOptionPane.QUESTION_MESSAGE, null, null, 0);
							
							//as long as user hits "OK" and has selected something
							if(option == 0 && !fileKeywordList.isSelectionEmpty()){
								//now send in the FileInfo corresponding to user's choice
//								lookUpFile(fileInfos.get(fileKeywordList.getSelectedIndex()));
								
								//do this on the event dispatch queue
								publish(fileInfos.get(fileKeywordList.getSelectedIndex()));
							}
						}
						else{
							JOptionPane.showMessageDialog(gui, new JLabel("No matches found"));
						}
					}
					return null;
				}
				
				@Override
				public void process(java.util.List<FileInfo> chunks){
					FileInfo fileInfo = chunks.get(chunks.size()-1);//the last one

					boolean lookedUp = services.lookupFile(fileInfo.getID(), fileInfo.getName());
					if(lookedUp){
						//add the row and user can download whenever he wants (it will automatically look up)
						downloadPanel.addRow(fileInfo);
						fileTabbedPane.setSelectedIndex(1);
					}
					else{
						JOptionPane.showMessageDialog(gui, "Could not look up file.");
					}
				}
			};
			

			lookupKeywordThread.execute();
		}
		
	}
	
	public class HelpListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			//send off to the help window
			new HelpWindow(gui);
		}
	}
	
	public class AboutListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			JOptionPane.showMessageDialog(gui, 
					"CS Final Project\n\n" +
					"Farooq Kassam\n" +
					"Shahmir Noorani\n" +
					"Andrew Seguin\n" +
					"Charles Spencer",
					"About",
					JOptionPane.INFORMATION_MESSAGE
					);
		}
	}
}
