package gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import networkOverlay.*;
import java.util.*;
import java.net.*;
import javax.swing.text.html.*;
import org.w3c.dom.html.HTMLTitleElement;


public class HelpWindow extends JFrame{
	public static final long serialVersionUID = 1;
	
	private ChordGUI gui;
	
	private URL helpLocation;
	private String helpLocationString = "usermanual.html";
	
	private JScrollPane helpScrollPane = new JScrollPane();
	private JTextPane helpTextPane = new JTextPane();
	
	private HTMLDocument htmlDocument;
	
	public HelpWindow(ChordGUI gui){
		this.gui = gui;
		try{
			File file = new File(helpLocationString);
//			System.out.println(file.exists());
//			helpLocation = new URL("file:///" + file.getAbsolutePath());
			helpLocation = file.toURI().toURL();
//			htmlDocument = new HTMLDocument();
//			htmlDocument.setBase(helpLocation);

			helpTextPane.setPage(helpLocation);
//			helpTextPane.getStyledDocument().
//			helpTextPane.setDocument(htmlDocument);
//			htmlDocument = (HTMLDocument)helpTextPane.getDocument();
//			HTMLDocument.Iterator iter = htmlDocument.getIterator(HTML.Tag.TITLE);
//			iter.next();
			this.setTitle("User Manual");
//			this.setTitle(htmlDocument.TitleProperty);
			helpTextPane.setEditable(false);
			
			helpScrollPane.getViewport().add(helpTextPane);
			add(helpScrollPane);
			pack();
			setSize(800,600);
			setLocationRelativeTo(null);//centers window
			
			setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
			setVisible(true);
		}
//		catch(MalformedURLException ex){
//			
//		}
		catch(IOException ex){
			ex.printStackTrace();
			//display message if not working
			JOptionPane.showMessageDialog(gui, 
				"An error has occurred displaying the user manual.");
		}
	}
}
