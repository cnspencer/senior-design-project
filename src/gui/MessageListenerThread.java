////used from the Sun website
//
//package gui;
//
//import javax.swing.*;
//import java.util.*;
//
///*
// * MessageListenerThread
// * this class is responsible for listening to see if a message
// * has been received from another peer
// */
//
//public class MessageListenerThread extends SwingWorker<Void, String>{
//	ChordGUI gui;
//	
//	public MessageListenerThread(){}
//	
//	public MessageListenerThread(ChordGUI gui){
//		this.gui = gui;
//	}
//	
//	//this thread is meant to run for a long time, until program terminates
//	//operations for this thread happen in this method
//	@Override
//	protected Void doInBackground(){
//		while(!isCancelled()){
//			//continually waits for messages to process
//			this.publish(gui.getServices().waitToDisplayMessage());
//		}
//		return null;
//	}
//	
//	
//	@Override
//	//this will update the GUI to show any new messages
//	protected void process(List<String> messages){
//		for(int i = 0; i < messages.size(); i++)
//			//send it off to the GUI
//			gui.showMessage(messages.get(i));
//	}
//	
//	public void setGUI(ChordGUI gui){ this.gui = gui; }
//}
