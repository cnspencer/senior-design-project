package gui;

import javax.swing.*;
import javax.swing.table.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import networkOverlay.*;
import java.util.*;

//this incapsulates the panel that holds the list of downloads

public class DownloadPanel extends JPanel implements ListSelectionListener, MouseListener{
	public static final long serialVersionUID = 1;
	
	private ChordGUI gui;
	
	private String[] columnNames = {"File", "Status", "Button"};
	private Object[][] tableData = new Object[0][3];
	
	private ArrayList<Row> rows = new ArrayList<Row>();
	
	private DefaultTableModel tableModel = null;
	private JTable downloadTable;
	private ButtonGroup buttons = new ButtonGroup();
	
	private ListSelectionModel listSelectionModel = new DefaultListSelectionModel();
	
	public DownloadPanel(ChordGUI gui){
		this.gui = gui;
		setLayout(new BorderLayout());
		
		tableModel = new DefaultTableModel(tableData, columnNames){
			public static final long serialVersionUID = 1;
			public boolean isCellEditable(int row, int col){return col == 2;}//the column of the button is editable
		};
				
		downloadTable = new JTable(tableModel);
		downloadTable.setSelectionModel(listSelectionModel);
 //   	downloadTable.setSelectionModel(new DefaultListSelectionModel());
    	downloadTable.setSelectionBackground(Color.BLUE);
    	downloadTable.setSelectionBackground(Color.WHITE);
    	
 //   	downloadTable.setShowGrid(false);
 //   	downloadTable.setRowSelectionAllowed(true);
 //   	downloadTable.setColumnSelectionAllowed(false);
    	
    	downloadTable.addMouseListener(this);
		downloadTable.getSelectionModel().addListSelectionListener(this);
		
//		downloadTable.setBackground(Color.RED);
//		downloadTable.setSelectionBackground(Color.GREEN);
//		downloadTable.setSelectionForeground(Color.WHITE);
    	
		setup();
	}
	
    private void setup(){
//    	downloadTable.getSelectionModel().removeListSelectionListener(this);
//		downloadTable.getSelectionModel().addListSelectionListener(this);
    	downloadTable.setRowSelectionAllowed(true);
    	downloadTable.setColumnSelectionAllowed(false);
    	
    	downloadTable.getColumn("File").setCellRenderer(new DownloadPanelRenderer()); 
    	downloadTable.getColumn("Status").setCellRenderer(new DownloadPanelRenderer()); 
		downloadTable.getColumn("Button").setCellRenderer(new DownloadPanelRenderer());

    	DownloadButtonEditor dbe = new DownloadButtonEditor();
    	downloadTable.getColumn("Button").setCellEditor(dbe); 

    	add(downloadTable); 
    }
	
	public void addRow(FileInfo fileInfo){
		//arrange the data in the table and on the Row
		Object[] rowData = new Object[3];
		JLabel name = new JLabel(fileInfo.getName());
		JLabel status = new JLabel("ready");
		JButton button = new JButton("Download");
		buttons.add(button);
		
		rowData[0] = name;
		rowData[1] = status;
		rowData[2] = button;		
		Row row = new Row(name, status, button);
		
		//setup the DownloadListenerThread
		DownloadListenerThread dlt = new DownloadListenerThread(gui.getServices(), row, this,
				fileInfo);
		row.addDownloadListenerThread(dlt);
		
		button.addActionListener(row);
		
		//also begin the downloadlistenerthread
		dlt.execute();
		
		//do not add the row until the download listener thread succeedes in the lookup
		rows.add(row);
		tableModel.addRow(rowData);
		
		//for now we will begin the lookup immediately
		dlt.beginLookup();
		downloadTable.changeSelection(rows.size() - 1, 0, true, false);
		downloadTable.repaint();
	}
	
	//returns true if lookup begun, false if not begun (e.g. if row not added)
//	public boolean beginLookup(String fileName){
//		if(rows.contains(fileName)){
//			Row row = rows.get(rows.indexOf(fileName));
//			row.
//		}
//		else return false;
//	}
	
	public void removeDownload(String fileName){
		//TODO we hope that the download thread is not still running (for now)
		
		//this is kind convoluted, but its the only way I could find to get the data
		Vector<Vector<Object>> data = tableModel.getDataVector();
		for(int row = 0; row < data.size(); row++){
			if(data.get(row).get(0) != null){
				String rowName = ((JLabel)data.get(row).get(0)).getText();
				if(rowName.equals(fileName)){
					//here we have to basically reload the whole table 
					//but I was having problems just deleting 1 row 
					tableModel.getDataVector().remove(row); 
					Vector<String> names = new Vector<String>(); 
					for(int i = 0; i < columnNames.length; i++){ 
						names.add(columnNames[i]); 
					} 	
					tableModel.setDataVector(tableModel.getDataVector(), names); 
					setup();
					rows.remove(row);
					repaint();
				}					
			}
		}
	}
	
	///////////for ListSelectionListener//////////////////////////////
	@Override
	public void valueChanged(ListSelectionEvent e){
		System.out.println("list selection changed!!! at row " + e.getFirstIndex());
		System.out.println(e);
		
		
		//when it finishes adjusting
		if(!e.getValueIsAdjusting()){
			
			
//			int rowIndex = e.getFirstIndex();
//			Row row = rows.get(rowIndex);			
//			
//			row.getFileName().setBackground(Color.BLUE);
//			row.getStatus().setBackground(Color.BLUE);
//			row.getButton().setBackground(Color.BLUE);
//			
//			row.getFileName().setForeground(Color.WHITE);
//			row.getStatus().setForeground(Color.WHITE);
//			row.getButton().setForeground(Color.WHITE);
//			
//			for(int i = 0; i < e.getFirstIndex(); i++){
//				int rowIndex = e.getFirstIndex();
//				Row row = rows.get(rowIndex);
//				
//				row.getFileName().setBackground(Color.GRAY);
//				row.getStatus().setBackground(Color.GRAY);
//				row.getButton().setBackground(Color.GRAY);
//				
//				row.getFileName().setForeground(Color.BLACK);
//				row.getStatus().setForeground(Color.BLACK);
//				row.getButton().setForeground(Color.BLACK);
//			}
//			
//			for(int i = e.getFirstIndex(); i <= e.getLastIndex(); i++){
//				int rowIndex = e.getFirstIndex();
//				Row row = rows.get(rowIndex);
//				
//				row.getFileName().setBackground(Color.BLUE);
//				row.getStatus().setBackground(Color.BLUE);
//				row.getButton().setBackground(Color.BLUE);
//				
//				row.getFileName().setForeground(Color.WHITE);
//				row.getStatus().setForeground(Color.WHITE);
//				row.getButton().setForeground(Color.WHITE);
//			}
//			for(int i = e.getLastIndex() + 1; i < rows.size(); i++){
//				int rowIndex = e.getFirstIndex();
//				Row row = rows.get(rowIndex);
//				
//				row.getFileName().setBackground(Color.GRAY);
//				row.getStatus().setBackground(Color.GRAY);
//				row.getButton().setBackground(Color.GRAY);
//				
//				row.getFileName().setForeground(Color.BLACK);
//				row.getStatus().setForeground(Color.BLACK);
//				row.getButton().setForeground(Color.BLACK);
//			}
		}
		
		downloadTable.repaint();
	}
	
	/////////////for MouseListener//////////////////////////////////////
	////we can't have mouse events occur on Components in the table directly,
	//so we have to forward them through mouseClicked here, the listener for the JTable

	public void mouseClicked(MouseEvent e){
		int row = downloadTable.rowAtPoint(e.getPoint());
		int col = downloadTable.columnAtPoint(e.getPoint());
//		System.out.println("mouse clicked! for downloadTable, row " + row);
		
		//-1 indicates that event fell on table but not on a particular row
		if(row != -1){
			downloadTable.getSelectionModel().setSelectionInterval(row, row);
			System.out.println(downloadTable.getSelectionModel() + " " +
					downloadTable.getSelectionModel().getMinSelectionIndex());
			
			
			Object obj = downloadTable.getValueAt(row, col);
			Component component = (Component)obj;
			component.dispatchEvent(e);
		}
	}
	public void mouseEntered(MouseEvent e){}
	public void mouseExited(MouseEvent e){}
	public void mousePressed(MouseEvent e){}
	public void mouseReleased(MouseEvent e){}		
	
	/************************************************************************
	 ******************* INNER CLASSES **************************************
	 ************************************************************************/
	
	//get and set methods
	public DefaultTableModel getTableModel(){return tableModel;}
	public ChordGUI getGUI(){return gui;}
	
	//to make sure we can render correctly and any item we have
	private class DownloadPanelRenderer implements TableCellRenderer{
		public Component getTableCellRendererComponent(JTable table, Object value,
                  boolean isSelected, boolean hasFocus, int row, int column) {
			if (value==null){
				return null;
			}		
			return (Component)value;
		}
	}
	
	//to make sure we can render correctly
	private class DownloadButtonEditor extends AbstractCellEditor
			implements TableCellEditor, ItemListener{		
		public static final long serialVersionUID = 1;
		
		private JButton button;
		
		
		//**** from TableCellEditor ****//
		@Override
		public Component getTableCellEditorComponent(JTable table, Object value,
				boolean isSelected, int row, int column) {
			if (value==null) return null;
			button = (JButton)value;
//			button.addItemListener(this);
			return (Component)value;
		}  
		
		
		//***** from AbstractCellEditor ****//

		@Override
		public Object getCellEditorValue() {
//			button.removeItemListener(this);
			return button;
		}
		

		//**** for ItemListener ****
		
		public void itemStateChanged(ItemEvent e){
			super.fireEditingStopped();			
		}
	}//end class DownloadButtonEditor
	
	public class Row implements ActionListener, Comparable<Row>, MouseListener{
		private JLabel fileName;
		private JLabel status;
		private JButton button;
		
		private DownloadListenerThread dlt = null;
		
		public Row(JLabel fileName, JLabel status, JButton button){
			this.fileName = fileName;
			this.status = status;
			this.button = button;
			
			fileName.addMouseListener(this);
			status.addMouseListener(this);
			button.addMouseListener(this);
		}
		
		public void actionPerformed(ActionEvent e){
			String command = e.getActionCommand();
			if(command.equals("Look up")){
				dlt.beginLookup();
			}
			else if(command.equals("Download")){
				if(dlt != null){
//					status.setText("downloading...");
//					button.setText("Cancel");
					JFileChooser fileChooser = new JFileChooser();
					int option = fileChooser.showSaveDialog(null);
					if(option == JFileChooser.APPROVE_OPTION){
						File path = fileChooser.getSelectedFile();
						dlt.beginDownload(path);
//						dlt.execute();
						downloadTable.repaint();
					}
				}
			}
			else if(command.equals("Cancel")){
				//TODO
			}
			else if(command.equals("Close")){
				button.removeActionListener(this);
				removeDownload(fileName.getText());
			}
		}

		public JLabel getFileName() {return fileName;}
		public void setFileName(JLabel fileName) {this.fileName = fileName;}
		public JLabel getStatus() {return status;}
		public void setStatus(JLabel status) {this.status = status;}
		public JButton getButton() {return button;}
		public void setButton(JButton button) {this.button = button;}
		public DownloadListenerThread getDownloadListenerThread(){return dlt;}
		
		public void addDownloadListenerThread(DownloadListenerThread dlt){
			this.dlt = dlt;
		}
		@Override
		public int compareTo(Row obj){
//			if(obj instanceof String){
//				String otherString = (String)obj;
//				return fileName.getText().compareTo(otherString);
//			}
			if(obj instanceof Row){
				Row otherRow = (Row)obj;
				return fileName.getText().compareTo(otherRow.fileName.getText());
			}
			else{
				return 55;
			}
		}
		
		@Override		
		public boolean equals(Object obj){
			if(obj == null || !(obj instanceof Row || obj instanceof String)) return false;
			if(obj instanceof String){
				String otherString = (String)obj;
				return this.fileName.getText().equals(otherString);
			}
			Row otherRow = (Row)obj;
			return fileName.getText().equals(otherRow.getFileName().getText());
		}		

		////////////MouseListener methods/////////////////////////////////
		
		public void mouseClicked(MouseEvent e){
			System.out.println("mouse clicked! from Row");
//			int thisRowIndex = rows.indexOf(this);
//			listSelectionModel.addSelectionInterval(thisRowIndex, thisRowIndex);
		}
		public void mouseEntered(MouseEvent e){}
		public void mouseExited(MouseEvent e){}
		public void mousePressed(MouseEvent e){}
		public void mouseReleased(MouseEvent e){}		
	}//end class Row
}
