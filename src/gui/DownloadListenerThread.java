package gui;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.*;
import java.util.concurrent.Semaphore;
import networkOverlay.*;

/*
 * DownloadListenerThread
 * This class will track the progress of 1 pending download and update the GUI accordingly
 * instance of this class are created from hitting the download button on entries in the
 * table, in the DownloadPanel class
 */

public class DownloadListenerThread  extends SwingWorker<Void, Integer>{
	
	public static final int READY = 0;
	public static final int LOOKING_UP = 1;
	public static final int LOOKED_UP = 2;
	public static final int DOWNLOADING = 3;
	public static final int FINISHED = 4;
	public static final int FAILURE = 5;
	
	private int currentStatus = -1;
	
	
	private DownloadPanel.Row row = null;
	private Services services = null;
//	private NetworkServicesDummy dummy = null;
	private DownloadPanel downloadPanel = null;
	
	private Semaphore lookupSemaphore = new Semaphore(0);
	private Semaphore downloadSemaphore = new Semaphore(0);
	
	private String fileName = null;
	private FileInfo fileInfo = null;
	File file = null;
	
	//this is the ID of the file holder that the user has selected
//	private Long holderID = null;
	
	public DownloadListenerThread(Services s, DownloadPanel.Row r, DownloadPanel dp,
			FileInfo fileInfo){
		services = s;
		row = r;
		downloadPanel = dp;
		this.fileName = fileInfo.getName();
		this.fileInfo = fileInfo;
	}
	
//	public void setFile(File saveLocation){file = saveLocation;}
	
	//to control semaphores to begin the lookup and the download
	public void beginLookup(){
		System.out.println("in beginLookup");
		lookupSemaphore.release();
		System.out.println("leaving beginLookup");
	}
	
	public void beginDownload(File file){
		this.file = file;
		downloadSemaphore.release();		
	}
	
	public int getCurrentStatus(){return currentStatus;}

	@Override
	protected Void doInBackground(){
		//set to ready
		currentStatus = READY;
		publish(READY);
		
		//first start the lookup		

		System.out.println("about to be LOOKING_UP");
		lookupSemaphore.acquireUninterruptibly();
		System.out.println("LOOKING_UP");
		currentStatus = LOOKING_UP;
		publish(LOOKING_UP);
		System.out.println("LOOKING_UP complete");
		
		if(services.lookupFile(fileInfo.getID(), fileName)){
			currentStatus = LOOKED_UP;
			publish(LOOKED_UP);
			
			/*
			//now ask the user which file holder he wants to use
			FileInfo fileInfo = services.getLookedUpFileInfo(fileName);
			Vector<Long> holderIDs = fileInfo.getAllHolderIDs();
			JList fileHolderList = new JList(holderIDs);
			String[] options = {"OK", "Cancel"};
			JOptionPane.showOptionDialog(downloadPanel, 
					fileHolderList, "File Holders", JOptionPane.OK_CANCEL_OPTION, 
					JOptionPane.QUESTION_MESSAGE, 
					null, options, null);
			*/
			
			//******** all of this is now unnecessary since we download piecemeal
			//******** from all available file holders.
			
//			//first lets get the list of files we looked up
//			Vector<FileInfo> fileInfos = services.getLookupFileList(fileInfo.getID());
//			Vector<Long> holderIDs = new Vector<Long>();
//			for(int i = 0; i < fileInfos.size(); i++){
//				holderIDs.add(fileInfos.get(i).getHolderID());
//			}
//			
//			//now set up a window to display
//			JList fileHolderList = new JList(holderIDs);
//			fileHolderList.setSelectedIndex(0);//default to select the first one
//			fileHolderList.setBorder(BorderFactory.createLineBorder(Color.black));
//			JPanel fileHolderPanel = new JPanel();
//			fileHolderPanel.setLayout(new BorderLayout());
//			fileHolderPanel.add(fileHolderList, BorderLayout.CENTER);
//			fileHolderPanel.add(new JLabel("Select a file holder from which to download:"),
//					BorderLayout.NORTH);
//			
//			String[] options = {"OK", "Cancel"};
//			int chosenOption = JOptionPane.showOptionDialog(null, 
//					fileHolderPanel, "File Holders", JOptionPane.OK_CANCEL_OPTION, 
//					JOptionPane.QUESTION_MESSAGE, 
//					null, null, 0);
//			holderID = (Long)fileHolderList.getSelectedValue();
//			
//			if(chosenOption == 0){//if the user chose "OK", first choice in options
//				currentStatus = LOOKED_UP;
//				publish(LOOKED_UP);
//			}
//			else{
//				currentStatus = FAILURE;
//				publish(FAILURE);
//				return null;//quit here
//			}
		}
		else{
			currentStatus = FAILURE;
			publish(FAILURE);
		}
		
		//now start the download
		downloadSemaphore.acquireUninterruptibly();
		currentStatus = DOWNLOADING;
		if(services.downloadFile(fileInfo.getID(), file)){
			publish(FINISHED);
		}
		else{
			publish(FAILURE);
		}
		
		return null;
	}
	
	public void process(java.util.List<Integer> chunks){
		//get the last "chunk" published (chunks.size() - 1 is the last)
		switch(chunks.get(chunks.size() - 1).intValue()){
		case READY:
			//make it select the index of the downloads panel in the tabbed pane
			downloadPanel.getGUI().getFileTabbedPane().setSelectedIndex(1);
			row.getStatus().setText("Ready");
			row.getButton().setText("Look up");
			break;
		case LOOKING_UP:
			row.getStatus().setText("Looking up file");
			row.getButton().setText("Cancel");
			break;
		case LOOKED_UP:
			row.getStatus().setText("File found");
			row.getButton().setText("Download");
			break;
		case DOWNLOADING:
			row.getStatus().setText("Downloading");
			row.getButton().setText("Cancel");
			break;
		case FINISHED:
			row.getStatus().setText("Download finished");
			row.getButton().setText("Close");
			break;
		case FAILURE:
			row.getStatus().setText("Failed");
			row.getButton().setText("Close");
			break;
		default:
			throw new RuntimeException("OMG");
		}
//		row.getButton().setText("Close");
		downloadPanel.repaint();
	}
}
