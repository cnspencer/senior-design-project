package gui;

import java.io.*;
import java.util.*;

/*
 * NetworkServicesDummy
 * This class simply serves to fill in for a class in the API
 */

public class NetworkServicesDummy {
	private String homeDir = System.getProperty("user.home");
	
	private File localPath = new File(homeDir);
	private File foreignPath = new File(homeDir);
	
	private Vector<File> localFiles = new Vector<File>();
	private Vector<File> foreignFiles = new Vector<File>();
	
	private int displayMessageCount = 0;
	
	
	public NetworkServicesDummy(int portNumber){
		System.out.println("Port number is: " + portNumber);
		
		File[] localFileArray = localPath.listFiles();
		for(int i = 0; i < localFileArray.length; i++){
			localFiles.add(localFileArray[i]);
		}
		
		File[] foreignFileArray = foreignPath.listFiles();
		for(int i = 0; i < foreignFileArray.length; i++){
			foreignFiles.add(foreignFileArray[i]);			
		}
	}
	
	public Vector<File> getLocalFiles(){return localFiles;}
	public Vector<File> getForeignFiles(){return foreignFiles;}
	
	public void download(File targetFile, File sourceFile){
		
	}
	
	public void sendMessage(String message, int id){
		System.out.println("sending message: " + message);
		System.out.println("to host with id: " + id);
	}
	
	public String waitToDisplayMessage(){
		try{
			//wait some time
			Thread.sleep(1000);
		}
		catch(InterruptedException ex){
			//ex.printStackTrace();
		}
		//increment count and return
		return "display message " + (++displayMessageCount) + "!!!";
	}
}

