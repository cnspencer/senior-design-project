package gui;

import javax.swing.SwingUtilities;
import javax.swing.*;
import networkOverlay.*;
import javax.lang.model.SourceVersion;


/*
 * Launcher
 * starting point of the program
 * I wanted to keep it separate since it may launch
 * various threads as well as the GUI
 */
public class Launcher {
	static Integer inputPortNumber = null;
	
	public static void main(String[] args){		
		//user must have latest version of java
		if(	SourceVersion.latestSupported() == SourceVersion.RELEASE_0 ||	
				SourceVersion.latestSupported() == SourceVersion.RELEASE_1 ||
				SourceVersion.latestSupported() == SourceVersion.RELEASE_2 ||
				SourceVersion.latestSupported() == SourceVersion.RELEASE_3 ||
				SourceVersion.latestSupported() == SourceVersion.RELEASE_4 ||
				SourceVersion.latestSupported() == SourceVersion.RELEASE_5
				){
			System.out.println("You must have Java 1.6 to run this application.");
			System.exit(-555);
		}
		
		try{
			
			//set look and feel to native, instead of Java metal version
			UIManager.setLookAndFeel(
	            UIManager.getSystemLookAndFeelClassName());
//			UIManager.setLookAndFeel(
//					"com.sun.java.swing.plaf.motif.MotifLookAndFeel");
		}
		catch(Exception ex){
			ex.printStackTrace();
			System.exit(-111);
		}
		
		//check to see if user gave port number at command line
		if(args.length == 1){
			try{
				inputPortNumber = new Integer(args[0]);
			}
			catch(NumberFormatException ex){}
		}
		
		//make sure make any threads on the swing event queue
		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
//		    	String portNumber = JOptionPane.showInputDialog(null,null ,"Enter Port Number", 3);
//		    	if(portNumber != null){
			    	Services services;
//			    	if (portNumber.equals(null) || portNumber.equals(""))
//			    		services = new Services();
//			    	else
//			    		services = new Services (inputPortNumber);
			    	if(inputPortNumber == null)
			    		services = new Services();
			    	else
			    		services = new Services (inputPortNumber);
			    	new ChordGUI(services);
//		    	}
		    }
		});
	}
}
