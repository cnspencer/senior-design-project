package networkOverlay;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.swing.JOptionPane;

public class FingerTable2 implements Serializable{
	
	
	private static long twoToThirtyTwo = 1L << (long)32;//(long)Math.pow(2,32);
	public static final Object MYSELF = null;//return if target lies within my own space	
	
	private Vector<Long> fingerTable;
	//private ArrayList<String> peerList;
	private Vector<InetAddress> peerListIP;
	private  Vector<Integer> peerListPort;
	private  Vector<Long> peerListHashed;
	
	//private static transient String localHostIP;
	private static transient InetAddress localHostInetIP;
	private transient Long localHostHashed;
	private static transient int portNumber;
	private transient NetworkController networkController;
	
	//Pred inforrmation
	private transient Long myPredID;
	private transient InetAddress myPredIP;
	private transient int myPredPort;

	public FingerTable2 (InetAddress localHost, int port, NetworkController nc) {
		networkController = nc;
		localHostInetIP = localHost;
		//localHostIP = localHost.getHostAddress();
		portNumber = port;
		//localHostHashed = Hasher.hashPeerString(localHostIP+":"+portNumber);
		localHostHashed = Hasher.hashPeerLong(localHostInetIP + ":" + portNumber);
		System.out.println("My own hashed identifier: " + localHostHashed + " in Hex: " + Long.toHexString(localHostHashed));
		
		peerListIP = new Vector<InetAddress>();
		peerListPort = new Vector<Integer>();
		readPeerList();
		peerListHashed = Hasher.hashPeerListLong(peerListIP, peerListPort);
		fingerTable = new Vector<Long>();
		fingerTable = buildFingerTable(); // Setting up finger table
	}
	
	public Vector<InetAddress> getPeerList () {
		return peerListIP;
	}
	public Vector<Long> getHashedPeerList () {
		return peerListHashed;
	}
	public Long getLocalHostHashed () {
		return localHostHashed;
	}
	
	
	//***************************************************************
	// *******************Peer List Operations***********************
	// readPeerList
	public  void readPeerList(){
	    //ArrayList<String> peerList = new ArrayList<String>(); 
		//String inFileName = "src/networkOverlay/servers.txt";
		String inFileName = "networkOverlay/servers.txt";
	    File inFile = new File (inFileName);
	    System.out.println(inFile.getAbsolutePath());

	    Scanner fileScanner = null;
		//try {
			fileScanner = new Scanner(ClassLoader.getSystemResourceAsStream("networkOverlay/servers.txt"));
//		}
//		catch (FileNotFoundException ex){
//			ex.printStackTrace();
//			System.exit(4);
//		}
		
		while (fileScanner.hasNextLine()){
			String line = fileScanner.nextLine();
			StringTokenizer st = new StringTokenizer(line, ":");
			if(st.countTokens() != 2)
				continue;
			
			InetAddress serverIP; 
			String temp = "";
			try {
				temp = st.nextToken().trim();
				if(temp.equals("")) continue;
				
				//I put this in here for testing because I want it to start it's own
				//network or connect to a host on this machine if it reads the name
				//"localhost"
				if(temp.equals("localhost")){
					//we have to do this in order to avoid getting
					//127.0.0.1 for the local host
			//		serverIP = InetAddress.getByName(InetAddress.getLocalHost().getHostName());
					temp = InetAddress.getLocalHost().getHostName();
					System.out.println("local host: " + temp);
				}
				
				serverIP = InetAddress.getByName(temp);
				if (serverIP.getHostAddress().equals(localHostInetIP.getHostAddress()))
					serverIP = localHostInetIP;
				String temp2 = st.nextToken().trim();
				if(temp2.equals("")) continue;
				
				int serverPort = Integer.parseInt(temp2);
				long targetID = Hasher.hashPeerLong(serverIP+ ":" + serverPort); 
				//System.out.println(serverIP + "   " + this.localHostInetIP);
				if (targetID != getMyID()){
					Message message = Message.getHostVerificationMessage(localHostInetIP, portNumber,targetID);
					Client hostVerificationClient = new Client(serverIP, serverPort, message, networkController);
					new Thread(hostVerificationClient).start();
					//now block and wait for thread to finish
					int ExitValue = hostVerificationClient.getExitValue(4000);
					System.out.println("host veri client status: "  + ExitValue);
					if (ExitValue != Client.SUCCEEDED)
						continue;
					
//					Socket socket = new Socket(serverIP, serverPort);
//					ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
//					out.writeObject(Message.getHostVerificationMessage(localHostInetIP, portNumber,targetID));
//					socket.close();
				}
				else {
					networkController.setDedicatedPeerTrue();
					System.out.println("IM THE FIRST GUY!");
				}
				
				peerListIP.add(serverIP);
				peerListPort.add(serverPort);
				System.out.println("adding.. " + temp);
				break;
			}
			catch (UnknownHostException ex){
				System.out.println("No Host.. " + temp);
				continue;
			}
			catch  (IOException e) {
				System.out.println("No Socket.. " + temp);
				continue;
			}
		}
		if (peerListIP.isEmpty()) {	
			if (!networkController.getInTestSuite())
				JOptionPane.showMessageDialog(null, "None of the dedicated peers are up.\n Can not start new peer", "Initialization Error",0);
			System.out.println("None of the dedicated peers are up. \nExiting the appication");
			System.exit(11);
		}
	}


	
	// buildFingerTable
	public synchronized Vector<Long> buildFingerTable(){
		long localHostValue = localHostHashed;
		Vector<Long> fingerTable = new Vector<Long>();
		ArrayList<Long> peerListHashedLong = new ArrayList<Long>();
		peerListHashedLong.addAll(peerListHashed);
		Collections.sort(peerListHashedLong);
		this.fingerTable.clear();
		long minDifference;
		long currentDifference;
		int closestPeerIndex;
		
		//iterate for each value in the finger table
		//note that we start at 1, not 0, and iterate all the way up through and including
		//the finger table size (32)
		for (int i = 1; i <=32; i++ ){
			minDifference = Long.MAX_VALUE;			
			//the index in the peer list of the peer that belongs at location i
			closestPeerIndex = -1;
			//the "ideal peer position" calculated from i
//			long nextPeer = (localHostValue + (long)Math.pow(2, i))%twoToThirtyTwo;
			long nextPeer = (localHostValue + (long)(1L << (long)i))%twoToThirtyTwo;
			//System.out.println("pow " + i + " " + (1L << (long)i));
			//System.out.println("nextPeer = " + Long.toHexString(nextPeer));
			//If the peer ID calculated is in the peerlist
			if (peerListHashedLong.contains(nextPeer)){		//If the peer ID calculated is in the peerlist
				fingerTable.add(nextPeer);
			}
			else {												//If the peer ID calculated is not in the peerlist
				currentDifference = 0;							// Loop the sorted list until you just pass the calculated
				for (Long nextLong:peerListHashedLong){		//	peer. (rounding up)
					currentDifference = nextLong-nextPeer;
					if (minDifference>currentDifference && currentDifference>=0){						
						minDifference = currentDifference;
						closestPeerIndex = peerListHashedLong.indexOf(nextLong);
					}
					else
						continue;
				}
				for (Long nextLong:peerListHashedLong){		//	considering the special case where rounding up leads to a smaller number	
					currentDifference = (nextLong+twoToThirtyTwo)-nextPeer;		// add because nextPeer is now negative..
					if (minDifference>currentDifference && currentDifference>=0){						
						minDifference = currentDifference;
						closestPeerIndex = peerListHashedLong.indexOf(nextLong);
					}
					else
						continue;
				}
//				if(closestPeerIndex == -1){
//					System.out.println("current peer list index = " + closestPeerIndex);					
//				}
//				else{
//					System.out.println("current peer list index = " + closestPeerIndex);	
//					System.out.println("adding " + peerListHashedLong.get(closestPeerIndex) + " " + i);
//				}
				if(closestPeerIndex == -1){
					System.out.println("XXXXXXXXXX " + localHostHashed);
					fingerTable.add(this.localHostHashed);
				}
				else
					fingerTable.add(peerListHashedLong.get(closestPeerIndex));
			}
		}
		//printFingerTable(fingerTable);
		return fingerTable;
	}
	
	
//	public Long resolveTargetPeerIndex(int ID){
//		String targetPeer = resolveTargetPeer("0X" + Integer.toHexString(ID));
//		if (targetPeer.equals("myself"))
//			return -1;
//		else return fingerTable.indexOf(targetPeer);
//	}
	
	
	// resolveTargetPeer
	public Object[] resolveTargetPeer(Long identifier){
		return getPredPeer(identifier);
	}
	public Long getPeerByIndex(int index){
		if (fingerTable.size()>index)
			return fingerTable.get(index);
		else
			return null;
	}
	
	public synchronized Object[] getMySuccessorPeer() {
		return getSuccessorPeer(localHostHashed);
	}
	public Object[] getMyPredecessorPeer() {
		Object[] returnObj ={myPredID, myPredIP,myPredPort}; 
		return returnObj;
	}
	
	//getPredPeer - Finds the peer preceding the given ID. ROUNDING DOWN
	public synchronized Object[] getPredPeer(Long identifier){
		//why is this copied?
		long localHostValue = localHostHashed;
		//why is this copied?
		long identifierValue = identifier;
		//this is copied in order to add localhostValue
		ArrayList<Long> fingerTableLongValue = new ArrayList<Long>(fingerTable);
		fingerTableLongValue.add(localHostValue);
		Collections.sort(fingerTableLongValue);
		//now we have a copy of finger table, but with our own ID in it, and it's sorted
		
		//what is this value?
		long minDifference = Long.MAX_VALUE;
		//what is this?
		long currentDifference;
		//the index of the target
		int closestPeerIndex = -1;
		
		//for every peer in the finger table, and ourselves
		//basically this finds the closest peer index
		for (long peer:fingerTableLongValue){
			//currentDifference = the target ID - the peer ID, the difference
			currentDifference = identifierValue-peer;
			//System.out.println("from resolve target peer..-.."+peer);
			if (currentDifference<minDifference && currentDifference>=0){
				//System.out.println("from resolve target peer..a.."+peer + "..CD.." + currentDifference);
				minDifference = currentDifference;
				closestPeerIndex = fingerTableLongValue.indexOf(peer);
			}
		}
		
		//I guess this iteration is to help get the last combination when it wraps around
		identifierValue = identifierValue + twoToThirtyTwo;
		for (long peer:fingerTableLongValue){
			currentDifference = identifierValue-peer;
			if (currentDifference<minDifference && currentDifference>=0){
				//System.out.println("from resolve target peer..b.."+peer);
				minDifference = currentDifference;
				closestPeerIndex = fingerTableLongValue.indexOf(peer);
			}
		}
		//System.out.println("from find pred peer.. "+"0X" + Long.toHexString(fingerTableLongValue.get(closestPeerIndex)));
		
		//if we are the predecessor, return all nulls (why?)
		if (fingerTableLongValue.get(closestPeerIndex).equals(localHostHashed)){
				Object [] returnArray={null,null,null};
				return returnArray;
				
		}
		
		//else give the finger table value to forward it to to get the pred
		else{
			
			long pred = fingerTableLongValue.get(closestPeerIndex);
			
			InetAddress predIP = getInetAddress(pred);
			int predPort = getPortNumber(pred);
			//System.out.println("pred ID " + pred + "MY ID: " + localHostHashed + ".. \t predPort: " + predPort + " myPort: " + getMyPortNumber());
			Object [] returnArray = {pred, predIP,predPort};
			return returnArray;
		}
	}
	
	//getSuccessorPeer - Finds the peer succeeding the given ID. ROUNDING UP
	public synchronized Object[] getSuccessorPeer(Long identifier){
		//long localHostValue = convertHexToLongValue(localHostHashed);
		long identifierValue = identifier;
		ArrayList<Long> fingerTableLongValue = new ArrayList<Long>(fingerTable);
		//fingerTableLongValue.add(localHostValue);
		Collections.sort(fingerTableLongValue);
		long minDifference = Long.MAX_VALUE;
		
		long currentDifference;
		int closestPeerIndex = -1;
		for (long peer:fingerTableLongValue){
			currentDifference = peer-identifierValue;
			//System.out.println("from succ target peer..-.."+peer +" compare to succ of " + identifierValue + " current Difference: " + currentDifference);
			if (currentDifference<minDifference && currentDifference>0){
				//System.out.println("from suc target peer..a.."+peer + "..CD.." + currentDifference);
				minDifference = currentDifference;
				closestPeerIndex = fingerTableLongValue.indexOf(peer);
			}
		}
		
		identifierValue = identifierValue - twoToThirtyTwo;
		for (long peer:fingerTableLongValue){
			currentDifference = peer - identifierValue;
			//System.out.println("from succ target peer..-.."+peer +" compare to succ of " + identifierValue + " current Difference: " + currentDifference);
			
			if (currentDifference<minDifference && currentDifference>0){
				//System.out.println("from resolve target peer..b.."+peer);
				minDifference = currentDifference;
				closestPeerIndex = fingerTableLongValue.indexOf(peer);
			}
		}
		//System.out.println("from resolve find suc peer.. peer"+"0X" + Long.toHexString(fingerTableLongValue.get(closestPeerIndex)));
		
		if ((closestPeerIndex == -1)){ //|| fingerTableLongValue.get(closestPeerIndex).equals(localHostHashed)){
			Object [] returnArray={null,null,null};
			return returnArray;
		}
		 else{
			 long successor = fingerTableLongValue.get(closestPeerIndex);
			 InetAddress successorIP = getInetAddress(successor);
			 int successorPort = this.getPortNumber(successor);
			 Object [] returnArray = {successor,successorIP,successorPort};
			return returnArray;
		 }
	}
	
	
	public synchronized void updateFingerTable (InetAddress peerIP, int peerPort){
		//System.out.println("Attempting to update with ID: " + Long.toHexString(Hasher.hashPeerLong(peerIP, peerPort)));
		Long peerHashed = Hasher.hashPeerLong(peerIP+ ":" +  peerPort);
		//if (!peerListIP.contains(peerIP)){
			peerListIP.add(peerIP);
			peerListPort.add(peerPort);
			peerListHashed.add(peerHashed);
		//}
		fingerTable = buildFingerTable();
		removeInconsistentPeerEntries();
	}
	
	public synchronized void updateFingerTableEntry (InetAddress peerIP, int peerPort, int index){
		
		Long peerHashed = Hasher.hashPeerLong(peerIP+ ":" +  peerPort);
		peerListIP.add(peerIP);
		peerListPort.add(peerPort);
		peerListHashed.add(peerHashed);
		try{
		if (fingerTable.size()<=index || fingerTable != null){
			//System.out.println("Finger Table not set up...Can't update with peerID: " + Hasher.hashPeerLong(peerIP+ ":" +peerPort));
			return;
		}else{
			fingerTable.set(index, peerHashed);
			//System.out.println("Updating finger table entry " + Long.toHexString(fingerTable.get(index)) + " to " + Long.toHexString(peerHashed));
		}
		}
		catch (NullPointerException Ex){
			JOptionPane.showMessageDialog(null,"Finger Table Size: " + fingerTable.size());
			return;
		}
		//removeInconsistentPeerEntries();
	}
	
	public synchronized void removeFromFingerTable (InetAddress peerIP, int peerPort){

		printFingerTable();
		Long peerHashed = Hasher.hashPeerLong(peerIP+ ":" +  peerPort);
		//System.out.println("Removing from finger table: " + Long.toHexString(peerHashed));
		for (int i= 0; i<peerListHashed.size(); i++){
			//System.out.println(peerListHashed.get(i) + "Compare to" + peerHashed);
			if (peerListHashed.get(i).equals(peerHashed)){
				//System.out.println(i + "removing " + peerHashed);
				peerListIP.remove(i);
				peerListPort.remove(i);
				peerListHashed.remove(i);
				i=i-1;
			}
		}
		peerListIP.trimToSize();
		peerListPort.trimToSize();
		peerListHashed.trimToSize();
		fingerTable = buildFingerTable();
		removeInconsistentPeerEntries();
		printFingerTable();
	}
	
	public synchronized void removeInconsistentPeerEntries() {
		for (int i=0; i < peerListHashed.size(); i++){
			//String hashedPeer: peerListHashed
			Long hashedPeer = peerListHashed.get(i);
			if (!fingerTable.contains(hashedPeer)){
				//System.out.println("Removing from peer list:  " + Long.toHexString(hashedPeer));
				
				peerListIP.remove(peerListHashed.indexOf(hashedPeer));
				peerListPort.remove(peerListHashed.indexOf(hashedPeer));
				peerListHashed.remove(hashedPeer);
				i = i-1;
			}
		}
		peerListIP.trimToSize();
		peerListPort.trimToSize();
		peerListHashed.trimToSize();
	}
	
	public void setFingerTable(FingerTable2 ft){
		peerListIP = ft.peerListIP;
		peerListPort = ft.peerListPort;
		peerListHashed = ft.peerListHashed;
		fingerTable = ft.fingerTable;
		printFingerTable();
	}
	
	
	public synchronized void adjustFingerTable() {
		for (int i = 0; i <32; i++ ){
			long nextPeer = (getMyID() + (long)Math.pow(2, i))%twoToThirtyTwo;
			Object []targetPeer = getPredPeer(nextPeer);
			if (!(targetPeer[0] ==FingerTable2.MYSELF)){
				//System.out.println("Adjust: Sending out a message to: " + Long.toHexString((Long)targetPeer[0]));
				InetAddress targetPeerIP = (InetAddress)targetPeer[1];
				int targetPeerPort = (Integer)targetPeer[2];
				long targerPeerID = (Long)targetPeer[0];
				Client client = new Client(targetPeerIP, 
						targetPeerPort ,
						Message.getFindSuccessorMessage(localHostInetIP, portNumber, localHostHashed,nextPeer ,i),
						this.networkController
						);
				new Thread(client).start();
			}
			else
			{
				Object [] mySuccessor = getMySuccessorPeer();
				if (mySuccessor[0]== FingerTable2.MYSELF){
					updateFingerTableEntry(localHostInetIP, portNumber ,i);
					removeInconsistentPeerEntries();
				}
				else {
					InetAddress targetPeerIP = (InetAddress)mySuccessor[1];
					int targetPeerPort = (Integer)mySuccessor[2];
					//System.out.println("Adjust: Sending out a message to: " + Long.toHexString((Long)mySuccessor[0]));
					Client client = new Client(targetPeerIP, 
							targetPeerPort ,
							Message.getFindSuccessorMessage(localHostInetIP, portNumber, localHostHashed,nextPeer ,i),
							this.networkController
							);
					new Thread(client).start();
				}
			}
		}
	}

	
	
	public long getMyID() {
		return localHostHashed;
	}
	
	public InetAddress getMyIPAddress(){
		//System.out.println(identifier + " compare to my id " + localHostHashed);
			return  getInetAddress(-1L);
	}
	public InetAddress getInetAddress(Long identifier){
		//System.out.println(identifier + " compare to my id " + localHostHashed);
		if (identifier.equals(-1L)){
			return  localHostInetIP;
		}
		return peerListIP.get(peerListHashed.indexOf(identifier));
	}
	
	
	public int getMyPortNumber(){
			return  getPortNumber(-1L);
	}
	
	public int getPortNumber(Long identifier){
		if (identifier.equals(-1L)){
			return  portNumber;
		}return peerListPort.get(peerListHashed.indexOf(identifier));
	}
	
	
	
	public void printFingerTable(){
		System.out.println("Size of finger table: " + fingerTable.size());
		for (int i = 0; i < fingerTable.size(); i++) {
			//System.out.println("\t Finger table entry..." + 
				//	Long.toHexString(fingerTable.get(i)));
			
		}
		System.out.println("My ID: " + Long.toHexString(this.getMyID()));
			
	}
	
//	public void updateFingerTable1(){
//		for (int i = 0; i < fingerTable.size(); i++){
//			updateFingerTable1Entry(
//					peerListIP.get(peerListHashed.indexOf(fingerTable.get(i))),
//					peerListPort.get(peerListHashed.indexOf(fingerTable.get(i))),
//					i);
//
////			networkController.getFingerTable().update(
////					Long.valueOf(fingerTable.get(i)).intValue(), 
////					peerListIP.get(i), 
////					peerListPort.get(i));
//		}
//			
//		
//	}
	public void updateFingerTable1Entry(InetAddress peerIP, int peerPort, int index){
//				peerIP,peerPort,index);
//		networkController.getFingerTable().update(
//				fingerTable.get(index).intValue(), 
//				peerListIP.get(index), 
//				peerListPort.get(index));
	}
	
	
	public void setPredecessor(long predID, InetAddress predIP, int predPort){
		this.myPredID = predID;
		this.myPredIP = predIP;
		this.myPredPort = predPort;
	}
	
	public long getMyPredID(){
		return myPredID;
	}
	public InetAddress getMyPredIP(){
		return myPredIP;
	}
	public int getMyPredPort(){
		return myPredPort;
	}
	
	public synchronized boolean containsOnlyMyself(){
		for (int i =0; i<fingerTable.size();i++){
			if(!fingerTable.get(i).equals(getMyID())){
				return false;
			}
		}
		return true;
	}
	
	public boolean isBetween(Long testPeer, Long firstPeer, Long secondPeer){
		
		if (((long)testPeer == (long)secondPeer) || ((long)testPeer==(long)firstPeer))
			return false;
		
		if(firstPeer<secondPeer){
			if ((testPeer <firstPeer) || (testPeer >secondPeer))
				return false;
			else 
				return true;
		}else{
			if ((testPeer<firstPeer) && (testPeer>secondPeer))
				return false;
			else
				return true;
		}
	}
	
	
	//**********************************************************
	//*************Hex to Decimal Calculations******************
	//**********************************************************
	
	public static long convertHexToLongValue(String hexNumber){
//		if (hexNumber =="myself")
//			hexNumber = localHostHashed;
		long returnLong = 0;
		if (hexNumber.length()==10){
			returnLong = Integer.decode("0X" + hexNumber.substring(3,10)); 
			long eigthHex =  Integer.decode("0X" + hexNumber.charAt(2));
			eigthHex = eigthHex * 268435456;// Add the 8th number.
			returnLong +=eigthHex;
		}
		else{
			try {
			returnLong = (long)Integer.decode(hexNumber);
			}catch (Exception ex){
				System.out.println(ex.toString());
			}
		}
		return returnLong;
			
	}
	
	// convertHexToString
	public static ArrayList<Long> convertHexToLong (Collection<String> listOfStrings) {
		synchronized(listOfStrings){
			ArrayList<Long> listOfInts = new ArrayList<Long>();
			for (String s:listOfStrings){
				listOfInts.add(convertHexToLongValue(s));
			}
			return listOfInts;
		}
	}
}
