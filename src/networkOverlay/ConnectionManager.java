package networkOverlay;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.concurrent.Semaphore;

/*
 * ConnectionManager
 * sometimes when Client wants to communicate with someone, it must forward the first
 * message through the network, but the host that the client is communicating with will
 * respond back directly, on a different socket.  This class allows the Client to block and
 * wait until the response is recieved, and it replaces the socket with a new one
 */

public class ConnectionManager {
	private NetworkController networkController;
	
	private HashMap<Long, Client> waitingClients = new HashMap<Long, Client>();
	private HashMap<Long, Semaphore> waitingSemaphores = new HashMap<Long, Semaphore>();
	private HashMap<Long, Socket> waitingSockets = new HashMap<Long, Socket>();
	private HashMap<Long, ObjectOutputStream> waitingOutputs = new HashMap<Long, ObjectOutputStream>();
	private HashMap<Long, ObjectInputStream> waitingInputs = new HashMap<Long, ObjectInputStream>();
	
	public ConnectionManager(NetworkController nc){
		networkController = nc;
	}
	
	//the Client will call this method, and the ConnectionManager will read the client
	//and register him in a hashtable mapping targetID to the client.
	public Socket waitForNewConnection(long targetID, Client client,
			ObjectOutputStream output, ObjectInputStream input)throws IOException{
		waitingClients.put(targetID, client);
		Semaphore semaphore = new Semaphore(0);
		waitingSemaphores.put(targetID, semaphore);
		waitingOutputs.put(targetID, output);
		waitingInputs.put(targetID, input);
		

		//this should be done by server, not by client
		
//		//send message
//		Socket socket = client.getSocket();
//		ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
//		Message message = Message.getEstablishConnectionMessage(targetID);
//		output.writeObject(message);
		
		try{//wait for message to arrive
			System.out.println("Client about to wait in ConnectionManager");
			semaphore.acquire();
		}
		catch(InterruptedException ex){
			ex.printStackTrace();
			return null;
		}
		//we have to pass along the streams
		client.setObjectInputStream(waitingInputs.get(targetID));
		client.setObjectOutputStream(waitingOutputs.get(targetID));
		
//		waitingSemaphores.remove(targetID);
		return waitingSockets.remove(targetID);
	}
	
	//we sent a message to targetID and are awaiting a response
	//here we have received the response and would like to alert the Client waiting for the
	//response that it can go now
	public void checkIfConnectionAwaited(long targetID, Socket socket, 
			ObjectOutputStream output, ObjectInputStream input){
//		if(waitingClients.contains(targetID) && waitingSemaphores.containsKey(targetID)){
		if(socket.isClosed()) System.out.println("Why is the socket closed????");
			//put in the socket
			waitingSockets.put(targetID, socket);
			waitingInputs.put(targetID, input);
			waitingOutputs.put(targetID, output);
			
			//release once ready
			waitingSemaphores.get(targetID).release();
			System.out.println("leaving checkIfConnectionAwaited()");
//		}
		//else, extraneous message recieved
	}
}
