package networkOverlay;



import static java.lang.System.out;

import java.io.File;
import java.io.*;
import java.net.InetAddress;
import java.util.*;

/**
 * DownloadManager
 * this class manages all the downloads we are doing
 * as well as all the lookups
 */

public class DownloadManager {
	//download statuses
	public static final int NULL = 0;
	public static final int ERROR = 1;//internal error
	public static final int FAILED = 2;//cannot download (he may not have it any more)
	public static final int READY = 3;
	public static final int LOOKING_UP = 4;
	public static final int LOOKED_UP = 5;
	public static final int DOWNLOADING = 6;
	public static final int FINISHED = 7;
	
	//the max size of a download packet, in bytes
//	public static final int MAX_DOWNLOAD_PACKET_SIZE = 100;
	
	/**the min size we will download from a single peer (that is, we dont' want to download
	 * like 1 byte from each of a million different people)*/
	public static final int MIN_DOWNLOAD_SIZE = 4096;
	
	//the statuses of all the various files we are dealing with
	//maps from a fileID to a status
	private Hashtable<Long, Integer> fileStatuses = new Hashtable<Long, Integer>();
	
	//this maps from fileID to the File object (location on this machine)
	private Hashtable<Long, File> filePaths = new Hashtable<Long, File>();
	
	//this maps from fileID to the DownloadInfo for it, this is for being able to easy save pieces of a file
	private Hashtable<Long, DownloadInfo> downloadInfos = new Hashtable<Long, DownloadInfo>();
	
	//these are files we have looked up and are ready to begin downloading
	//this maps from fileID to FileInfo, giving us info for the guy who holds it, file size etc
	//actually it gives a vector of FileInfos, one for each host that has the file
	private Hashtable<Long, Vector<FileInfo>> lookedUpFiles = new Hashtable<Long, Vector<FileInfo>>();
	
	
	//maps from the file ID to the client working on looking up that file
	private Hashtable<Long, Client> lookupClients = new Hashtable<Long, Client>();
	
	//maps from the file ID to the client downloading that file
	private Hashtable<Long, Client> downloadClients = new Hashtable<Long, Client>();
	
	//maps from file ID to the Download object
//	private Hashtable<Long, Download> downloads = new Hashtable<Long, Download>();
	
	private NetworkController networkController = null;	
	
	public DownloadManager(NetworkController nc){
		networkController = nc;
	}

	//////// INTERACTION WITH LOOKED UP FILES ///////////////////////////
	public Vector<FileInfo> getLookedUpFile(Long fileID) {
		return lookedUpFiles.get(fileID);
	}
	public FileInfo getLookedUpFileInfo(Long fileID, Long fileHolderID){
		Vector<FileInfo> fileInfos = lookedUpFiles.get(fileID);
		for(int i = 0; i < fileInfos.size(); i++){
			if(fileInfos.get(i).getHolderID().equals(fileHolderID)){
				return fileInfos.get(i);
			}
		}
		return null;
	}
	public void registerLookedUpFile(Long fileID, Vector<FileInfo> fileInfos) {
//		System.out.println("ffffileID " + fileID + " fileInfo " + fileInfo);
		lookedUpFiles.put(fileID, fileInfos);
	}
//	public FileInfo removeLookedUpFile(Long fileID) {
//		return lookedUpFiles.remove(fileID);
//	}
	
	/////interaction with file paths////////////////////////
	public File getFilePath(Long fileID){
		return filePaths.get(fileID);
	}
	
	//////// METHODS FOR USE BY OTHER APPLICATIONS (GUI), OR BY Services ///////////
	
	//returns true if registered, false if nothing done
	public boolean registerFile(long fileID, File localPath){
		if(fileStatuses.contains(fileID)){
			return false;//nothing done
		}
		else{
			fileStatuses.put(fileID, READY);
			filePaths.put(fileID, localPath);
			return true;
		}
	}
	
	//removes file from all hash tables
	public void unregisterFile(long fileID){
		fileStatuses.remove(fileID);
		filePaths.remove(fileID);
		lookedUpFiles.remove(fileID);
		lookupClients.remove(fileID);
		downloadClients.remove(fileID);
		downloadInfos.remove(fileID);
	}

	//this sends out a message and returns the thread that is getting the message
	//we can block and wait for this thread to finish and get its exit value
	//the thread will automatically register the file info in our hashtable
	private Client lookUpFile(long fileID, String fileName){
		InetAddress myIP = networkController.getLocalHostIP();
		int myPort = networkController.getLocalHostPort();
		Message message = Message.getLookupFileMessage(fileID, myIP, myPort, fileName);
		Client client = new Client(fileID, message, networkController);
		lookupClients.put(fileID, client);
		
		new Thread(client).start();
		
		return client;
	}
	
//	private Client downloadFileOld(long fileID, long fileHolderID){	
//		//the guy who will download it
//		Client downloadClient = null;
//		//the guy who looked it up
//		Client lookupClient = lookupClients.remove(fileID);//immediately remove it (dont want memory leaks)
//		int exitValue = lookupClient.getExitValue();//it blocks until finished
//		if(exitValue == Client.SUCCEEDED){
//			System.out.println("downloadfile(): we see that lookup succeeded");
//			
//			//if we are here, the file should be registered in our table
//			//immediately remove (memory leaks)
//			FileInfo fileInfo = getLookedUpFileInfo(fileID, fileHolderID);
//			fileInfo.setFilePath(filePaths.get(fileInfo.getID()));
//			
//			Message downloadMessage = Message.getDownloadFileMessage(fileID,
//					fileInfo.getHolderID(), 0, fileInfo.getFileSize());
//			//make a direct connection
//			System.out.println("" + fileInfo.getHolderPort() + fileInfo.getHolderIPAddress() + "");
//			downloadClient = new Client(fileInfo.getHolderIPAddress(), fileInfo.getHolderPort(),
//					downloadMessage, networkController);
//			new Thread(downloadClient).start();
//			
//			//downloadClient will later start a new Download
//		}
//		else{
//			System.out.println("lookup did not succeed");
//		}
//		return downloadClient;		
//	}
	
	/**downloads a file we have already
	 * we have to have already looked it up and have it in our table 
	 * we call a method in the client to block until it is finished downloading*/
	private int downloadFile(long fileID){
		System.out.println("in downloadfile()");
	
		//the guy who looked it up
		Client lookupClient = lookupClients.remove(fileID);//immediately remove it (dont want memory leaks)
		int exitValue = lookupClient.getExitValue();//it blocks until finished
		System.out.println("in downloadfile() 1");
		if(exitValue != Client.SUCCEEDED){//make sure the lookup is okay
			System.out.println("lookup did not succeed");
			return Client.FAILED;
		}
		System.out.println("in downloadfile() 2");
		
//		FileInfo fileInfo = getLookedUpFileInfo(fileID, fileHolderID);
		
		//first lets do some calculations
		//our goal is to distribute the download evenly among all the hosts that have the file
		//but we want to make sure not to download like 1 byte from each of a million peers
		//so we use a minimum number of bytes per piece: DownloadManager.MIN_DOWNLOAD_SIZE
		
		Vector<FileInfo> fileInfos = lookedUpFiles.remove(fileID);
		Collections.shuffle(fileInfos);//so we don't always download more from the first guy

		System.out.println("in downloadfile() 3");
		
		int numberOfHosts = fileInfos.size();
		if(numberOfHosts == 0) return Client.FAILED;
		long fileSize = fileInfos.get(0).getFileSize();

		System.out.println("in downloadfile() 4");
		
		
		//this is rounded down
		int bytesPerPiece = (int)(fileSize / numberOfHosts);

		System.out.println("in downloadfile() 5");
		
		//if the file is smaller than the minimum packet size, just download from 1 host
		if(fileSize < MIN_DOWNLOAD_SIZE){
			System.out.println("in downloadfile() 5.0.1");
			numberOfHosts = 1;
			bytesPerPiece = (int)fileSize;
		}
		//if bytesPerPiece is less that MIN_DOWNLOAD_SIZE, we can't download from all the hosts
		else if(bytesPerPiece < MIN_DOWNLOAD_SIZE){
			System.out.println("in downloadfile() 5.1");
			numberOfHosts = (int)(fileSize / MIN_DOWNLOAD_SIZE);//rounds down
			System.out.println("in downloadfile() 5.2");
			System.out.println("fileSize: " + fileSize + " numberOfHosts: " + numberOfHosts);
			bytesPerPiece = (int)(fileSize / numberOfHosts);
			System.out.println("in downloadfile() 5.3");
		}
		

		System.out.println("in downloadfile() 6");
		
		
		

		System.out.println("in downloadfile() 7");
		
		//bytes left over after the division (these will get distributed among a few hosts)
		//(distributed 1 byte per each host for a proper subset of the hosts)
		int remainingBytes = (int)(fileSize - (numberOfHosts * bytesPerPiece));

		System.out.println("in downloadfile() 8");
		//this is our return value
		Client[] downloadClients = new Client[numberOfHosts];
		int currentOffset = 0;//where to start the download from a client, gets increased
		int currentLeftoverBytes = remainingBytes; //the remainder bytes, 
			//only some clients get the extra byte downloaded and this gets decremented

		System.out.println("in downloadfile() 9");
		//make the DownloadInfo. It automatically makes sure the save file exists on the machine
		DownloadInfo downloadInfo = new DownloadInfo(filePaths.get(fileInfos.get(0).getID()),
				fileInfos.get(0).getFileSize());

		System.out.println("in downloadfile() 10");
		//remember it
		downloadInfos.put(fileInfos.get(0).getID(), downloadInfo);		
		
		//start up all the clients
		for(int i = 0; i < numberOfHosts; i++){
			int length = bytesPerPiece;
			System.out.println("in downloadfile() 10.1");
			if(currentLeftoverBytes > 0){
				//give a byte over to the length from the leftover bytes if any remain
				length++;
				currentLeftoverBytes--;
			}
			//start the download of this piece
			downloadClients[i] = downloadFilePiece(fileInfos.get(i), currentOffset, length);
			currentOffset += length;//adjust the length for the next piece
		}
		System.out.println("in downloadfile() 11");
		
		//now at this point, we wait for Clients to finish
		int returnExitValue = Client.NULL;
		
		//this is the next host we download from in case of failure
		int nextHostToDownloadFrom = 0;
		//number of people that have failed
		int numberNotWorking = 0;
		//this is a list of booleans showing if a host is working
		boolean[] hostIsWorking = new boolean[fileInfos.size()];
		for(int i = 0; i < hostIsWorking.length; i++) hostIsWorking[i] = true;
		
		//we loop through to make sure everyone succeeded
		//if they failed, we find someone else to download from
		for(int i = 0; i < downloadClients.length; i++){
			int currentExitValue = downloadClients[i].getExitValue();			
			//if not succeeded, we must download from someone else
			if(currentExitValue != Client.SUCCEEDED){
				hostIsWorking[i] = false;//we just found out he's not working
				numberNotWorking++;
				
				
				//loop until we find a host that's working to download from
				//it will wrap around and stop right before it reaches nextHostToDownloadFrom again
				while(true){
					//if no one is working, send failure message
					if(numberNotWorking >= fileInfos.size()) return Client.FAILED;
					
					if(hostIsWorking[nextHostToDownloadFrom]){
						Message oldMessage = downloadClients[i].getStartMessage();
						downloadClients[i] = 
							downloadFilePiece(fileInfos.get(nextHostToDownloadFrom), oldMessage.getStartingIndex(), 
									(int)oldMessage.getBytesToDownload());
						
						i--;//we want to wait on this client again
						
						break;
					}
					else{
						nextHostToDownloadFrom++;
						nextHostToDownloadFrom %= fileInfos.size();
						
						numberNotWorking++;//eventually we can break out of loop
					}
				}
			}
		}
		
		//now check the file content against the hash
		Long contentHash = Hasher.hashFileContent(filePaths.get(fileID));
		if(contentHash.equals(fileID))		
			return Client.SUCCEEDED;
		else
			return Client.FAILED;
	}

	/**downloads a piece of a file (we do separate pieces since each piece may be from
	 * a separate host).*/
	private Client downloadFilePiece(FileInfo fileInfo, int offset, int length){
		System.out.println("downloadfile(): we see that lookup succeeded");
		
		Client downloadClient = null;
			
		fileInfo.setFilePath(filePaths.get(fileInfo.getID()));
			
		Message downloadMessage = Message.getDownloadFileMessage(fileInfo.getID(),
				fileInfo.getHolderID(), offset, length);
		//make a direct connection
		System.out.println("" + fileInfo.getHolderPort() + fileInfo.getHolderIPAddress() + "");
		downloadClient = new Client(fileInfo.getHolderIPAddress(), fileInfo.getHolderPort(),
				downloadMessage, networkController);
		new Thread(downloadClient).start();
			
		//downloadClient will later start a new Download
		return downloadClient;
	}
	
	public int getDownloadStatus(long fileID){
		return fileStatuses.get(fileID);
	}

	
	//this waits to finish a lookup then returns the status
	public int startLookupAndWait(long fileID, String fileName){
		
		//start the lookup
		fileStatuses.put(fileID, LOOKING_UP);
		Client lookupClient = lookUpFile(fileID, fileName);
		
		//block to get value for the switch
		switch(lookupClient.getExitValue()){
		case Client.SUCCEEDED:
			fileStatuses.put(fileID, LOOKED_UP);
			out.println("leaving startLookupAndWait()");
			return LOOKED_UP;
		case Client.FAILED:
			fileStatuses.put(fileID, FAILED);
			out.println("leaving startLookupAndWait()");
			return FAILED;
		case Client.ERROR:
			fileStatuses.put(fileID, ERROR);
			out.println("leaving startLookupAndWait()");
			return ERROR;
		default:
			out.println("leaving startLookupAndWait()");
			return NULL;
		}
	}
	
	//this waits to finish the download the returns the status
	public int startDownloadAndWait(long fileID, File filePath){
		System.out.println("in startDownloadAndWait()");
		
		if(!lookupClients.containsKey(fileID)) return FAILED;
			
		//register file
		this.registerFile(fileID, filePath);
		System.out.println("File found in lookupClients");
		//start the download
		fileStatuses.put(fileID, DOWNLOADING);
//		Client[] downloadClients = downloadFile(fileID);
//		int exitValue = Client.NULL;
//		if  (downloadClients == null)
//			exitValue = Client.FAILED;
//		else {
//			//this loop causes all waits until all the Clients (each downloading a piece of the file)
//			//finish downloading, and sets the exit status too
//			for(int i = 0; i < downloadClients.length; i++){
//				//wait for each one to finish
//				int currentExitValue = downloadClients[i].getExitValue();
//				
//				//set the final exit value (if any had an error, set it to error
//				//else if any had a failure, set it to failed
//				if(currentExitValue == Client.ERROR){
//					exitValue = Client.ERROR;
//				}
//				else if(currentExitValue == Client.FAILED || currentExitValue == Client.NULL){
//					if(exitValue != Client.ERROR)
//						exitValue = Client.FAILED;
//				}
//				else if (currentExitValue == Client.SUCCEEDED){
//					exitValue = Client.SUCCEEDED;
//				}
//			}
//		}
		
		int exitValue = downloadFile(fileID);
			
		//block to get value for the switch
		switch(exitValue){
		case Client.SUCCEEDED:
			fileStatuses.put(fileID, FINISHED);
			return FINISHED;
		case Client.FAILED:
			fileStatuses.put(fileID, FAILED);
			return FAILED;
		case Client.ERROR:
			fileStatuses.put(fileID, ERROR);
			return ERROR;
		default:
			return NULL;
		}
	}
	
	//if we want to do lookup and download together, we use this
	@Deprecated
	public int waitOnLookupAndDownload(long fileID, long fileHolderID, File localPath, String fileName){
		try{
			//register
		//	registerFile(fileID, localPath);
			
			//lookup
			int lookupStatus = startLookupAndWait(fileID, fileName);
			switch(lookupStatus){
			case LOOKED_UP:
				//download
				int downloadStatus = startDownloadAndWait(fileID, localPath);
				switch(downloadStatus){
				case FINISHED:
					return FINISHED;
				default:
					return downloadStatus;
				}
			default:
				return lookupStatus;
			}
		} finally{
			//be sure to always unregister the file on exit
			unregisterFile(fileID);
		}
	}
	
	/*package_access*/ void saveFilePiece(Long fileID, int offset, byte[] data){
		DownloadInfo downloadInfo = downloadInfos.get(fileID);
		if(downloadInfo != null){
			downloadInfo.saveFilePiece(offset, data);
		}
	}
	
	/**
	 * DownloadInfo: 
	 * This class is used to manage info about a download for a file, such as the pieces
	 * 
	 */
	public class DownloadInfo{
		private File filePath = null;
		private RandomAccessFile raf = null;
		
		public DownloadInfo(File saveLocation, long fileLength){
			filePath = saveLocation;
			try{
				raf = new RandomAccessFile(filePath, "rw");
				raf.setLength(fileLength);
				raf.close();
			}
			catch(FileNotFoundException ex){
				ex.printStackTrace();
			}
			catch(IOException ex){
				ex.printStackTrace();
			}			
		}
		
		/**Having this class allows us to synchronize on a download, instead of on the whole
		 * DownloadManager class or something
		 * @param offset
		 * @param length
		 */
		public synchronized void saveFilePiece(int offset, byte[] data){
			RandomAccessFile raf = null;
			try{
				raf = new RandomAccessFile(filePath, "rw");
				
				//we don't want to write beyond end of file
				if(offset + data.length > raf.length()) return;
				
				//set offset
				raf.seek(offset);				
				//write it out
				raf.write(data);
			}
			catch(FileNotFoundException ex){
				ex.printStackTrace();
			}
			catch(IOException ex){
				ex.printStackTrace();
			}
			finally{
				//be sure to try to close the file
				if(raf != null){
					try{
						raf.close();
					} catch(IOException ex){ex.printStackTrace();}
				}
			}
		}
		
		@Override
		public void finalize(){
			//be sure to try to close the file
			if(raf != null){
				try{
					raf.close();
				} catch(IOException ex){ex.printStackTrace();}
			}
		}
	}

}
