package networkOverlay;

import static java.lang.System.out;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.concurrent.*;
import java.security.*;
import javax.crypto.*;

public class Client implements Runnable{
	//the exit values
	public static final int NULL = 0;
	public static final int SUCCEEDED = 1;
	public static final int ERROR = 2;
	public static final int FAILED = 3;
	
	private int exitValue = NULL;
	
	private Semaphore finishedSemaphore = new Semaphore(0);
	
	private Message startMessage;
	private Message replyMessage;
	private Semaphore replyReceived = new Semaphore(0);
	private InetAddress targetAddress;
	private int targetPort;
	private NetworkController networkController;
	private boolean secondAttempt = false;
	
	private Socket socket;
	private ObjectOutputStream output;
	private ObjectInputStream input;
	
	private boolean directConnection = false;//false if we used the network, true if we
	//contacted peer directly via IP address and port
	
	//call this if socket not yet connected, and we want a direct connection (not through network)
	public Client(InetAddress ip, int port, Message startMessage, NetworkController nc){
		out.println("Client() arrived in client");
		
		targetAddress = ip;
		targetPort = port;
		this.startMessage = startMessage;
		networkController = nc;
		directConnection = true;
	}
	
	//this is if we only know the ID, it must be sent to someone on the finger table and forwarded
	public Client(long targetID, Message startMessage, NetworkController nc){
		out.println("Client() arrived in client" );
		
		networkController = nc;
		//FingerTable fingerTable = networkController.getFingerTable();		
		//this is the guy we will send it to, and he will forward it
		FingerTable2 fingerTable2 = networkController.getFingerTable2();
		Object [] targetPeer = networkController.getFingerTable2().resolveTargetPeer(targetID);
		if(targetPeer[0] == FingerTable2.MYSELF){
			//System.out.println("message for myself, but send anyways");
			targetAddress = fingerTable2.getMyIPAddress();
			targetPort = fingerTable2.getMyPortNumber();
			this.startMessage = startMessage;
			directConnection = false;
		}
		else{
			targetAddress = (InetAddress)targetPeer[1];
			targetPort = (Integer)targetPeer[2];
			//System.out.println("from Client() " + targetAddress + ":" + targetPort);
			this.startMessage = startMessage;
			directConnection = false;
		}
	}
	
	public void run(){
		out.println("began run() for message " + startMessage.getMessageType());
		try{
			//dial the phone
			socket = new Socket(targetAddress, targetPort);
			output = new ObjectOutputStream(socket.getOutputStream());
			input = new ObjectInputStream(socket.getInputStream());
			
			//start talking
			if (socket.isConnected()){
				System.out.println("Socket is connected for: " + startMessage.getMessageType() + " to " + targetPort);
				
				//here is the method call that lets us start communication with the other host
				startConversation(startMessage);
				
			}
			else{
				System.out.println("Socket is connected for: " + startMessage.getMessageType());
				throw new IOException();
			}
			if(exitValue == NULL) exitValue = SUCCEEDED;
		}
		catch (IOException ex){
			System.out.println("FAILED TO CONNECT TO SERVER: " + targetAddress + " Port: " + targetPort + " for message " + startMessage.getMessageType());
			if (!secondAttempt){
				//System.out.println("Trying to connect again...");
				secondAttempt = true;
				tryConnectingAgain();
			}
			else {
				ForwardingFailureHandler ffh = new ForwardingFailureHandler(targetAddress,targetPort,startMessage,networkController);
				new Thread(ffh).start();
				exitValue = ffh.getExitStatus();
				//ex.printStackTrace();
			}
		}
		catch(ClassNotFoundException ex){
			ex.printStackTrace();
			exitValue = ERROR;
		}
		catch(Exception ex){
			ex.printStackTrace();
			exitValue = ERROR;
		}
		finally{
			//anyone that blocked on us can now go and get our exit value
			System.out.println("in finally");
			finishedSemaphore.release();
			cleanUp();
			System.out.println("leaving Client for " + startMessage.getMessageType());
			try{
				Thread.sleep(10000);
			}catch(Exception ex){};
		}
	}
	
	public void tryConnectingAgain(){
		run();
	}
	
	//***** other stuff ***********************
	
	//just cleans up the sockets and io streams
	private void cleanUp(){
		try{
			if(output != null)
				output.close();
		}
		catch(IOException ex){
			ex.printStackTrace();
		}
		
		try{
			if(input != null)
				input.close();
		}
		catch(IOException ex){
			ex.printStackTrace();
		}
		
		try{
			if(socket != null)
				socket.close();
		}
		catch(IOException ex){
			ex.printStackTrace();
		}
	}
	
	protected void finalize(){
		cleanUp();
	}
	
	public int getExitValue(){
		//wait for it to finish first
		if(exitValue != NULL) return exitValue;
		try{
			finishedSemaphore.acquire();
		}
		catch(InterruptedException ex){
			ex.printStackTrace();
			return NULL;
		}
		return exitValue;
	}
	
	public Message getStartMessage(){return startMessage;}
	
	/**This will get the exit value, but waits no more than timeToWait milliseconds*/
	public int getExitValue(long millisToWait){
		//wait for it to finish first
		if(exitValue != NULL) return exitValue;
		try{
			boolean acquired = finishedSemaphore.tryAcquire(millisToWait, TimeUnit.MILLISECONDS);
			
			if(!acquired){
				return ERROR;
			}
		}
		catch(InterruptedException ex){
			ex.printStackTrace();
			return NULL;
		}
		return exitValue;
	}
	
	public void setExitStatus (int exitStatus){
		exitValue = exitStatus;
	}
	public Socket getSocket(){return socket;}
	public void setSocket(Socket s){socket = s;}
	public void setObjectOutputStream(ObjectOutputStream oos){output = oos;}
	public void setObjectInputStream(ObjectInputStream ios){input = ios;}
	public Message getReplyMessage(){
		try{
			replyReceived.acquire();
			return replyMessage;
			}
		catch(InterruptedException ex){
			ex.printStackTrace();
			return replyMessage;
		}
	}
	
	//here we make a direct connection to the target of this message (we dont use network)
	private void makeDirectConnection(Message message) throws IOException, ClassNotFoundException{
		ConnectionManager cm = networkController.getConnectionManager();
		//here we have to wait for the response from someone on the network
		//who holds the id in message in their space
		socket = cm.waitForNewConnection(message.getTargetID(), this, output, input);
	}
	
	
	//******* Conversation methods ************************************************
	
	private void startConversation(Message messageToSend) throws IOException, ClassNotFoundException{
		System.out.println("in startConversation()");
		switch (messageToSend.getMessageCategory()){
		
			case JOIN: {handleJoinMessageType(messageToSend);break;}
			case FILE: {fileConversation(messageToSend);break;}
			case UPDATE: {updateConversation(messageToSend);break;}
			case TEST: {testConversation(messageToSend);break;}
			case EXIT: {exitConversation(messageToSend);break;}
			case KEYWORD: keywordConversation(messageToSend);break;
			default :
				System.out.println("Received message not recognized. Type: " 
						+ messageToSend.getMessageType().toString());
				exitValue = ERROR;
		}
	}
	
	//////////////////JOIN//////////////////////////////////////
	//join convos, like "I want to join"
	private void handleJoinMessageType(Message message) throws IOException,ClassNotFoundException{
		output.writeObject(message);
		switch (message.getMessageType()){
			case HOST_VERIFICATION:
				handleHostVerification(message);
				break;
			default:
				output.writeObject(message);
			
		}
	}
	
	private void handleHostVerification(Message message) throws IOException,ClassNotFoundException {
		System.out.println("Client: in host verification");
		
		output.writeObject(message);
		output.flush();
		Message reply = (Message)input.readObject();
		switch(reply.getMessageType()){
			case HOST_AVAILABLE:
				break;
			case HOST_UNAVAILABLE:
				System.out.println("Host not ready..." + targetAddress.getHostName());
				exitValue = FAILED;
				break;
		}
		
	}
//	
//	private void handleFindPredecessor(Message message) throws IOException{
//		output.writeObject(message);
//	}
//
//	private void handleFindSuccessor(Message message) throws IOException{
//		output.writeObject(message);
//	}
	
	////////////////FILE////////////////////////////////////////////
	//file convos, like "gimme a file"
	private void fileConversation(Message messageToSend) throws IOException, ClassNotFoundException{
		UploadManager uploadManager;
		
		switch(messageToSend.getMessageType()){
		case PUBLISH_FILE:
			if(directConnection) throw new RuntimeException("No direct connection on publish.");
			//remember that we published it
			uploadManager = networkController.getUploadManager();
			//this should have already been done...
	//		uploadManager.shareFile(messageToSend.getFileInfo());
			output.writeObject(messageToSend);
			output.flush();
			System.out.println("PUBLISH message just sent");
			break;
		case RETRACT_FILE:
//			if(directConnection) throw new RuntimeException("No direct connection on retract.");
			//we are no longer going to upload this file
//			uploadManager = networkController.getUploadManager();
			//this is already done
//			uploadManager.removeSharedFile(messageToSend.getFileID());
			output.writeObject(messageToSend);
			output.flush();
			
			retractFileConversation(messageToSend);
			
			break;
		case LOOKUP_FILE:
			if(directConnection) throw new RuntimeException("No direct connection on lookup.");
			out.println("under LOOKUP_FILE in Client");
			lookupFileConversation(messageToSend);
			break;
			
		case DOWNLOAD_FILE:
			if(!directConnection) throw new RuntimeException("Direct connection required on download.");
//			downloadFileConversation(messageToSend);
			//temporary...
			output.writeObject(messageToSend);
			output.flush();
			startDownload(messageToSend.getFileID());
			break;
		default:
			System.out.println("wrong message type");
			exitValue = ERROR;
		}
	}
	
	//we are looking up a file (in fact, here we wait for a response to our message
	private void lookupFileConversation(Message messageToSend) 
			throws IOException, ClassNotFoundException{
		output.writeObject(messageToSend);
		output.flush();
		//now wait for reply from someone else (have to switch sockets)
		makeDirectConnection(messageToSend);//makes new socket and we block
		Message reply = (Message)input.readObject();
		
		System.out.println("lookup file message sent");
		//and interpret message
		switch(reply.getMessageType()){
		case SEND_FILE_HOLDER:
			// save key in hash table, so we remember who has it for later
			DownloadManager downloadManager = networkController.getDownloadManager();
			downloadManager.registerLookedUpFile(reply.getFileID(), reply.getFileInfoVector());
			System.out.println("lookup success");
			break;
		case LOOKUP_FAILED:
			System.out.println("lookup failed");
			exitValue = FAILED;
			break;
		default:
			System.out.println("wrong message type");
			exitValue = ERROR;
		}
		System.out.println("from Client: lookup complete");
	}
	
	public void retractFileConversation(Message messageToSend)
			throws IOException, ClassNotFoundException{
		makeDirectConnection(messageToSend);
		
		System.out.println("about to wait for challenge message");

		Message reply = (Message)input.readObject();
		
		switch(reply.getMessageType()){
		case RETRACT_SUCCEEDED:
			//it's possible that he won't care about the challenge message
			break;
		case RETRACT_FAILED:
			//may occur if the file was never published
			exitValue = FAILED;
			break;
		case CHALLENGE_MESSAGE:
			//normal execution comes here
			byte[] challengeDecoded = meetChallenge(reply.getChallengeMessage());
			Message challengeReply = Message.getChallengeReply(challengeDecoded);
			System.out.println("about to send challenge response");
			output.writeObject(challengeReply);
			output.flush();
			//wait to see if if challenge succeeded
			Message secondReply = (Message)input.readObject();
			switch(secondReply.getMessageType()){
			case RETRACT_SUCCEEDED:
				break;
			case RETRACT_FAILED:
				exitValue = FAILED;
				break;
			default:
				System.out.println("wrong message type: " + reply.getMessageType());
				exitValue = ERROR;	
			}
			break;
		default:
			System.out.println("wrong message type: " + reply.getMessageType());
			exitValue = ERROR;	
		}
	}
	
	//communiation for the download (the actually reading of bytes occurs in DownloadManager.Download
	public void downloadFileConversation(Message messageToSend) 
			throws IOException, ClassNotFoundException{
		System.out.println("in downloadFileConversation()");
		output.writeObject(messageToSend);
		output.flush();
		
		//now get a reply (no need to switch sockets since direct connection required)
		Message reply = (Message)input.readObject();
		switch(reply.getMessageType()){
		case UPLOAD_FILE:
			System.out.println("under UPLOAD_FILE");
			startDownload(messageToSend.getFileID());
			break;
		case DOWNLOAD_FAILED:
			System.out.println("download failed");
			exitValue = FAILED;
			break;
		default:
			System.out.println("wrong message type");
			exitValue = ERROR;
		}
	}
	
	/**basically, this Client object will be used just for communication, it will not
	 * be used for downloading the file image.  This thread will make a new thread,
	 * an instance of DownloadManager.Download, and it will get the bytes of the file.
	 * It will start Download running and later wait for it to finish.*/	
	public void startDownload(long fileID) throws IOException, ClassNotFoundException{
		System.out.println("in startDownload, FILE_DATA message version");

		//make all this try-catch and stuff for the file writing
		BufferedOutputStream bos = null;
		//first find the file pathname
		try{
//			File path = networkController.getDownloadManager().getFilePath(fileID);
			bos = null;//new BufferedOutputStream(new FileOutputStream(path));
			
			//we loop until we see a DOWNLOAD_FINISHED or DOWNLOAD_FAILED, where we break
			downloadLoop:
			while(true){
				Message reply = (Message)input.readObject();
				switch(reply.getMessageType()){
				case FILE_DATA://contains all the file's data
					System.out.println("reading FILE_DATA");
					//write it to the file
					DownloadManager downloadManager = networkController.getDownloadManager();
					downloadManager.saveFilePiece(reply.getFileID(), 
							reply.getStartingIndex(), reply.getFileData());
//					byte[] fileData = reply.getFileData();
//					bos.write(fileData);
//					bos.flush();
					continue downloadLoop;
				case DOWNLOAD_FINISHED:
					System.out.println("reading DOWNLOAD_FINISHED");
					//exitValue will actually be set later
					//exitValue = SUCCEEDED;
					break downloadLoop;
				case DOWNLOAD_FAILED:
					System.out.println("DOWNLOAD_FAILED");
					exitValue = FAILED;
					break downloadLoop;
				default:
					System.out.println("wrong message type");
					exitValue = ERROR;
				}
				break downloadLoop;
			}
		}
		catch(FileNotFoundException ex){
			ex.printStackTrace();
		}
		catch(IOException ex){
			ex.printStackTrace();
		}
		finally{
			//here we try to close it as we leave
			if(bos != null){
				bos.close();
			}
		}
	}
	
	//////////////////UPDATE///////////////////////////////////////
	//update convos, like "let me check my table"
	private void updateConversation(Message messageToSend) throws IOException,ClassNotFoundException{
		switch(messageToSend.getMessageType()){
		case SEND_YOUR_PRED:
			handleSendYourPred(messageToSend);
			break;
		default:
			output.writeObject(messageToSend);
		}
	}
	
	/** This method has to be called separately because we want a reply back.
	 * 
	 */
	public void handleSendYourPred(Message messageToSend) throws IOException, ClassNotFoundException{
		output.writeObject(messageToSend);
		output.flush();
		Message reply = (Message)input.readObject();
		replyMessage = reply;
		replyReceived.release();
	}
	
	
	//////////////////TEST///////////////////////////////////////
	//testing messages such as test suite tests or testing connection
	private void testConversation(Message messageToSend) throws IOException{
		output.writeObject(messageToSend);
		output.flush();
	}
	/////////////////////////EXIT/////////////////////////////////////
	//for things like "I am leaving"
	private void exitConversation(Message messageToSend) throws IOException{
		output.writeObject(messageToSend);
		output.flush();
	}
	/////////////////////////KEYWORD/////////////////////////////////////
	
	private void keywordConversation(Message messageToSend) 
			throws IOException, ClassNotFoundException {
		switch(messageToSend.getMessageType()){
		case ASSOCIATE_KEYWORD:
			associateKeywordConversation(messageToSend);
			break;
		case UNASSOCIATE_KEYWORD:
			unassociateKeywordConversation(messageToSend);
			break;
		case GET_FILES_FOR_KEYWORD:
			getFilesForKeywordConversation(messageToSend);
			break;
		default:
			System.out.println("wrong message type");
			exitValue = ERROR;			
		}
	}
	private void associateKeywordConversation(Message messageToSend) 
			throws IOException, ClassNotFoundException{
		//first lets send the message
		output.writeObject(messageToSend);	
		output.flush();	
		
		//wait for direct response (not through network)
		makeDirectConnection(messageToSend);
		
		//now get the reply
		Message reply = (Message)input.readObject();
		
		switch(reply.getMessageType()){
		case ASSOCIATE_FINISHED:
			exitValue = SUCCEEDED;
			break;
		case ASSOCIATE_FAILED:
			exitValue = FAILED;
			break;
		default:
			System.out.println("wrong message type");
			exitValue = ERROR;	
		}
	}
	private void unassociateKeywordConversation(Message messageToSend)
			throws IOException, ClassNotFoundException{
		//first lets send the message
		output.writeObject(messageToSend);
		output.flush();
		
		//wait for direct response (not through network)
		makeDirectConnection(messageToSend);
		
		//now get the reply
		Message reply = (Message)input.readObject();
		
		switch(reply.getMessageType()){
		case UNASSOCIATE_FINISHED:
			//this shouldn't happen, i just put it in here for completeness
			exitValue = SUCCEEDED;
			break;
		case UNASSOCIATE_FAILED:
			//other host may not even send the challenge if association doesnt exist
			exitValue = FAILED;
			break;
		case CHALLENGE_MESSAGE:
			//normal execution comes here
			byte[] challengeDecoded = meetChallenge(reply.getChallengeMessage());
			Message challengeReply = Message.getChallengeReply(challengeDecoded);
			System.out.println("about to send challenge response");
			output.writeObject(challengeReply);
			output.flush();
			//wait to see if if challenge succeeded
			Message secondReply = (Message)input.readObject();
			switch(secondReply.getMessageType()){
			case UNASSOCIATE_FINISHED:
				break;
			case UNASSOCIATE_FAILED:
				exitValue = FAILED;
				break;
			default:
				System.out.println("wrong message type: " + reply.getMessageType());
				exitValue = ERROR;	
			}
			break;
		default:
			System.out.println("wrong message type");
			exitValue = ERROR;	
		}
	}
	private void getFilesForKeywordConversation(Message messageToSend)
			throws IOException, ClassNotFoundException{
		//first lets send the message
		System.out.println("about to send keyword lookup");
		output.writeObject(messageToSend);
		output.flush();
		
		if (this.exitValue == ERROR){
			System.out.println("From Lookup Keyword:Could not connect...");
			return;
		}
		
		//wait for direct response (not through network)
		System.out.println("about to make new connection");
		makeDirectConnection(messageToSend);
		
		//now get the reply
		System.out.println("about to read keyword lookup response");
		Message reply = (Message)input.readObject();

		KeywordManager keywordManager = networkController.getKeywordManager();
		switch(reply.getMessageType()){
		case GET_FILES_FOR_KEYWORD_FINISHED:
			//save the keyword lookup
			System.out.println("about to post keyword lookup");
			keywordManager.postKeywordLookup( //we have the other guy send us back the keyword to be sure
					reply.getKeyword(), reply.getFileInfoVector());
			exitValue = SUCCEEDED;
			break;
		case GET_FILES_FOR_KEYWORD_FAILED:
			//if it failed, we should still post that we have nothing for the keyword
			keywordManager.removeKeywordLookup(messageToSend.getKeyword());
			exitValue = FAILED;
			break;
		default:
			System.out.println("wrong message type");
			exitValue = ERROR;	
		}		
	}

//	/** we call this when we are expecting a challenge message*/
//	private void handleChallengeMessage() 
//			throws IOException, ClassNotFoundException{
//		Message reply = (Message)input.readObject();
//		
//		switch(reply.getMessageType()){
//		case CHALLENGE_MESSAGE:
//			byte[] challengeDecoded = meetChallenge(reply.getChallengeMessage());
//			Message challengeReply = Message.getChallengeReply(challengeDecoded);
//			System.out.println("about to send challenge response");
//			output.writeObject(challengeReply);
//			output.flush();
//			return;
//		default:
//			System.out.println("wrong message type: " + reply.getMessageType());
//			exitValue = ERROR;	
//		}
//	}
	
	private byte[] meetChallenge(byte[] challengeMessage){
		System.out.println("in meetChallenge()");
		try{
			//we need to decode this with the private key
			PrivateKey privateKey = networkController.getKeyPair().getPrivate();
			Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.DECRYPT_MODE, privateKey);
			
			return cipher.doFinal(challengeMessage);
			
//			byte[] challengeResponse = null;
//			
//			int offset = 0;
//			int bytesRemaining = challengeMessage.length;
//			int blockSize = cipher.getBlockSize();
//			System.out.println(" block size " + blockSize);
//			boolean finished = false;
//			while(!finished){
//				if(bytesRemaining > blockSize){
//					cipher.update(challengeMessage, 
//							offset, 
//							offset + blockSize);
//					offset += blockSize;
//					bytesRemaining -= blockSize;
//				}
//				else{
//					challengeResponse = cipher.doFinal(
//							challengeMessage, 
//							offset, 
//							challengeMessage.length);
//					finished = true;
//				}
//			}
//			System.out.println("past loop");
//			
//			return challengeResponse;
		}
		catch(NoSuchPaddingException ex){
			ex.printStackTrace();
			return null;
		}
		catch(NoSuchAlgorithmException ex){
			ex.printStackTrace();
			return null;
		}
		catch(InvalidKeyException ex){
			ex.printStackTrace();
			return null;
		}
		catch(IllegalBlockSizeException ex){
			ex.printStackTrace();
			return null;
		}
		catch(BadPaddingException ex){
			ex.printStackTrace();
			return null;
		}
	}
	
}//end class Client
