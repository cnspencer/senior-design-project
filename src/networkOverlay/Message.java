package networkOverlay;

import java.io.Serializable;
import java.net.InetAddress;
import java.util.Hashtable;
import java.util.Vector;
import java.security.*;

public class Message implements Serializable{	
	private final MessageType messageType;
	private transient static InetAddress myIP;
	private transient static int myPort;
	private int forwardCounter = 50;
	
	public static enum MessageCategory {
		JOIN, FILE, UPDATE, EXIT, TEST, UNDEFINED, KEYWORD
	}
	public static enum MessageType{
		//******* messages to send through the network **********
		//for entering the network
		
		/**to be sent out on start up to the host(s) in the config file*/
		HOST_VERIFICATION,
		HOST_AVAILABLE,
		HOST_UNAVAILABLE,
		/**for finding the predecessor and getting finger table and keys*/
		FIND_PRED,
		/**more like I_AM_PRED. sends the finger table and file keys (Last hop)*/
		FOUND_PRED,
		FIND_SUCCESSOR,//TODO: implement in stage 3, like what the prof said in class
		FOUND_SUCCESSOR,//these are for checking the new guy's finger table
		
		//for leaving the network
		EXIT,//TODO: not implemented
		
		//for file sharing
		/**This lets other hosts know that we have the file.  It stores the record
		 * of this knowledge at the peer who has fileID in his space.*/
		PUBLISH_FILE,
		/**retracts a previously published file*/
		RETRACT_FILE,
		/**finds the peers who holds the key for the file in order to find the peer who has the file*/
		RETRACT_FAILED,
		RETRACT_SUCCEEDED,
		LOOKUP_FILE,
		/**sends the peer who has the file*/
		SEND_FILE_HOLDER,
		/**indicates that the file hasn't been published*/
		LOOKUP_FAILED, 
//		/**this message indicates that this socket will be used for the actual file image transfer*/
//		DOWNLOAD_SOCKET,
		/**message indicates we want to download (this socket still used for messages)*/
		DOWNLOAD_FILE,
		/**message indicates that we have the file and will upload it*/
		UPLOAD_FILE, //acknowledge DOWNLOAD_FILE and begin sending file
		/**we don't have the file*/
		DOWNLOAD_FAILED,
		CANCEL_FILE_DOWNLOAD,//TODO: canceling not implemented yet
		CANCEL_FILE_UPLOAD,
		/**sent on the message-socket to indicate that the file is finished downloading*/
		DOWNLOAD_FINISHED,
		
		/**temporary for now...just holds the whole data of a file lol*/
		FILE_DATA,
		
		/**For making sure the server responds back directly instead of through the network.
		 * When a Server recieves this message, it means that there is a Client on the system
		 * who sent a message and the response just arrived (from a different host, because
		 * it was forwarded).
		 */
		
		//Update
		PEER_EXISTS,	//points to correction
		FIND_NEW_PRED,  //Incase my pred can't be contacted
		SEND_YOUR_PRED, // For forwarding failure algorithm
		I_AM_YOUR_PRED,	//when new peer joins, he has to inform his successor about his existance
		PEER_LEFT,      //For messages when download failed. Inform keyholder that one of the guy has left.
		
		ESTABLISH_CONNECTION,
		SEND_KEYHOLDER,
		
		//Exit
		PRED_LEAVING,
		SUCCESSOR_LEAVING,
		
		//Test
		TEST_CONNECTION,
		TESTSUITE_TEST,
		
		//Keyword
		ASSOCIATE_KEYWORD,
		ASSOCIATE_FAILED,
		ASSOCIATE_FINISHED,
		
		UNASSOCIATE_KEYWORD,
		UNASSOCIATE_FAILED,
		UNASSOCIATE_FINISHED,
		
		GET_FILES_FOR_KEYWORD,
		GET_FILES_FOR_KEYWORD_FINISHED,
		GET_FILES_FOR_KEYWORD_FAILED,
		
		//Probe messages
		PROBE_MESSAGE,
		
		//for authentication
		CHALLENGE_MESSAGE,
		CHALLENGE_REPLY,
	//	CHALLENGE_SUCCEEDED,
//		CHALLENGE_FAILED
		
	}
	
//	private FingerTable2 fingerTable2 = null;
	//private FingerTable fingerTable = null;
	private FingerTable2 fingerTable2 = null;
	
	//this is for the file keys given when a person joins network
	private Hashtable<Long, Vector<FileInfo>> fileKeyTable = null;
	private Hashtable<String, Vector<FileInfo>> keywordFilesTable = null;
	
	private Long fileID = null;//I prefer int because it's short
	private InetAddress sourceIP = null;//for when we are entering
	private int sourcePort = 0;
	private Long sourceID = 0L;
	
	
	private Long successorID = 0L;
	private InetAddress successorIP = null;
	private int successorPort = 0;
	private Long fileHolderID = 0L;
	
	private Long optimalID = 0L;
	private InetAddress optimalIP = null;
	private int optimalPort = 0;
	
	private Long predID = 0L;
	private InetAddress predIP = null;
	private int predPort = 0;
	
	private InetAddress lastIP = null;
	private int lastPort = 0;
	
	private int fingerTableUpdateIndex = 0;
	private FileInfo fileInfo = null;
	private Vector<FileInfo> fileInfos = null;
	
	private Long targetID = null;
	
	/**temporary, this is the array of the data for this package*/
	private byte[] fileData = null;
	/**the starting index of the data in this message*/
	private int startingIndex = -1;
	/**the number of bytes we want to download*/
	private long bytesToDownload = -1;
	
	private String keyword = null;
	private PublicKey publicKey = null;
	
	//for authenticaton
	private byte[] challengeMessage = null;
	
	//other classes don't get to use the constructor
	protected Message(MessageType messageType){
		this.messageType = messageType;
		this.lastIP =myIP;
		this.lastPort = myPort;
		this.sourceIP = myIP;
		this.sourcePort = myPort;
		this.sourceID = Hasher.hashPeerLong(myIP, myPort);
	}
	
	//************** FACTORY METHODS *******************************//
	//I made a bunch of static "factory" methods instead of constructors
	//this is how they did the JOptionPaneClass
	//I thought it was necessary because I other classes to have to use like
	//10 arguments in the constructor where most of them are null, and I wanted
	//each different kind of message to have it's own specially-named method  to instantiate
	//the class, which isn't possible with constructors
	public boolean isPointsToCorrectionNeeded() {
		switch (messageType) {
			case HOST_VERIFICATION:
			case FIND_PRED:
			case FOUND_PRED:
			case I_AM_YOUR_PRED:
			case FIND_SUCCESSOR:
			case FOUND_SUCCESSOR:
			case PEER_EXISTS:
			case SEND_FILE_HOLDER:
			case LOOKUP_FAILED:
			case DOWNLOAD_FILE:
			case UPLOAD_FILE:
			case FIND_NEW_PRED:
			case SEND_YOUR_PRED:
			case TEST_CONNECTION:
			case TESTSUITE_TEST:
			case PROBE_MESSAGE:
				return false;
			default: return true;
		}
				
				
		
	}
	public MessageCategory getMessageCategory(){
		switch (messageType) {
			//join category
			case HOST_VERIFICATION:
			case HOST_AVAILABLE:
			case HOST_UNAVAILABLE:
			case FIND_PRED:
			case FOUND_PRED:
			case FIND_SUCCESSOR:
			case FOUND_SUCCESSOR:
				return MessageCategory.JOIN;
			
				
			//file category
			case PUBLISH_FILE:
			case RETRACT_FILE:
			case LOOKUP_FILE:
			case LOOKUP_FAILED:
			case DOWNLOAD_FILE:
			case UPLOAD_FILE:
			case DOWNLOAD_FAILED:
			case CANCEL_FILE_DOWNLOAD:
			case CANCEL_FILE_UPLOAD:
				return MessageCategory.FILE;
				
			//update
			case PEER_EXISTS:
			case FIND_NEW_PRED:
			case SEND_YOUR_PRED:
			case I_AM_YOUR_PRED:
			case PEER_LEFT:
				return MessageCategory.UPDATE;
			
			//exit
			case PRED_LEAVING:
			case SUCCESSOR_LEAVING:
			case EXIT: 
				return MessageCategory.EXIT;
				
			//Test
			case TEST_CONNECTION:
			case TESTSUITE_TEST:
			case PROBE_MESSAGE:
				return MessageCategory.TEST;
				
			//Keyword
			case ASSOCIATE_KEYWORD:
			case ASSOCIATE_FAILED:
			case ASSOCIATE_FINISHED:			
			case UNASSOCIATE_KEYWORD:
			case UNASSOCIATE_FAILED:
			case UNASSOCIATE_FINISHED:			
			case GET_FILES_FOR_KEYWORD:
			case GET_FILES_FOR_KEYWORD_FINISHED:
			case GET_FILES_FOR_KEYWORD_FAILED:
				return MessageCategory.KEYWORD;
				
			default: return MessageCategory.UNDEFINED;
		}
	}
	
	
//	public static Message getDownloadSocketMessage(Long fileID){
//		Message message = new Message(MessageType.DOWNLOAD_SOCKET);
//		message.fileID = fileID;
//		message.targetID = fileID;
//		return message;
//		
//	}	
	
	public static Message getHostVerificationMessage(InetAddress sourceIP, int port, long targetID){
		checkIfNull(sourceIP, port, targetID);
		Message message = new Message(MessageType.HOST_VERIFICATION);
		message.sourceIP = sourceIP;
		message.sourcePort = port;
		message.targetID= targetID;
		return message;
	}

	public static Message getHostAvailableMessage(long targetID){
		checkIfNull(targetID);
		Message message = new Message(MessageType.HOST_AVAILABLE);
		message.targetID= targetID;
		return message;
	}

	public static Message getHostUnavailableMessage(long targetID){
		checkIfNull(targetID);
		Message message = new Message(MessageType.HOST_UNAVAILABLE);
		message.targetID= targetID;
		return message;
	}
	
	public static Message getFindPredMessage(InetAddress sourceIP, int sourcePort){
			Message message = new Message(MessageType.FIND_PRED);
			message.sourceIP = sourceIP;
			message.sourcePort = sourcePort;
//			message.sourceID = Hasher.hashPeerString(sourceIP.getHostAddress()+":"+sourcePort);
	//		message.sourceID = "0X" + Long.toHexString(FingerTable2.convertHexToLongValue(Hasher.hashPeerString(sourceIP + ":" + sourcePort))-1);
			message.sourceID = Hasher.hashPeerLong(sourceIP + ":"+ sourcePort);
			message.targetID = Hasher.hashPeerLong(sourceIP +":" + sourcePort)-1;
			return message;
		}
	
//	public static Message getFoundPredMessage(InetAddress sourceIP, int sourcePort,
//		FingerTable fingerTable, Hashtable<Integer, FileInfo> fileKeyTable){
//		Message message = new Message(MessageType.FOUND_PRED);
//		message.sourceIP = sourceIP;
//		message.sourcePort = sourcePort;
////		message.sourceID = Hasher.hashPeerString(sourceIP.getHostAddress()+":"+sourcePort);
//		message.sourceID = Hasher.hashPeerInt(sourceIP, sourcePort);
//		message.fingerTable = fingerTable;
//		message.fileKeyTable = fileKeyTable;
//		return message;
//	}
	//override
	public static Message getFoundPredMessage(InetAddress sourceIP, int sourcePort,
		FingerTable2 fingerTable2, Hashtable<Long, Vector<FileInfo>> fileKeyTable, 
		Hashtable<String, Vector<FileInfo>> keywordFilesTable,long targetID){
		Message message = new Message(MessageType.FOUND_PRED);
		message.sourceIP = sourceIP;
		message.sourcePort = sourcePort;
//			message.sourceID = Hasher.hashPeerString(sourceIP.getHostAddress()+":"+sourcePort);
		message.sourceID = Hasher.hashPeerLong(sourceIP + ":" + sourcePort);
		message.fingerTable2 = fingerTable2;
		message.fileKeyTable = fileKeyTable;
		message.keywordFilesTable = keywordFilesTable;
		message.targetID = targetID;
		return message;
		}
	
//	public static Message getFindSuccessorMessage(InetAddress sourceIP, int sourcePort, 
//			String sourceID, String successorID, int fingerTableIndex){
//		Message message = new Message(MessageType.FIND_SUCCESSOR);
//		message.sourceIP = sourceIP;
//		message.sourcePort = sourcePort;
////		message.sourceID = sourceID;
////		message.successorID = successorID;
//		message.sourceID = (int)Hasher.convertHexToLongValue(sourceID);
//		message.successorID = (int)Hasher.convertHexToLongValue(successorID);
//		message.fingerTableUpdateIndex = fingerTableIndex; 
//		return message;
//	}
	
	//overloaded
	public static Message getFindSuccessorMessage(InetAddress sourceIP, int sourcePort, long sourceID, 
			long successorID, int fingerTableIndex){
		Message message = new Message(MessageType.FIND_SUCCESSOR);
		message.sourceIP = sourceIP;
		message.sourcePort = sourcePort;
		message.sourceID = sourceID;
		message.successorID = successorID;
		message.targetID = successorID;
		message.fingerTableUpdateIndex = fingerTableIndex; 
		return message;
	}
	
	public static Message getFoundSuccessorMessage(Message recievedMessage, InetAddress successorIP,
		int successorPort, long successorID){
		Message message = new Message(MessageType.FOUND_SUCCESSOR);
		message.sourceIP = recievedMessage.getSourceIP();
		message.sourcePort = recievedMessage.getSourcePort();
		message.sourceID = recievedMessage.getSourceID();
		message.targetID = recievedMessage.getSourceID();
		message.successorID = successorID;
		message.successorIP = successorIP;
		message.successorPort = successorPort;
		message.fingerTableUpdateIndex = recievedMessage.fingerTableUpdateIndex;
		return message;
	}
	
//	public static Message getFoundSuccessorMessage(Message recievedMessage, InetAddress successorIP,
//		int successorPort, String successorID){
//		Message message = new Message(MessageType.FOUND_SUCCESSOR);
//		message.sourceIP = message.getSourceIP();
//		message.sourcePort = message.getSourcePort();
//		message.sourceID = message.getSourceID();
//		message.successorID = (int)Hasher.convertHexToLongValue(successorID);
//		message.successorIP = successorIP;
//		message.successorPort = successorPort;
//		message.fingerTableUpdateIndex = recievedMessage.fingerTableUpdateIndex;
//		return message;
//		}

//	public static Message getExitMessage(String sourceID){
//		Message message = new Message(MessageType.EXIT);
//		message.sourceID = (int)Hasher.convertHexToLongValue(sourceID);
//		return message;
//	}
	
	//overloaded
	public static Message getExitMessage(long sourceID){
		Message message = new Message(MessageType.EXIT);
		message.sourceID = sourceID;
		return message;
	}

	public static Message getPublishFileMessage(FileInfo fileInfo, InetAddress ip, int port){
		Message message = new Message(MessageType.PUBLISH_FILE);
		message.fileInfo = fileInfo;
		message.sourceIP = ip;
		message.sourcePort = port;
		message.sourceID = Hasher.hashPeerLong(ip +":" + port);
		message.targetID = fileInfo.getID();
		message.fileID = message.targetID;
		return message;
	}
	
	public static Message getRetractFileMessage(long fileID){
		Message message = new Message(MessageType.RETRACT_FILE);
		message.fileID = fileID;
		message.targetID = fileID;
		message.fileHolderID = Hasher.hashPeerLong(myIP + ":" + myPort );
		return message;
	}
	
	//overloaded
	public static Message getRetractFileMessage(FileInfo fileInfo){
		Message message = new Message(MessageType.RETRACT_FILE);
		message.fileID = fileInfo.getID();
		message.targetID = fileInfo.getID();
		message.fileInfo = fileInfo;
		message.fileHolderID = Hasher.hashPeerLong(myIP + ":" + myPort );
		return message;		
	}
	//overloaded
	public static Message getRetractFileMessage(long fileID,long fileHolderID){
		Message message = new Message(MessageType.RETRACT_FILE);
		message.fileID = fileID;
		message.targetID = fileID;
		message.fileHolderID = fileHolderID;
		return message;
	}
	
	public static Message getRetractFailedMessage(){
		Message message = new Message(MessageType.RETRACT_FAILED);
		return message;
	}
	public static Message getRetractSucceededMessage(){
		Message message = new Message(MessageType.RETRACT_SUCCEEDED);
		return message;
	}
	
//	@Deprecated
//	public static Message getLookupFileMessage(String fileHolderID, InetAddress sourceIP, int sourcePort){
//		Message message = new Message(MessageType.LOOKUP_FILE);
//		message.fileHolderID = Long.valueOf(fileHolderID).intValue();
//		message.sourceIP = sourceIP;
//		message.sourcePort = sourcePort;
//		message.targetID = message.fileHolderID;
//		return message;
//	}
//	//overloaded
	
	//we dont really care who the file holder is, just want to lookup file
	public static Message getLookupFileMessage(long fileID, InetAddress sourceIP, int sourcePort, String file){
		checkIfNull(fileID, sourceIP, sourcePort, file);
		Message message = new Message(MessageType.LOOKUP_FILE);
		message.fileID = fileID;
		message.targetID = fileID;
		message.sourceIP = sourceIP;
		message.sourcePort = sourcePort;
		message.fileInfo = new FileInfo(file, fileID, sourceIP, sourcePort);
		return message;
	}
	
	public static Message getSendFileHolderMessage(Vector<FileInfo> fileInfos){
		Message message = new Message(MessageType.SEND_FILE_HOLDER);
		message.fileInfos = fileInfos;
		message.fileID = fileInfos.get(0).getID();
		return message;
	}

//	//TODO: is it even necessary to give the fileID?
//	public static Message getLookupFailedMessage(String fileID){
//		Message message = new Message(MessageType.LOOKUP_FAILED);
//		message.fileID = (int)Hasher.convertHexToLongValue(fileID);
//		return message;
//	}	//overloaded
//	
	
	public static Message getLookupFailedMessage(long fileID){
		Message message = new Message(MessageType.LOOKUP_FAILED);
		message.fileID = fileID;
		return message;
	}

//	@Deprecated //Charles made this, so we use Charles's version lol
//	public static Message getDownloadFileMessage(String fileID){
//		Message message = new Message(MessageType.DOWNLOAD_FILE);
//		message.fileID = Long.valueOf(fileID).intValue();
//		message.targetID = message.fileID;
//		return message;
//	}
//	//overloaded
	
	public static Message getDownloadFileMessage(long fileID, long fileHolderID, 
			int startingIndex, long bytesToDownload){
		Message message = new Message(MessageType.DOWNLOAD_FILE);
		message.fileID = fileID;
		message.targetID = fileHolderID;
		message.startingIndex = startingIndex;
		message.bytesToDownload = bytesToDownload;
		return message;
	}
	
//	public static Message getUploadFileMessage(String fileID){
//		Message message = new Message(MessageType.UPLOAD_FILE);
//		message.fileID = Long.valueOf(fileID).intValue();
//		return message;
//	}	//overloaded
	
	public static Message getUploadFileMessage(long fileID){
		
		Message message = new Message(MessageType.UPLOAD_FILE);
		message.fileID = fileID;
		message.targetID = fileID;
		return message;
	}
	
//	public static Message getDownloadFailedMessage(String fileID){
//		Message message = new Message(MessageType.DOWNLOAD_FAILED);
//		message.fileID = (int)Hasher.convertHexToLongValue(fileID);
//		return message;
//	}//overloaded
	
	public static Message getDownloadFailedMessage(long fileID){
		Message message = new Message(MessageType.DOWNLOAD_FAILED);
		message.fileID = fileID;
		return message;
	}

//	public static Message getCancelFileDownloadMessage(String fileID){
//		Message message = new Message(MessageType.CANCEL_FILE_DOWNLOAD);
//		message.fileID = (int)Hasher.convertHexToLongValue(fileID);
//		return message;
//	}//overloaded
	
	public static Message getCancelFileDownloadMessage(long fileID){
		Message message = new Message(MessageType.CANCEL_FILE_DOWNLOAD);
		message.fileID = fileID;
		return message;
	}
	
//	public static Message getCancelFileUploadMessage(String fileID){
//		Message message = new Message(MessageType.CANCEL_FILE_UPLOAD);
//		message.fileID = (int)Hasher.convertHexToLongValue(fileID);
//		return message;
//	}
	//overloaded
	public static Message getCancelFileUploadMessage(long fileID){
		Message message = new Message(MessageType.CANCEL_FILE_UPLOAD);
		message.fileID = fileID;
		return message;
	}
	
	public static Message getDownloadFinishedMessage(long fileID){
		Message message = new Message(MessageType.DOWNLOAD_FINISHED);
		message.fileID = fileID;
		return message;
	}
	
	/**targetID here is the number that the Client blocked in ConnectionManager uses to ID himself
	 * though it also happens to be the ID of the file*/	
	public static Message getEstablishConnectionMessage(long targetID){
		Message message = new Message(MessageType.ESTABLISH_CONNECTION);
		message.targetID = targetID;
		return message;
	}
	
	/**temporary, FILE_DATA message just holds all the data of the file*/
	public static Message getFileDataMessage(FileInfo fileInfo, byte[] bytes, int startingIndex){
		Message message = new Message(MessageType.FILE_DATA);
		message.fileID = fileInfo.getID();
		message.fileInfo = fileInfo;
		message.fileData = bytes;
		message.startingIndex = startingIndex;
		return message;
	}
	
	

	// *******************************************************************
	// ************************* UPDATE **********************************
	// *******************************************************************
	
	
	
	public static Message getPeerExistsMessage(InetAddress optimalIP, int optimalPort, long optimalID, long targetID){
		Message message = new Message(MessageType.PEER_EXISTS);
		message.optimalIP = optimalIP;
		message.optimalPort = optimalPort;
		message.optimalID = optimalID;
		message.targetID = targetID;
		return message;
	}
	
	public static Message getFindNewPredMessage(InetAddress sourceIP, int sourcePort){
		Message message = new Message(MessageType.FIND_NEW_PRED);
		message.sourceIP = sourceIP;
		message.sourcePort = sourcePort;
		message.sourceID = Hasher.hashPeerLong(sourceIP +":"+ sourcePort);
		message.targetID = Hasher.hashPeerLong(sourceIP+ ":" + sourcePort)-1;
		return message;
	}
	
	public static Message getSendYourPredMessage(InetAddress sourceIP,int sourcePort,Long targetID){
		Message message = new Message(MessageType.SEND_YOUR_PRED);
		message.sourceIP = sourceIP;
		message.sourcePort = sourcePort;
		message.sourceID =  Hasher.hashPeerLong(sourceIP+ ":" + sourcePort);
		message.targetID = targetID;
		return message;
	}
	public static Message getSendYourPredMessage(InetAddress sourceIP,int sourcePort,
			InetAddress predIP,int predPort,Long targetID){
		Message message = new Message(MessageType.SEND_YOUR_PRED);
		message.sourceIP = sourceIP;
		message.sourcePort = sourcePort;
		message.sourceID =  Hasher.hashPeerLong(sourceIP + ":" + sourcePort);
		message.predIP = predIP;
		message.predPort = predPort;
		message.predID =  Hasher.hashPeerLong(predIP+ ":" + predPort);
		message.targetID = targetID;
		return message;
	}
	
	public static Message getIAmYourPredMessage(InetAddress myIP, int myPort, long myID, long targetID){
		Message message = new Message(MessageType.I_AM_YOUR_PRED);
		message.sourceIP = myIP;
		message.sourcePort = myPort;
		message.sourceID = myID;
		message.targetID = targetID;
		return message;
	}
	
	public static Message getPeerLeft(InetAddress peerLeftIP, int peerLeftPort,  long fileID){
		Message message = new Message(MessageType.PEER_LEFT);
		message.sourceIP = peerLeftIP;
		message.sourcePort = peerLeftPort;
		message.sourceID = Hasher.hashPeerLong(peerLeftIP +":"+ peerLeftPort);
		message.fileID = fileID;
		message.targetID = fileID;
		return message;
	}
	// *******************************************************************
	// ************************** TEST ***********************************
	// *******************************************************************
	
	public static Message getTestConnectionMessage(InetAddress sourceIP, int sourcePort, Long targetID){
		Message message = new Message(MessageType.TEST_CONNECTION);
		message.sourceIP = sourceIP;
		message.sourcePort = sourcePort;
		message.sourceID = Hasher.hashPeerLong(sourceIP+ ":" + sourcePort);
		message.targetID = targetID;
		return message;
	}
	
	public static Message getProbeMessage(InetAddress sourceIP, int sourcePort, int fingerTableIndex, Long targetID){
		Message message = new Message(MessageType.PROBE_MESSAGE);
		message.sourceIP = sourceIP;
		message.sourcePort = sourcePort;
		message.sourceID = Hasher.hashPeerLong(sourceIP + ":" + sourcePort);
		message.fingerTableUpdateIndex = fingerTableIndex;
		message.targetID = targetID;
		return message;
	}

	// *******************************************************************
	// ************************** EXIT ***********************************
	// *******************************************************************
	//Sent to your successor
	// Tell him you are going and that his pred is now your pred
	public static Message getPredLeavingMessage(InetAddress predIP, int predPort,Long targetID){
		Message message = new Message(MessageType.PRED_LEAVING);
		message.sourceIP = myIP;
		message.sourcePort = myPort;
		message.sourceID = Hasher.hashPeerLong(myIP + ":" + myPort);
		message.predIP = predIP;
		message.predPort = predPort;
		message.predID = Hasher.hashPeerLong(predIP + ":" + predPort);
		message.targetID = targetID;
		return message;
	}
	
	//Sent to pred
	// Tell him you are leaving and that he is responsible for the keys now
	public static Message getSuccessorLeavingMessage(InetAddress successorIP, int successorPort, 
			 Hashtable<Long, Vector<FileInfo>> fileKeyTable,Hashtable<String, Vector<FileInfo>> keywordFiles, long targetID){
		Message message = new Message(MessageType.SUCCESSOR_LEAVING);
		message.sourceIP = myIP;
		message.sourcePort = myPort;
		message.sourceID = Hasher.hashPeerLong(myIP+ ":" + myPort);
		message.successorIP = successorIP;
		message.successorPort = successorPort;
		message.successorID = Hasher.hashPeerLong(successorIP+ ":" + successorPort);
		message.fileKeyTable = fileKeyTable;
		message.keywordFilesTable = keywordFiles;
		message.targetID = targetID;
		return message;
	}

	// *******************************************************************
	// ************************** KEYWORD ********************************
	// *******************************************************************

	public static Message getAssociateKeywordMessage(String keyword, FileInfo fileInfo,
			InetAddress sourceIP, int sourcePort, PublicKey pk){
		Message message = new Message(MessageType.ASSOCIATE_KEYWORD);
		message.fileInfo = fileInfo;
		message.keyword = keyword;
		message.sourceIP = sourceIP;
		message.sourcePort = sourcePort;
		message.sourceID = Hasher.hashPeerLong(sourceIP + ":" + sourcePort);
		message.targetID = Hasher.hashPeerLong(keyword);
		message.publicKey = pk;
		return message;
	}
	
	public static Message getAssociateFailedMessage(){
		Message message = new Message(MessageType.ASSOCIATE_FAILED);
		return message;
	}
	
	public static Message getAssociateFinishedMessage(){
		Message message = new Message(MessageType.ASSOCIATE_FINISHED);
		return message;
	}
	
	public static Message getUnassociateKeywordMessage(String keyword, FileInfo fileInfo,
			InetAddress sourceIP, int sourcePort){
		Message message = new Message(MessageType.UNASSOCIATE_KEYWORD);
		message.fileInfo = fileInfo;
		message.keyword = keyword;
		message.sourceIP = sourceIP;
		message.sourcePort = sourcePort;
		message.sourceID = Hasher.hashPeerLong(sourceIP + ":" + sourcePort);
		message.targetID = Hasher.hashPeerLong(keyword);
		return message;
	}
	
	public static Message getUnassociateFailedMessage(){
		Message message = new Message(MessageType.UNASSOCIATE_FAILED);
		return message;
	}
	
	public static Message getUnassociateFinishedMessage(){
		Message message = new Message(MessageType.UNASSOCIATE_FINISHED);
		return message;
	}
	
	public static Message getGetFilesForKeywordMessage(String keyword,
			InetAddress sourceIP, int sourcePort){
		Message message = new Message(MessageType.GET_FILES_FOR_KEYWORD);
		message.keyword = keyword;
		message.sourceIP = sourceIP;
		message.sourcePort = sourcePort;
		message.targetID = Hasher.hashPeerLong(keyword);
		return message;
	}
	public static Message getGetFilesForKeywordFinishedMessage(
			String keyword, Vector<FileInfo> fileInfos){
		Message message = new Message(MessageType.GET_FILES_FOR_KEYWORD_FINISHED);
		message.keyword = keyword;
		message.fileInfos = fileInfos;
		return message;
	}
	public static Message getGetFilesForKeywordFailedMessage(){
		Message message = new Message(MessageType.GET_FILES_FOR_KEYWORD_FAILED);
		return message;
	}

	// *******************************************************************
	// ***************** AUTHENTICATION CHALLENGES ***********************
	// *******************************************************************
	
	public static Message getChallengeMessage(byte[] challengeMessage){
		Message message = new Message(MessageType.CHALLENGE_MESSAGE);
		message.challengeMessage = challengeMessage;
		return message;
	}	
	public static Message getChallengeReply(byte[] challengeReply){
		Message message = new Message(MessageType.CHALLENGE_REPLY);
		message.challengeMessage = challengeReply;
		return message;
	}
//	public static Message getChallengeSucceeded(){
//		Message message = new Message(MessageType.CHALLENGE_SUCCEEDED);
//		return message;
//	}
//	public static Message getChallengefailed(){
//		Message message = new Message(MessageType.CHALLENGE_FAILED);
//		return message;
//	}
	
	
	
	////get methods (I didn't see any need to include set methods)////
	public void setTargetID(long target){targetID = target;}
	
	public MessageType getMessageType(){return messageType;}
	//public FingerTable getFingerTable(){return fingerTable;}
	public FingerTable2 getFingerTable2(){return fingerTable2;}
	public long getFileID(){return fileID;}
	public Hashtable<Long, Vector<FileInfo>> getFileKeyTable(){return fileKeyTable;}
	public Hashtable<String, Vector<FileInfo>> getKeywordFilesTable() {return keywordFilesTable;};
	public InetAddress getSourceIP(){return sourceIP;}
	public int getSourcePort(){return sourcePort;}
	public InetAddress getSuccessorIP(){return successorIP;}
	public int getSuccessorPort(){return successorPort;}
	public long getSourceID(){return sourceID;}
	public long getSuccessorID(){return successorID;}
	public long getFileHolderID(){return fileHolderID;}
	public FileInfo getFileInfo(){return fileInfo;}
	public int getfingerTableUpdateIndex(){return fingerTableUpdateIndex;} 
	public long getTargetID(){return targetID;}
	public byte[] getFileData(){return fileData;}
	public int getStartingIndex(){return startingIndex;}
	public long getBytesToDownload(){return bytesToDownload;}
	
	public InetAddress getOptimalIP(){return optimalIP;}
	public int getOptimalPort(){return optimalPort;}
	public long getOptimalID(){return optimalID;}

	public InetAddress getPredIP(){return predIP;}
	public int getPredPort(){return predPort;}
	public long getPredID(){return predID;}
	
	public InetAddress getLastIP(){return lastIP;}
	public int getLastPort(){return lastPort;}
	
	public Vector<FileInfo> getFileInfoVector(){return fileInfos;}
	public String getKeyword(){return keyword;}
	
	public byte[] getChallengeMessage(){return challengeMessage;}

	public void setMyselfAsLast(){
		lastIP = myIP;
		lastPort = myPort;
	}
	
	public void setLastIP(InetAddress IP){lastIP = IP;}
	public void setLastPort(int port){lastPort = port;}
		
	public static void setMyIP(InetAddress IP){myIP = IP;}
	public static void setMyPort(int port){myPort = port;}
	
	public PublicKey getPublicKey(){return publicKey;}
	
	public boolean countMessageForwards() {
		forwardCounter--;
		if (forwardCounter < 0)
			return false;
		else
			return true;
	}
	
	//get String representations of the IDs
//	public String getSourceIDString(){return "0X" + (Integer.toHexString(sourceID));}
//	public String getSuccessorIDString(){return "0X" + (Integer.toHexString(successorID));}
//	public String getFileHolderIDString(){return "0X" + (Integer.toHexString(fileHolderID));}
//	

	
	//this will throw an exception if any are null
	//used to make sure user does not pass null values
	public static void checkIfNull(Object... objs){
		for(int i = 0; i < objs.length; i++){
			if(objs[i] == null)
				throw new NullPointerException();
		}
	}
}
