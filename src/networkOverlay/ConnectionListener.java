package networkOverlay;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

//one of the 2 classes that Server was split into
public class ConnectionListener implements Runnable{
	private NetworkController networkController = null;
	private ServerSocket serverSocket = null;
	private int port = -1;
	private Socket client;
	private boolean runningFlag = true;
//	private static Semaphore networkControllerListenerSync, mutexList;
//	private static ArrayList<Message> messagesReceived;//from all clients, in arbitrary order
	
	public ConnectionListener(int portNumber, NetworkController nc){
		networkController = nc;
		
//		networkControllerListenerSync = new Semaphore(0);
//		mutexList = new Semaphore(1);
//		messagesReceived = new ArrayList<Message>();
	  	port = portNumber;
	  	//We must call the start() method from oustide the contructor
		//the old method listenSocket();
	  	
	  	// Find an available port, continue incrementing until port number is free
	  	boolean freePortFound = false;
	  	while (!freePortFound){
			try{
				serverSocket = new ServerSocket(port);
				freePortFound = true;
		    }
			catch (IOException e) {
				port++;
			}
		}
	  	new Thread(this).start();
	}
	
	public void run(){
		while(runningFlag){
			try{
//				ClientHandler clientHandler = new ClientHandler(serverSocket.accept(), this);
				Server clientHandler = new Server(serverSocket.accept(), networkController);
				//Server clientHandler = new Server(server.accept());
				Thread clientAcceptThread = new Thread(clientHandler);
				clientAcceptThread.start();
			}
			catch (IOException e) {
				System.out.println("Accept failed: " + port);
				if (runningFlag)
					System.exit(-1);
			}
		}		
	}
	
	//outside classes call this to get messages from this object
//	public Message waitForMessage () { //should we rethrow the InterruptedException?
//		try {
//			networkControllerListenerSync.acquire();
//			mutexList.acquire();
//			Message message =  messagesReceived.remove(0);
//			mutexList.release();
//			return message;
//		} 
//		catch (InterruptedException ex){
//			//it's fine to get an InterruptedException
//		}
//		return null; //indicates error
//	}
	
	//for ClientHandler threads to add messages
//	public void addMessage(Message message){
//		try{
//			mutexList.acquire();
//	        messagesReceived.add(message);
//			mutexList.release();
//	        networkControllerListenerSync.release();
//		} catch (InterruptedException ex){
//			ex.printStackTrace();
//		}
//	}
	

	//be sure to close sockets if this object gets destroyed
	protected void finalize(){
		try{
			if(!serverSocket.isClosed())
				serverSocket.close();
			if(!client.isClosed())
				client.close();
		}
		catch (IOException e) {
			System.out.println("Could not close socket");
			System.exit(-1);
		}
	}
	
	
	///////MAIN only for testing ConnectionListener, ClientHandler, and ClientCharles////
//	public static void main(String[] args){
//		int portNum = 201;
//		//get local IP address string
//		String ipAddress = null;
//		try{
//			ipAddress = InetAddress.getLocalHost().getHostAddress();
//		} 
//		catch(Exception ex){ex.printStackTrace();}
//		
//		//setup server
//		System.out.println("Starting test");
//		ConnectionListener cl = new ConnectionListener(portNum, null);
//		//Thread clThread = new Thread(cl);
//		//clThread.start();
//		System.out.println("Server started");
//		
//		//setup client with a message
//		//sClient client = new Client(ipAddress, portNum, "hello");		//Thread clientThread = new Thread(client);
//		//clientThread.start();//send message
//		System.out.println("Client started");
//		System.out.println( cl.waitForMessage() );
//		
//		
//		
//		//other client
//		//Client client2 = new Client(ipAddress, portNum, "yeah");		//Thread clientThread2 = new Thread(client2);
//		//clientThread2.start();//send message
//		System.out.println("Client started");
//		
//		System.out.println( cl.waitForMessage() );
//		System.exit(0);//necessary to shut down other threads
//	}
	
	public int getPort() {return port;}
	public void shutdown() {
		runningFlag = false;
		try{
			serverSocket.close();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
	}
}
