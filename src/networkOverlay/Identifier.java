package networkOverlay;

/*
 * I use this class to encapsulate the Identifier
 * that is hashed and used to ID clients in finger table
 * -Charles
 */

import java.net.InetAddress;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Identifier {
	private static MessageDigest hasher;
	static {		
		//create hasher
		try{
			hasher = MessageDigest.getInstance("SHA-1");
		}
		catch(NoSuchAlgorithmException ex){
			ex.printStackTrace();
		}		
	}
	
	private int id;
	private InetAddress ipAddress;
	private int portNumber;
	
	public Identifier(InetAddress ipAddress, int portNumber){
		this.ipAddress = ipAddress;
		this.portNumber = portNumber;
		id = hashPeerInt(ipAddress, portNumber);
	}
	
	public Identifier(int num){
		id = num;
	}
	
	//for testing
	public Identifier(){}

	//tests if this object is in between the first and second, exclusive
	//starts just above the first identifier and goes till just below the second
	//and accounts for overflow
	public boolean isBetween(Identifier first, Identifier second){
		if(first.getID() < second.getID()){//the "normal" case
			return getID() > first.getID() && //larger than lower (first) one AND
				getID() < second.getID(); //less than higher (second) one
		}
		if(first.getID() > second.getID()){//special case
			return getID() > first.getID() ||//greater than higher (first) one OR
				getID() < second.getID();//less than lower (second) one
		}
		//if(first.getID() == second.getID()
		return false;
	}
	
	//static version that takes 3 arguments
	//the "middle" must be "between" the first and second, as per the above
	public static boolean isBetween(int first, int middle, int second){
		if(first < second){//the "normal" case
			return middle > first && //larger than lower (first) one AND
				middle < second; //less than higher (second) one
		}
		if(first > second){//special case
			return middle > first ||//greater than higher (first) one OR
				middle < second;//less than lower (second) one
		}
		//if(first == second
		return false;		
	}
	
	/****** set and get methods ***************************/
	
	public int getID(){return id;}
	public String getIDString(){
		String resultHex = "0X" + Integer.toHexString(id);
		return resultHex;
	}
	public InetAddress getIPAddress(){return ipAddress;}
	public int getPortNumber(){return portNumber;}
	
	//for testing only
	public void setID(int newID){id = newID;}
	
	/******** static methods for hashing ******************/
	
	//hashPeer
	//turns an array of bytes into a hashed int
	public static int hashPeerInt(byte[] inputBytes){
		byte[] digestBytes;
		digestBytes = hasher.digest(inputBytes);
		if(digestBytes.length != 20)//160 bits from SHA-1
			//shouldn't get thrown, so we can make it a RuntimeException
			throw new RuntimeException("Hash Algorithm failed " + digestBytes.length);
		
		int result = 0;
		//we compress the 160 bits down to 32 bits
		//or, the 20 bytes down to 1 int
		for(int row = 0; row < 5; row++){//five "rows" of 32 bits to me anded in
			//basically we can split the 160 bits into 5 groups of 32 (thats why there are 5 iterations)
			//and we XOR all of them together (that's what ^= does) may consider +=
			//and we have to left shift to get each byte in the right part of the int
			
			result ^= digestBytes[4*row + 0] << 0;
			result ^= digestBytes[4*row + 1] << 8;
			result ^= digestBytes[4*row + 2] << 16;
			result ^= digestBytes[4*row + 3] << 24;
		}
		return result;		
	}
	
	//hashPeerString
	//This method will hash a String into an int hashcode
	public static String hashPeerString(String peer){
		byte[] inputBytes;		
		inputBytes = peer.getBytes();//each character is a byte, may not work with accented chars etc.
		
		int result = hashPeerInt(inputBytes);		
		
		String resultHex = "0X" + Integer.toHexString(result);
		return resultHex;		
	}
	
	//turns a String into a hashed int
	public static int hashPeerInt(String peer){
		byte[] inputBytes;		
		inputBytes = peer.getBytes();//each character is a byte, may not work with accented chars etc.
		
		return hashPeerInt(inputBytes);
	}
	
	//turns an InetAddres and portNumber into a hashed int
	public static int hashPeerInt(InetAddress ipAddress, int portNumber){
		byte[] ipBytes, portBytes, inputBytes;
		ipBytes = ipAddress.getAddress();
		portBytes = new byte[4];
		//an int is 4 bytes, so with the following code, we simply
		//fill portBytes with one of those bytes from portNumber
		portBytes[0] = (byte)(0x000000ff & portNumber);
		portBytes[1] = (byte)(0x000000ff & (portNumber >> 8));
		portBytes[2] = (byte)(0x000000ff & (portNumber >> 16));
		portBytes[3] = (byte)(0x000000ff & (portNumber >> 24));
		
		//merge 2 arrays into 1
		inputBytes = new byte[ipBytes.length + portBytes.length];
		for(int i = 0; i < ipBytes.length; i++){
			inputBytes[i] = ipBytes[i];
		}
		for(int i = 0; i < portBytes.length; i++){
			inputBytes[ipBytes.length + i] = portBytes[i];
		}

		return hashPeerInt(inputBytes);
	}

	////equals() and toString()//////////
	@Override
	public boolean equals(Object obj){
		if(obj.getClass() != Identifier.class) return false;
		Identifier identifier = (Identifier)obj;
		return id == identifier.id &&
			ipAddress.equals(identifier.ipAddress) &&
			portNumber == identifier.portNumber;
	}
	
	@Override
	public String toString(){
		return getIDString();
	}
	
	///////////MAIN for testing this class////////////////
	public static void main(String[] args){
		Identifier i1, i2, i3;
		i1 = new Identifier();
		i2 = new Identifier();
		i3 = new Identifier();
		i1.setID(3);
		i2.setID(7);
		i3.setID(5);
		
		if(i3.isBetween(i1, i2))//for the simple case, i1 should be less than i2
			System.out.println("test 1 succeeded");
		else 
			System.out.println("test 1 failed");
		
		i1.setID(849999);
		i2.setID(0);
		i3.setID(850000);//i3 is now "between" i1 and i2

		if(i3.isBetween(i1, i2))//for the special case
			//we are asking if i3 is between i1 and i2
			//that is, it can be any value starting at 600000, going all the way
			//up to the maximum int value, all the way up from the minimum int value
			//and up to -400000
			System.out.println("test 2 succeeded");
		else 
			System.out.println("test 2 failed");
	}
}
