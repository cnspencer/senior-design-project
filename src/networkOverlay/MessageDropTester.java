package networkOverlay;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.util.Random;
import java.util.StringTokenizer;


public class MessageDropTester {
	
	private static Random random;
	private static long seed;
	private static int numProcesses;
	
	private static long[] longIDs;
	private static String[] hashedIDs;
	private static Process[] serviceProcesses;
	
	private static InputStream[] serviceInputStreams;
	private static OutputStream[] serviceOutputStreams;
	private static InputStream[] serviceErrorStreams;
	
	private static Thread[] readerThreads;
	private static Thread[] writerThreads;
	private static Thread[] errorThreads;
	
	private static Integer[] messagesSent;
	private static Integer[] messagesReceivedBack;
	
	//Keeps a tally of the amount of files that have finished being published and retracted
	private static int[] workCounter;
	
	//Keeps tally of all the randomly generated ports
	private static int[] ports;
	
	public static void main(String args[]) throws Exception {
		//while(true) {
			checkArgs(args);
			createRandom(args);
			createServicesProcesses();
			forceFingerTableUpdate();
			addShutdownHook();
			terminateSomeProcesses();
			try{
				Thread.sleep(5000);
			}
			catch (InterruptedException ex){
				ex.toString();
			}
			try{
				Thread.sleep(600000);
			}
			catch (InterruptedException ex){
				ex.toString();
			}
			printMessageDroppingResults();
			terminateServicesProcesses();
			System.out.println("[API] \t\t Total Messages Sent: " + messagesSent[0]);
			System.out.println("[API] \t\t Total Messages Received Back: " + messagesReceivedBack[0]);
			System.out.println("[API] \t\t Percentage of Messages Delivered: " + ((messagesReceivedBack[0]*100)/messagesSent[0]) + "%");
		//}
	}
	
	//Make sure that a seed has been entered by the user
	private static void checkArgs(String args[]) {
		if(args.length != 1) {
			System.out.println("The program expects a seed as an argument. For example: java TestSuite 100");
			System.exit(-1);
		}
	}
	
	//Terminate all child processes when the test suite is shut down
	//This doesn't work with the terminate button in eclipse though
	//See https://bugs.eclipse.org/bugs/show_bug.cgi?id=38016 as it's a problem with eclipse
	private static void addShutdownHook() {
		System.out.println("[API] Creating shutdown hook");
		Runtime.getRuntime().addShutdownHook(new Thread() {
		    public void run() {
				for(Process process : serviceProcesses) {
					process.destroy();
				}
		    }
		});
	}
	
	//Create our pseudo-random generator based on the inputed seed
	private static void createRandom(String args[]) {
		
		random = new Random(seed);
		//We want to create a random amount of processes, between 5 and 20
		//numProcesses = random.nextInt(16) + 5;
		numProcesses = 26;
		//numProcesses = 2;
		
		System.out.println("[API] The API is going to create " + numProcesses + " different servers.");
		
		ports = new int[numProcesses];
		seed = Long.parseLong(args[0]);
		
		//The default port is 7456 and is written in servers.txt so we initially create one with this port number
		//and all subsequent servers will connect to this server
		int port = 7456;
		ports[0] = port;
		//Create all the random ports
		for(int count = 1; count < numProcesses; count++) {
			do {
				//We want our port to be higher than 7000 just to be safe
				port = random.nextInt(100) + 7000;
			}
			while(searchArray(ports, port) >= 0);
			ports[count] = port;
		}
	}
	
	private static void createServicesProcesses() throws IOException, InterruptedException {
		longIDs					= new long[numProcesses];
		hashedIDs				= new String[numProcesses];
		
		serviceProcesses		= new Process[numProcesses];
		serviceInputStreams		= new InputStream[numProcesses];
		serviceOutputStreams	= new OutputStream[numProcesses];
		serviceErrorStreams		= new InputStream[numProcesses];
		
		readerThreads			= new Thread[numProcesses];
		writerThreads			= new Thread[numProcesses];
		errorThreads			= new Thread[numProcesses];
		
		//The first counter makes sure all servers have finished being created
		//The second counter keeps track that all files have finished publishing
		//The third counter keeps track that all downloads have finished
		//The fourth counter keeps track that all finles have finished being retracted
		//The fifth count keeps track of if a server has finished being initialized
		//The sixth counter keeps track of all the keywords that have finish being associated
		//The seventh counter keeps track of all the files that have finished downloading
		//The seventh counter keeps track of how many files have finished being looked up
		workCounter				= new int[8];
		workCounter[0]			= 0;
		workCounter[1]			= 0;
		workCounter[2]			= 0;
		workCounter[3]			= 0;
		workCounter[4]			= -1;
		workCounter[5]			= 0;
		workCounter[6]			= 0;
		workCounter[7]			= -1;
		
		messagesSent 			= new Integer[1];
		messagesSent[0]			= 0;
		messagesReceivedBack	= new Integer[1];
		messagesReceivedBack[0]			= 0;
		Runtime runtime = Runtime.getRuntime(); 
		
		for(int count = 0; count < numProcesses; count++) {
			longIDs[count] = Hasher.hashPeerLong(InetAddress.getLocalHost()+ ":" + ports[count]);
			hashedIDs[count] = Long.toHexString(longIDs[count]);
			
			System.out.println("[API] Creating server " + hashedIDs[count] + " with port " + ports[count]);
			
			serviceProcesses[count] = runtime.exec("java networkOverlay.Services TEST");
			serviceInputStreams[count] = serviceProcesses[count].getInputStream();
			serviceOutputStreams[count] = serviceProcesses[count].getOutputStream();
			serviceErrorStreams[count] = serviceProcesses[count].getErrorStream();
			serviceOutputStreams[count].write((ports[count] + "\n").getBytes());
			serviceOutputStreams[count].flush();
			
			readerThreads[count] = new ReaderThread(serviceInputStreams[count], "Stdout", hashedIDs[count], hashedIDs,  workCounter, messagesSent,messagesReceivedBack);
			readerThreads[count].start();
			
			//Create threads to display any errors
			errorThreads[count] = new ReaderThread(serviceErrorStreams[count], "Stderr", hashedIDs[count], hashedIDs, 
				workCounter, messagesSent,messagesReceivedBack);
			errorThreads[count].start();
			
			waitWhileInitializing(count);
		}
	}
	
	private static void waitWhileInitializing(int position) throws InterruptedException {
		while(workCounter[4] < position) {
			Thread.sleep(100);
		}
	}
	
	private static void forceFingerTableUpdate() {
		try{
			
			for(int count = 0; count < numProcesses; count++) {
				writerThreads[count] = new WriterThread(serviceOutputStreams[count], "Update", random);
				writerThreads[count].start();
				Thread.sleep(1000);
			}
		}
		catch (InterruptedException ex){
			
		}
	}
	
	
	//Gracefully terminate the last peer created
	private static void terminateSomeProcesses() throws InterruptedException {
		//System.out.println("[API] Gracefully terminating " + hashedIDs[numProcesses-1]);
		gracefullyTerminate(2, random);
		Thread.sleep(500);
		gracefullyTerminate(4, random);
		Thread.sleep(500);
		gracefullyTerminate(6, random);
		Thread.sleep(500);
		gracefullyTerminate(8, random);
		Thread.sleep(500);
		gracefullyTerminate(9, random);
		Thread.sleep(500);
		gracefullyTerminate(11, random);
		Thread.sleep(500);
		gracefullyTerminate(13, random);
		Thread.sleep(500);
		gracefullyTerminate(18, random);
		Thread.sleep(500);
		gracefullyTerminate(20, random);
		Thread.sleep(500);
		gracefullyTerminate(21, random);
		Thread.sleep(500);
		gracefullyTerminate(23, random);
		Thread.sleep(500);
		gracefullyTerminate(24, random);
		Thread.sleep(500);
	}
	

	
	//Terminate program once all files have been retracted
	private static void terminateServicesProcesses() throws InterruptedException {
		for(int count = 0; count < numProcesses; count++) {
			gracefullyTerminate(count, random);
			Thread.sleep(1000);
		}
		
		//Wait for the OS to free up the port
		Thread.sleep(1000);
	}
	
	private static void gracefullyTerminate(int position, Random random) {
		writerThreads[position] = new WriterThread(serviceOutputStreams[position], "Exit", random);
		writerThreads[position].start();
	}
	
	public static void printMessageDroppingResults(){
		for(int count = 0; count < numProcesses; count++) {
			writerThreads[count] = new WriterThread(serviceOutputStreams[count], "Printresults", random);
			writerThreads[count].start();
			try{
				Thread.sleep(100);
			}
			catch (Exception ex){
				
			}
		}
	}
	
	private static void nonGracefullyTerminate(int position, Random random) {
		serviceProcesses[position].destroy();
	}
	
	public static int searchArray(Object[] haystack, Object needle) {
		for(int count = 0; count < haystack.length; count++) {
			if(haystack[count].equals(needle)) {
				return count;
			}
		}
		return -1;
	}
	
	public static int searchArray(int[] haystack, int needle) {
		for(int count = 0; count < haystack.length; count++) {
			if(haystack[count] == needle) {
				return count;
			}
		}
		return -1;
	}
	
	public static int searchArray(long[] haystack, long needle) {
		for(int count = 0; count < haystack.length; count++) {
			if(haystack[count] == needle) {
				return count;
			}
		}
		return -1;
	}
}

class WriterThread extends Thread {
	OutputStream input;
	String message;
	Random random;
	
	public WriterThread(OutputStream myInput, String myMessage, Random myRandom) {
		input = myInput;
		message = myMessage;
		random = myRandom;
	}
	
	public void run() {
		
//		//Sleep for a random amount of time before sending the message
//		try {
//			Thread.sleep(random.nextInt(1000));
//		} catch (InterruptedException e1) {
//			System.out.println("[API] Thread failed to sleep.");
//		}
		
		try {
			input.write((message + "\n").getBytes());
		} catch (IOException e) {
			System.out.println("[API] Failed to send message to child.");
		}
		
		try {
			input.flush();
		} catch (IOException e) {
			System.out.println("[API] Failed to force buffer to write to child.");
		}
	}
}

	//Class used to create threads to display messages on screen
	class ReaderThread extends Thread {
		InputStream output;
		String prefix;
		int[] messagesDone;
		String myHashedID;
		String[] hashedIDs;
		int[] workCounter;
		Integer[] messagesSent;
		Integer[] messagesReceived;
		
public ReaderThread(InputStream myOutput, String myPrefix, String myMyHashedID, String[] myHashedIDs, int[] myWorkCounter, Integer[] messagesSent, Integer[] messagesReceived) {
			output				= myOutput;
			prefix				= "[" + myPrefix + "] ";
			myHashedID			= myMyHashedID;
			hashedIDs			= myHashedIDs;
			workCounter			= myWorkCounter;
			this.messagesSent 		= messagesSent;
			this.messagesReceived	= messagesReceived;
		}
		
		public void run() {
			BufferedReader brCleanUp = new BufferedReader (new InputStreamReader (output));
			String line;
			try {
				while ((line = brCleanUp.readLine ()) != null) {
					System.out.println (prefix + line);
					countMessagesSent(line);
					countMessagesReceived(line);
					verifyInitializationCompletion(line);
				}
				
				
			} catch (IOException e) {
				System.out.println("[API] Failed to display buffer to screen.");
			}

			try {
				brCleanUp.close();
			} catch (IOException e) {
				System.out.println("[API] Failed to close buffer.");
			}
		}
		private void countMessagesSent(String line) {
			if(line.indexOf("Probe Messages Sent: ") > -1) {
				
				StringTokenizer token = new StringTokenizer(line);
				//Don't need the first three tokens
				token.nextToken();
				token.nextToken();
				token.nextToken();
				//token.nextToken();
				
				messagesSent[0] += Integer.parseInt(token.nextToken());
			}
		}
		
		private void countMessagesReceived(String line) {
			if(line.indexOf("Probe Messages Received: ") > -1) {
				
				StringTokenizer token = new StringTokenizer(line);
				
				//Don't need the first three tokens
				token.nextToken();
				token.nextToken();
				token.nextToken();
				//token.nextToken();
				
				messagesReceived[0] += Integer.parseInt(token.nextToken());
			}
		}
		
		private void verifyInitializationCompletion(String line) {
			if(line.indexOf("Finished setting up peer:") > -1) {
				
				StringTokenizer token = new StringTokenizer(line);
				
				//Don't need the first four tokens
				token.nextToken();
				token.nextToken();
				token.nextToken();
				token.nextToken();
				
				System.out.println("[API] " + Long.toHexString(Long.parseLong(token.nextToken())) + " has finished being initialized.");
				workCounter[4]++;
				System.out.println("[API] Initialization counter: " + workCounter[4]);
			}
		}
		
	}
			
//			services[18].getNetworkController().getMessageTargetDecider().stopRunning();
//			services[18].getNetworkController().gracefulExitForMessageTests();
//			services[23].getNetworkController().getMessageTargetDecider().stopRunning();
//			services[23].getNetworkController().gracefulExitForMessageTests();
//			services[24].getNetworkController().getMessageTargetDecider().stopRunning();
//			services[24].getNetworkController().gracefulExitForMessageTests();
//			
//			Thread.sleep(200000);
//			
//			for (int i = 0; i <services.length; i++){
//					services[i].getNetworkController().getMessageTargetDecider().stopRunning();
//			}
//			for (int i = 0; i <services.length; i++){
//					services[i].getNetworkController().getMessageTargetDecider().printResults();
//			}
//			
//			System.exit(0);
//		}
//		catch (Exception ex){ ex.printStackTrace();}
//	}
	