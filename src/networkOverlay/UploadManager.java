package networkOverlay;

import java.io.File;
import java.net.InetAddress;
import java.util.HashSet;
import java.util.Hashtable;

/**
 * UploadManager
 * This class manages which file we have published and are available for
 * others to download as well as the files we are currently uploading.
 */

public class UploadManager {
	//the max size of a upload packet, in bytes
	public static final int MAX_UPLOAD_PACKET_SIZE = 4096;//sounded like a good enough number
	
	/**list of files we have published and are willing to upload, maps from fileID to fileInfo*/
	Hashtable<Long, FileInfo> publishedFiles = new Hashtable<Long, FileInfo>();
	
//	/**list of current Upload threads, maps from fileID and sourceID to Upload*/
//	Hashtable<FileAndSourceIDs, Upload> uploadThreads = new Hashtable<FileAndSourceIDs, Upload>();
	
	/**the list of files we expect to upload soon.  contains fileID-sourceID pairings
	 */
	HashSet<FileAndSourceIDs> registeredUploads = new HashSet<FileAndSourceIDs>();
	
	NetworkController networkController;
	
	public UploadManager(NetworkController nc){
		networkController = nc;
	}
	
	//this indicates that we are expecting a handleUpload() to be made from sourceID source
//	public Upload registerUpload(long fileID, long sourceID){
//		//must be willing to upload in order to register it
//		if(publishedFiles.containsKey(fileID)){
//			FileAndSourceIDs pairing = new FileAndSourceIDs(fileID, sourceID);
//			synchronized(registeredUploads){
//				registeredUploads.add(pairing);
//			}
//			//make the Upload object
//			Upload upload = new Upload(fileID);
//			uploadThreads.put(pairing, upload);
//
//			System.out.println("upload properly registered");
//			return upload;
//		}
//		System.out.println("upload not registered");
//		return null;
//	}
	
//	public void unregisterUpload(long fileID, long sourceID){
//		FileAndSourceIDs pairing = new FileAndSourceIDs(fileID, sourceID);
//		synchronized(registeredUploads){
//			registeredUploads.remove(pairing);
//		}
//		//remove Upload object
//		uploadThreads.remove(fileID);
//	}
	
	//Server will call this method when it realizes that it may need to start an upload
	//this will check to make sure an upload is expected and, if so, will start a new Upload
//	public void handleUpload(long fileID, Socket socket){
//		long sourceID = Hasher.hashPeerLong(socket.getInetAddress(), socket.getPort());
//		FileAndSourceIDs pairing = new FileAndSourceIDs(fileID, sourceID);
//		if(registeredUploads.contains(pairing)){
//			System.out.println("about to do upload from handleUpload()");
//			//we have it, so get the registered upload
//			Upload upload = uploadThreads.get(pairing);
//			upload.setSocket(socket);
//			
//			//start it up
//			new Thread(upload).start();
//		}
//		else System.out.println("abort upload");
//	}
		
	/** file will be available for uploading, and also the keyholder will have it*/
	public FileInfo publishFile(File file, InetAddress sourceIP, int sourcePort){
		FileInfo fileInfo = shareFile(file, sourceIP, sourcePort);//first make it available (necessary to do this before publish)
		//make the message to send
		Message message = Message.getPublishFileMessage(fileInfo, sourceIP, sourcePort);
		//now send the new message so it can be published
		Client client = new Client(fileInfo.getID(), message, networkController);
		
		//start thread
		System.out.println("from UploadManager: " + sourceIP + " " + sourcePort 
				+ " " + fileInfo.getID() + "file ID in hex: " + Long.toHexString(fileInfo.getID()));
		new Thread(client).start();
		System.out.println("ending publishFile()");
		return fileInfo;
	}
	
	//file will no longer be available, and keyholder must forget about it
	public void retractFile(long fileID){
		//first make it unavailable to upload
		removeSharedFile(fileID);
		//now tell the keyholder to forget about it
		Message message = Message.getRetractFileMessage(fileID);
		Client client = new Client(fileID, message, networkController);
		new Thread(client).start();
		client.getExitValue();
	}
	
	public void retractAllFiles(){
		Long [] keys = new Long[publishedFiles.keySet().size()];
		keys = publishedFiles.keySet().toArray(keys);
		for (int i =0; i< keys.length;i++){
			if (publishedFiles.get(keys[i]).getHolderID().equals(networkController.getFingerTable2().getMyID()) &&
					(!networkController.getOnlineStatus())){
				networkController.removeFileHolder(
						publishedFiles.get(keys[i]).getID(), publishedFiles.get(keys[i]).getHolderID());
			}
			retractFile(keys[i]);

		}
	}
	
	// file be available for uploading
	//returns fileID
//	public int shareFile(FileInfo fileInfo){
//		publishedFiles.put(fileInfo.getID(), fileInfo);
//		return fileInfo.getID();
//	}	
	/**Once this method is complete, the system knows that we are willing to upload the file*/
	public FileInfo shareFile(File file, InetAddress myIP, int myPort){
		FileInfo fileInfo = new FileInfo(file, myIP, myPort, networkController.getKeyPair().getPublic());
		publishedFiles.put(fileInfo.getID(), fileInfo);
		System.out.println("shareFile() ending");
		return fileInfo;
	}
	public FileInfo removeSharedFile(long fileID){
		return publishedFiles.remove(fileID);
	}
	//checks if the file is available for uploading
	public boolean checkIfFileAvailable(long fileID){
		return publishedFiles.containsKey(fileID);
	}
	
	//gets the file info for the shared file
	public FileInfo getFileInfo(long fileID){return publishedFiles.get(fileID);}

	/**This class just encapsulates the fileID and sourceID, so I can use them  both
	 * as a key for hashing.  This is necessary since more than one peer can request
	 * a single file, and more than one file can be requested by a single peer.
	 */
	public class FileAndSourceIDs{
		Long fileID;
		Long sourceID;
		
		public FileAndSourceIDs(long fileID, long sourceID){
			this.fileID = fileID;
			this.sourceID = sourceID;
		}
		
		@Override
		public int hashCode(){
			return fileID.hashCode() ^ sourceID.hashCode();//XOR the bits of the hashcodes together
		}
		
		@Override
		public boolean equals(Object obj){
			if(obj == null) return false;
			if(!(obj instanceof FileAndSourceIDs)) return false;
			FileAndSourceIDs other = (FileAndSourceIDs)obj;
			return this.fileID.equals(other.fileID) &&
				this.sourceID.equals(other.sourceID);
		}
	}
	
}
