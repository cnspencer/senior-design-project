package networkOverlay;

//import java.util.Collection;
import java.util.*;
import java.net.*;
import java.io.*;
import java.security.*;

/**This class just manages the keywords that hash to our space. 
 * It also manages any keyword lookups the user wants to make (in lookUpKeyword)*/
public class KeywordManager {
	private NetworkController networkController;
	
	/**The holderID in FileInfo here is unimportant, only the
	 * fileID, the file name, file size etc  
	 * The user will get the fileID and use that to get all the
	 * file holders for that file.  
	 * There is only 1 FileInfo object for each file here, even if 
	 * there are more than 1 file holder hosts.
	 */
	private Hashtable<String, Vector<FileInfo>> keywordFiles = new Hashtable<String, Vector<FileInfo>>();
	
	private Hashtable<KeywordFile, Integer> keywordFileCounts = new Hashtable<KeywordFile, Integer>();
	
	/**This is where we hold all the keyword lookups for the local user*/
	private Hashtable<String, Vector<FileInfo>> localKeywordLookups = new Hashtable<String, Vector<FileInfo>>();
	
	/**hashes from associatorID to vector of public keys*/
	private Hashtable<Long, PublicKey> publicKeys = new Hashtable<Long, PublicKey>();
	
	/**hashes from associatorID to count of associations that that host has made*/
	private Hashtable<Long, Integer> associatorCounts = new Hashtable<Long, Integer>();
	
	/**database of tuples of associatorID, keyword, and fileID (all count as keys for db table)*/
	private HashSet<KeywordAssociatorFile> masterList = new HashSet<KeywordAssociatorFile>();
	
//	/**database of keyword/file mappings and the count the number of such mappings for a keyword/file pair*/
//	private HashSet<KeywordFile> keywordFiles = new HashSet<KeywordFile>();
	
//	/**hashes from keyword hash to Vector of the challenges we have sent*/
//	private Hashtable<Long, Vector<String>> challengesSent = new Hashtable<Long, Vector<String>>();
	
	public KeywordManager(NetworkController nc){
		networkController = nc;
	}
	
	/**adds a keyword association for a given file. 
	 * The user has to already have the FileInfo object, but this is good
	 * because the system can remember lots of info about the file in 1 hash table
	 */
	public synchronized void associateKeyword(String keyword, FileInfo file, 
			Long associatorID, PublicKey pk){
		KeywordAssociatorFile entry = new KeywordAssociatorFile(keyword, associatorID, file.getID());
		//add only if we don't already have this entry in the database
		if(!masterList.contains(entry)){
			masterList.add(entry);
			//add to keywordFiles
			Vector<FileInfo> fileInfos = keywordFiles.get(keyword);
			if(fileInfos == null){//this means no keyword yet in that table
				fileInfos = new Vector<FileInfo>();
				fileInfos.add(file);
				keywordFiles.put(keyword, fileInfos);
			}
			else if(!fileInfos.contains(file)){//add if not already contained
				fileInfos.add(file);
			}
			//now write down the public key
			if(!publicKeys.containsKey(associatorID))
				publicKeys.put(associatorID, pk);
			if(!associatorCounts.containsKey(associatorID))
				associatorCounts.put(associatorID, 1);//start count at 1
			else{//if we already have the publicKey and a count, just increment the count
				associatorCounts.put(associatorID, 
						associatorCounts.get(associatorID) + 1);
			}
		}
		
		KeywordFile keywordFile = new KeywordFile(keyword, file.getID());
		Integer count = keywordFileCounts.get(keywordFile);
		if(count == null){//if it doesn't have it, make it
			keywordFileCounts.put(keywordFile, 1);//start count off as 1
		}
		else{
			keywordFileCounts.put(keywordFile, ++count);		
		}
		
		//Need this line for the test suite. Don't know where it should belong in this function though
		System.out.println("Associated keyword: " + keyword + " with file: " + file.getFilePath());

		System.out.println("masterList size: " + masterList.size());
		System.out.println("keywordFiles size: " + keywordFiles.size());
		System.out.println("publicKeys size: " + publicKeys.size());
		System.out.println("associatorCounts size: " + associatorCounts.size());
		System.out.println("this associatorCount size: " + associatorCounts.get(associatorID));
		System.out.println("keyword file counts: " + keywordFileCounts.size());
		System.out.println("this keyword file count: " + keywordFileCounts.get(keywordFile));
		
		
		
	/*	if(keywordFiles.containsKey(keyword)){
			Vector<FileInfo> fileInfos = keywordFiles.get(keyword);
			//check if it already contains this keyword-file association
			//and if not, add it
			boolean hasAssociation = false;
			for(int i = 0; i < fileInfos.size(); i++){
				if(fileInfos.get(i).getID().equals(file.getID())){
					hasAssociation = true;
					break;
				}
			}
			//if it doesn't have the association, add it in
			if(!hasAssociation){
				fileInfos.add(file);
				
				System.out.println("Associated keyword: " + keyword + " with file: " + file.getFilePath());
			}
		}
		else{//doesn't contain the key
			Vector<FileInfo> fileInfos = new Vector<FileInfo>();
			fileInfos.add(file);
			keywordFiles.put(keyword, fileInfos);
			
			System.out.println("Associated keyword: " + keyword + " with file: " + file.getFilePath());
		}*/
	}
	
//	/**generate a challenge message*/
//	public String createChallengeMessage(String keyword, PublicKey publicKey){
//		//TODO
//		return null;
//	}
	
//	/**unassociate after keyword challenge*/
//	public void unassociateAfterChallenge(String keyword, String challengeAnswer){
//		
//	}
	
	
	public boolean associationExists(String keyword, Long fileID, 
			Long associatorID){
		boolean returnValue = masterList.contains(new KeywordAssociatorFile(
				keyword, associatorID, fileID));
		System.out.println("associationExists() is returning: " + returnValue);
		System.out.println("there are " + masterList.size() + " in masterList");
		return returnValue;
	}
	
	public PublicKey getPublicKey(Long associatorID){
		//first get all the
		return publicKeys.get(associatorID);
	}

	/**removes keyword association for the file with fileID matching that inf fileInfo*/
	public synchronized void unassociateKeyword(String keyword, FileInfo fileInfo, 
			Long associatorID){		
		//for fail-fast behavior
		if(fileInfo.getID() == null)
			throw new NullPointerException();
		else{
			unassociateKeyword(keyword, fileInfo.getID(), associatorID);
		}
	}
	
	/**removes keyword association for the file, FileInfo object not required.*/
	public void unassociateKeyword(String keyword, Long fileID, 
			Long associatorID){
		System.out.println("entered unassociateKeyword with " + keyword + " " + fileID);
		
		//remove from the master list
		KeywordAssociatorFile triplet = new KeywordAssociatorFile(keyword, associatorID, fileID);
		System.out.println("remove triplet: " + masterList.remove(triplet));
		
		//remove from keywordFiles, but only if no one else has made this association
		//first check if someone else has made this keyword-file association
		KeywordFile keywordFile = new KeywordFile(keyword, fileID);
		Integer kfcount = keywordFileCounts.get(keywordFile);
		if(kfcount == null || kfcount <= 1){//if theres just 1 left, we are getting rid of it
			keywordFiles.remove(keywordFile);//get rid of it
			
			Vector<FileInfo> fileInfos = keywordFiles.get(keyword);
			System.out.println("about to mess with keywordFiles");
			if(fileInfos != null){
				System.out.println("keywordFiles not null");
				for(int i = 0; i < fileInfos.size(); i++){
					System.out.println("i: " + i + " stuff: " + fileInfos.get(i));
					System.out.println(fileID);
					if(fileInfos.get(i).getID().equals(fileID)){
						fileInfos.remove(i);
						System.out.println("fileInfo removed from keywordFiles");
					}
					else{
						System.out.println("fileInfo NOT removed from keywordFiles");
					}
				}
				//after the loop, check if we can remove the entry entirely
				if(fileInfos.size() == 0){
					System.out.println("whole entry removed from keywordFiles");
					keywordFiles.remove(keyword);
				}
			}
		}
		else{//there are still some left, just decrement
			keywordFileCounts.put(keywordFile, --kfcount);
		}
		
		//forget the public key only if count for that host = 0 (the count is # of associations made)
		int count = associatorCounts.get(associatorID) - 1;
		if(count == 0){
			System.out.println("count is 0");
			associatorCounts.remove(associatorID);
			publicKeys.remove(associatorID);
		}
		else{
			System.out.println("count is " + count);
			//put in the newly decremented value
			associatorCounts.put(associatorID, count);
		}
		

		System.out.println("masterList size: " + masterList.size());
		System.out.println("keywordFiles size: " + keywordFiles.size());
		System.out.println("publicKeys size: " + publicKeys.size());
		System.out.println("associatorCounts size: " + associatorCounts.size());
		System.out.println("this associatorCount size: " + associatorCounts.get(associatorID));
		System.out.println("keyword file counts: " + keywordFileCounts.size());
		System.out.println("this keyword file count: " + kfcount);
		
		/*boolean hasRemovedSomething = false;
		if(keywordFiles.containsKey(keyword)){
			Vector<FileInfo> fileInfos = keywordFiles.get(keyword);
			if(fileInfos != null){
				if(fileInfos.size() > 0){
					//for testing
					for(int i = 0; i < fileInfos.size(); i++){
						FileInfo fileInfo = fileInfos.get(i);
						System.out.println(fileInfo.getName() + " " + fileInfo.getID());
					}
					//now scan through and check the fileIDs
					//and if a fileInfo has that idea, remove it and break
					//this may remove all such associations, just in case
					for(int i = 0; i < fileInfos.size(); i++){
						if(fileInfos.get(i).getID().equals(fileID)){
							fileInfos.remove(i);
							System.out.println("unassociating keyword");
							hasRemovedSomething = true;
							//decrement the counter because we just removed one
							//so everything has to get bumped forward
							//and we do want to keep checking the remaining ones
							i--;
							break;
						}
					}
					//for testing
					for(int i = 0; i < fileInfos.size(); i++){
						FileInfo fileInfo = fileInfos.get(i);
						System.out.println(fileInfo.getName() + " " + fileInfo.getID());
					}
				}
				
				//finally, if the fileInfos size is 0, remove the keyword
				if(fileInfos.size() == 0)
					keywordFiles.remove(keyword);
			}
		}
		if(!hasRemovedSomething){
			System.out.println("have not unassociated anything");
		}*/
		System.out.println("leaving unassociateKeyword()");
	}
	
	/**This method is not guaranteed to return a FileInfo for each host
	 * that has published the file with that keyword, but it should return
	 * a FileInfo for each file with that keyword.
	 */
	public Vector<FileInfo> getAllFilesForKeyword(String keyword){
		System.out.println("in getAllFilesForKeyword()");
		return keywordFiles.get(keyword);
	}
	
	/**sends off message to associate keyword to file, but must give FileInfo as argument*/
	public void sendAssociateKeywordMessage(String keyword, FileInfo fileInfo){
		long keywordHash = Hasher.hashPeerLong(keyword);
		InetAddress targetIP;
		int targetPort;
		Object [] targetPeer = networkController.getFingerTable2().resolveTargetPeer(keywordHash);
		if (targetPeer[0]!=null){
			targetIP = (InetAddress)targetPeer[1];
			targetPort = (Integer)targetPeer[2];
		}
		else{
			targetIP=  networkController.getLocalHostIP();
			targetPort = networkController.getLocalHostPort();
		}
//		InetAddress sourceIP = networkController.getLocalHostIP();
//		int sourcePort = networkController.getLocalHostPort();
		Message message = Message.getAssociateKeywordMessage(keyword, fileInfo, 
				networkController.getLocalHostIP(), networkController.getLocalHostPort(),
				networkController.getKeyPair().getPublic());
		Client client = new Client(targetIP, targetPort, message, networkController);
		new Thread(client).start();
		
		//now wait for the thread to finish
		client.getExitValue();
	}
	
	/**sends off message to associate keyword to file, give File as object.
	 * Must have the file on the local machine (of course).*/
	public void sendAssociateKeywordMessage(String keyword, File file){
		sendAssociateKeywordMessage(keyword, new FileInfo(file));
	}
	
	public void sendUnassociateKeywordMessage(String keyword, File file){
		sendUnassociateKeywordMessage(keyword, new FileInfo(file));
	}
	
	public void sendUnassociateKeywordMessage(String keyword, FileInfo fileInfo){

		long keywordHash = Hasher.hashPeerLong(keyword);
		InetAddress sourceIP;
		int sourcePort;
		Object [] targetPeer = networkController.getFingerTable2().resolveTargetPeer(keywordHash);
		if (targetPeer[0]!=null){
			sourceIP = (InetAddress)targetPeer[1];
			sourcePort = (Integer)targetPeer[2];
		}
		else{
			sourceIP=  networkController.getLocalHostIP();
			sourcePort = networkController.getLocalHostPort();
		}
//		InetAddress sourceIP = networkController.getLocalHostIP();
//		int sourcePort = networkController.getLocalHostPort();
		Message message = Message.getUnassociateKeywordMessage(keyword, fileInfo, 
				networkController.getLocalHostIP(), networkController.getLocalHostPort());
		Client client = new Client(sourceIP, sourcePort, message, networkController);
		new Thread(client).start();
		
		//now wait for the thread to finish
		client.getExitValue();
		System.out.println("leaving sendUnassociateKeywordMessage()");
	}

	/**this method blocks until response received and returns Vector<FileInfo> if successful, 
	 * meant to be used by the user*/
	public Vector<FileInfo> lookUpKeyword(String keyword){
		long keywordHash = Hasher.hashPeerLong(keyword);
		InetAddress sourceIP;
		int sourcePort;
		Object [] targetPeer = networkController.getFingerTable2().resolveTargetPeer(keywordHash);
		if (targetPeer[0]!=null){
			sourceIP = (InetAddress)targetPeer[1];
			sourcePort = (Integer)targetPeer[2];
		}
		else{
			sourceIP=  networkController.getLocalHostIP();
			sourcePort = networkController.getLocalHostPort();
		}
			
//		InetAddress sourceIP = networkController.getLocalHostIP();
//		int sourcePort = networkController.getLocalHostPort();
		Message message = Message.getGetFilesForKeywordMessage(keyword, 
				networkController.getLocalHostIP(), networkController.getLocalHostPort());
		Client client = new Client(sourceIP, sourcePort, message, networkController);
		new Thread(client).start();
		
		System.out.println("running client from lookUpKeyword()");
		//now wait for the thread to finish
		System.out.println("About to wait for client to send Keyword lookup message...");
		if (client.getExitValue(5000)==Client.ERROR){
			keyword="qweryjkl;adghjmk,lzxcvbnm,qwertyuioyjyuyhtu";
		}
		System.out.println("Continuing after client sent Keyword lookup message...");
		//may return null if lookup unsuccessful
		//Client thread will call postKeywordLookup() some time before getExitValue() returns
		System.out.println("about to exit lookUpKeyword()");
		//removes the entry as it leaves
		return localKeywordLookups.remove(keyword);
	}
	
	/**this method is called by Client to register each keyword lookup*/
	void postKeywordLookup(String keyword, Vector<FileInfo> fileInfos){
		localKeywordLookups.put(keyword, fileInfos);
	}
	
	void removeKeywordLookup(String keyword){
		localKeywordLookups.remove(keyword);
	}
	
	//Sets, gets, and adds
	public Hashtable<String, Vector<FileInfo>> getKeywordFiles(){
		return keywordFiles;
	}
	public void addkeywordFiles(Hashtable<String, Vector<FileInfo>> keywordFiles){
		this.keywordFiles.putAll(keywordFiles);
	}
	public void setKeywordFiles(Hashtable<String, Vector<FileInfo>> keywordFiles){
		this.keywordFiles=keywordFiles;
	}
	
	/**this is a class for making a 2 part key for a hashtable*/
	public class KeywordAssociatorFile{
		private String keyword = null;
		private Long associatorID = null;
		private Long fileID = null;
		public KeywordAssociatorFile(String keyword, Long associatorID, Long fileID){
			this.keyword = keyword;
			this.associatorID = associatorID;
			this.fileID = fileID;
		}
		public String getKeyword(){return keyword;}
		public Long getAssociatorID(){return associatorID;}
		public Long getFileID(){return fileID;}
		
		@Override
		public boolean equals(Object obj){
			if(obj == null || !(obj instanceof KeywordAssociatorFile))
				return false;
			KeywordAssociatorFile kafi = (KeywordAssociatorFile)obj;
			System.out.println("in equals()");
			System.out.println("my values: " + keyword + " " + associatorID + " " + fileID);
			System.out.println("kafi values: " + kafi.keyword + " " + kafi.associatorID + " " + kafi.fileID);
			return keyword.equals(kafi.keyword) &&
				associatorID.equals(kafi.associatorID) &&
				fileID.equals(kafi.fileID);
		}
		
		@Override
		public int hashCode(){
			//^ means XOR, just xor together all the individual hashcodes
			return keyword.hashCode() ^ associatorID.hashCode() ^ fileID.hashCode();
		}
	}
	/**t
	 * his is a class for making a 2 part key for a hashtable*/
	public class KeywordFile{
		private String keyword = null;
		private Long fileID = null;
//		private int count = 1;//initialize at one
		public KeywordFile(String keyword, Long fileID){
			this.keyword = keyword;
			this.fileID = fileID;
		}
//		public void incrementCount(){count++;}
//		public void decrementCount(){count--;}
//		public int getCount(){return count;}
		public String getKeyword(){return keyword;}
		public Long getFileID(){return fileID;}
		
		@Override
		public boolean equals(Object obj){
			if(obj == null || !(obj instanceof KeywordFile))
				return false;
			KeywordFile kafi = (KeywordFile)obj;
			return keyword.equals(kafi.keyword) &&
				fileID.equals(kafi.fileID);
		}
		
		@Override
		public int hashCode(){
			//^ means XOR, just xor together all the individual hashcodes
			return keyword.hashCode() ^ fileID.hashCode();
		}
	}
}
