package networkOverlay;
import java.io.File;
import java.util.*;

public class Services {
	public final static int DEFAULT_PORT = 7456;

	private int portNumber;
	private NetworkController networkController;
	
	//Helper variables for the main method
	private static boolean test_suite = false;
	private static Services services;
	private static StringTokenizer st;
	
	/*Constructor
	 * This class will be instantiated in the GUI.
	 When you instantiate it, make sure you have already asked the user for the port number because you will need
	 it for the constructor of this class.
	
	 This will make it easier for the Test Suite too.. all you will need to do is to instantiate as many Services
	 Classes as you need. (Each Service instance will be a peer) */
	public Services (int portNumber) {
		this.portNumber = portNumber;
		networkController = new NetworkController (this.portNumber,test_suite);
	}
	
	//default port constructor
	public Services(){
		this(DEFAULT_PORT);
	}
	
	/*sendMessage
	 * The message will be input from the GUI and sent to the network backend through this method
	 ************* Called by the GUI */	
	public void sendMessage(String message, String identifier) {
		//networkController.sendMessage(message, identifier);
	}
	
	/* waitToDisplayMessage
	 * The GUI/Test Suite Thread will wait here. The Network Backend will send the signal...
	  After the signal, the GUI will receive a string and that String will be the message to display
	  ************* Called by the GUI */
	public String waitToDisplayMessage() {
		return networkController.displayMessageOnGUI();
	}
	
	/* hashPeer
	 * For the test suite
	 */
	@Deprecated
	public String hashPeer (String address) {
		//return networkController.getHasher().hashPeerString(address);
		throw new RuntimeException("I think the test suite should just use the static method I put in Hasher - Charles");
	}
	
	//This fileDownload will be implemented in Phase II (Yeah Shahmir, I realized what you were saying, Finally hehehhe)
	public void fileDownload(String filename)
	{}
	
	
	//Temporary.. for until we don't merge the GUI/ Test Suite with the Network Overlay
	public static void main (String args[]) {		
		try {
			args[0].equals("TEST");
			test_suite = true;
		} catch (Exception e) {
		}
		
		Scanner sc = new Scanner(System.in);
		
		//The test suite automatically sends the port, so we don't want to ask for it
		if(!test_suite) {
			System.out.println("Enter port number");
		}
		
	  	int port = sc.nextInt();
	  	
		//Display to the test suite that a services instance is being created
		if(test_suite) {
			System.out.println("Creating services instance with port number: " + port);
		}
		services = new Services (port);
		
		sc.nextLine();		
		while (true) {
			//The test suite automatically sends the messages, so we don't want to prompt for it
			if(!test_suite) {
				System.out.println("Enter Message followed by the Identifier");
			}
			try {
			  	String input = sc.nextLine();
			  	st = new StringTokenizer(input);
			  	String action = st.nextToken();
			  	//Force a finger table update
			  	if(action.equals("Update")) {
			  		main_updateFingerTable();
			  	}
			  	else if(action.equals("Publish")) {
			  		main_publishFile();
			  	}
			  	else if(action.equals("Associate")) {
			  		main_associateKeyword();
			  	}
			  	else if(action.equals("Retract")) {
			  		main_retractFile();
			  	}
			  	else if(action.equals("Lookup")) {
			  		main_lookupFile();
			  	}
			  	else if(action.equals("Download")) {
			  		main_downloadFile();
			  	}
			  	else if(action.equals("Exit")) {
			  		main_exitService();
			  	}
			  	else if (action.equals("Printresults")){
			  		printResults();
			  	}
					
			}
			catch(NoSuchElementException e) {
			}
		}
	}
	private static void printResults() {
//		services.networkController.getMessageTargetDecider().stopRunning();
//		services.networkController.getMessageTargetDecider().printResults();
	}
	
	private static void main_updateFingerTable() {
  		if(test_suite) {
			System.out.println(Long.toHexString(services.networkController.getFingerTable2().getMyID()) + " is updating its finger table");
  		}
  		services.updateFingerTable();
	}
	
	private static void main_publishFile() {
  		FileInfo fileInfo = new FileInfo(new File(st.nextToken()));
  		if(fileInfo.getFilePath().exists())
  		{
  			if(test_suite) {
  				System.out.println(Long.toHexString(services.networkController.getFingerTable2().getMyID()) + 
					" publishing file: " + fileInfo.getFilePath() + " with file ID: " + fileInfo.getID());
  			}
  			services.publishFile(fileInfo.getFilePath());
  		}
  		else {
  			System.out.println("Error. The file does not exist.");
  		}
	}
	
	private static void main_associateKeyword() {
  		FileInfo fileInfo = new FileInfo(new File(st.nextToken()));
  		String keyword = st.nextToken();
  		if(test_suite) {
  			System.out.println(Long.toHexString(services.networkController.getFingerTable2().getMyID()) + 
				" associating keyword: " + keyword + " with file: "+ fileInfo.getFilePath() + " and file ID: " + fileInfo.getID());
  		}
  		services.associateKeyword(keyword, fileInfo.getFilePath());
	}
	
  	//We don't check for file existence here 'cause maybe we want to retract a file that we've deleted
	private static void main_retractFile() {
  		FileInfo fileInfo = new FileInfo(new File(st.nextToken()));
  		if(test_suite) {
  			System.out.println(Long.toHexString(services.networkController.getFingerTable2().getMyID()) + 
				" retracting file: " + fileInfo.getFilePath() + " with file ID: " + fileInfo.getID());
  		}
  		services.retractFile(fileInfo.getID());
	}
	
	private static void main_lookupFile() {
		String keyword = st.nextToken();
		Vector<FileInfo> fileInfoVector = services.lookUpKeyword(keyword);
		
		if(fileInfoVector == null) {
			System.out.println("File lookup failed. No file associated with the keyword " + keyword);
		}
		else {
			FileInfo fileInfo = fileInfoVector.get(0);
			Boolean fileLookupResult = services.lookupFile(fileInfo.getID(), fileInfo.getName());
						
			if(fileLookupResult) {				
				Vector<FileInfo> fileHolderVector = services.getLookupFileList(fileInfo.getID());
				
				if(fileHolderVector == null) {
					System.out.println("File lookup partially succeeded for keyword " + keyword + 
						". The file is " + fileInfo.getFilePath() + " with file ID: " + fileInfo.getID() + 
						". Failed to find anyone holding this file.");
				}
				else {
					FileInfo fileHolder = fileHolderVector.get(0);
					
					System.out.println("File lookup succeeded for keyword " + keyword + ". The file is " + fileInfo.getFilePath() + 
						" with file ID: " + fileInfo.getID() + " and is held by peer: " + fileHolder.getHolderID() + " hex_peer: " + 
						Long.toHexString(fileHolder.getHolderID()));
				}
			} else {
				System.out.println("File lookup failed for keyword " + keyword);
			}
		}
	}
	
	private static void main_downloadFile() {
		long fileID = Long.parseLong(st.nextToken());
		long fileHolderID = Long.parseLong(st.nextToken());
		File saveLocation = new File(st.nextToken());
		
		Boolean download_successful = services.downloadFile(fileID, fileHolderID, saveLocation);

		if(download_successful) {
			System.out.println("File download successful. " + Long.toHexString(services.networkController.getFingerTable2().getMyID()) + 
				" successfully downloaded file with file ID: " + fileID + " to " + saveLocation + " from " + fileHolderID);
		}
		else {
			System.out.println("File download failed. " + Long.toHexString(services.networkController.getFingerTable2().getMyID()) + 
				" failed to download file with file ID: " + fileID + " to " + saveLocation + " from " + fileHolderID);
		}
	}
	
	private static void main_exitService() {
  		if(test_suite) {
			System.out.println(Long.toHexString(services.networkController.getFingerTable2().getMyID()) + 
				" trying to gracefully exit");
  		}
  		services.initiateExitSequence();
	}
	
	public long getIdentifier() {
		return networkController.getIdentifier();
	}
	
	/**arguments: location of the file in your system
	 * ..also beware that this method will probably return before the
	 * publish file message arrives at the target.*/
	public FileInfo publishFile(File file){
		FingerTable2 fingerTable2 = networkController.getFingerTable2();
		UploadManager um = networkController.getUploadManager();
		//here we do the publish... this will send off the message and take care of everything
		System.out.println("from Services: " + fingerTable2.getMyIPAddress() + " " +
				fingerTable2.getMyPortNumber());
		return um.publishFile(file, fingerTable2.getMyIPAddress(), fingerTable2.getMyPortNumber());
	}
	
	/**arguments: the File object of the file.
	 * this will probably return before the retract finishes*/
	@Deprecated //because it will have to re hash the whole file content
	public void retractFile(File file){
//		long fileID = Hasher.hashPeerLong(file.getName());
		long fileID = Hasher.hashFileContent(file);
		networkController.getUploadManager().retractFile(fileID);
	}

//	/**arguments: the name of the file.
//	 * this will probably return before the retract finishes*/
//	private void retractFile(String fileName){
//		long fileID = Hasher.hashPeerLong(fileName);
//		networkController.getUploadManager().retractFile(fileID);		
//	}
	
	/**arguments: the name of the file.
	 * this will probably return before the retract finishes*/
	public void retractFile(Long fileID){
		networkController.getUploadManager().retractFile(fileID);		
	}
	
//	/** arguments: File object of file.
//	 * this method will block until finished with the lookup*/
//	public boolean lookupFile(File file){
//		long fileID = Hasher.hashPeerLong(file.getName());
//		int status = networkController.getDownloadManager().startLookupAndWait(fileID, file.getName());	
//		if(status == DownloadManager.LOOKED_UP) return true;
//		return false;
//	}
	
//	/** arguments: name of file. 
//	 * this method will block until finished with the lookup*/
//	private boolean lookupFile(String fileName){
//		long fileID = Hasher.hashPeerLong(fileName);
//		int status = networkController.getDownloadManager().startLookupAndWait(fileID, fileName);
//		if(status == DownloadManager.LOOKED_UP) return true;
//		return false;
//	}
	
	/** arguments: name of file. 
	 * this method will block until finished with the lookup*/
	public boolean lookupFile(Long fileID, String fileName){
		int status = networkController.getDownloadManager().startLookupAndWait(fileID, fileName);
		if(status == DownloadManager.LOOKED_UP) return true;
		return false;
	}
	
//	/**gets the file info for a file we have already looked up*/
//	public FileInfo getLookedUpFileInfo(File file, Long fileHolderID){
//		Long fileID = Hasher.hashPeerLong(file.getName());
//		return networkController.getDownloadManager().getLookedUpFile(fileID);
//	}
	
//	/**gets the file info for a file we have already looked up*/
//	public FileInfo getLookedUpFileInfo(String fileName){
//		Long fileID = Hasher.hashPeerLong(fileName);
//		return networkController.getDownloadManager().getLookedUpFile(fileID);
//	}
	
//	/**
//	 * returns a list of all file holders that have published this file
//	 */
//	private Vector<FileInfo> getLookupFileList(String fileName){
//		Long fileID = Hasher.hashPeerLong(fileName);
//		return networkController.getDownloadManager().getLookedUpFile(fileID);
//	}
	
	/**
	 * returns a list of all file holders that have published this file
	 */
	public Vector<FileInfo> getLookupFileList(Long fileID){
		return networkController.getDownloadManager().getLookedUpFile(fileID);
	}

//	/** arguments: String fileName, File saveLocation
//	 * this method will block until ready*/
//	private boolean downloadFile(String fileName, Long fileHolderID, File saveLocation){
//		long fileID = Hasher.hashPeerLong(fileName);
//		int status = networkController.getDownloadManager().
//			startDownloadAndWait(fileID, fileHolderID, saveLocation);
//		System.out.println("status: " + status);
//		if(status == DownloadManager.FINISHED)return true;
//		return false;
//	}
	
	/** arguments: Long fileID, File saveLocation
	 * this method will block until ready*/
	public boolean downloadFile(Long fileID, File saveLocation){
		int status = networkController.getDownloadManager().
			startDownloadAndWait(fileID, saveLocation);
		System.out.println("status: " + status);
		if(status == DownloadManager.FINISHED)return true;
		return false;
	}	
	
	/**Deprecated: fileHolderID is unnecessary and is never used. 
	 * arguments: Long fileID, Long fileHolderID, File saveLocation
	 * this method will block until ready*/
	@Deprecated
	public boolean downloadFile(Long fileID, Long fileHolderID, File saveLocation){
		int status = networkController.getDownloadManager().
			startDownloadAndWait(fileID, saveLocation);
		System.out.println("status: " + status);
		if(status == DownloadManager.FINISHED)return true;
		return false;
	}
	
//	/** arguments: File originalFile, File saveLocation
//	 * this method will block until ready*/
//	public boolean downloadFile(File originalFile, Long fileHolderID, File saveLocation){
//		long fileID = Hasher.hashPeerLong(originalFile.getName());
//		int status = networkController.getDownloadManager().
//			startDownloadAndWait(fileID, fileHolderID, saveLocation);
//		System.out.println("status: " + status);
//		if(status == DownloadManager.FINISHED)return true;
//		return false;
//	}

//	/** arguments: String fileName, File saveLocation
//	 * this method will block until ready, it downloads from file holder with fileHolderID*/
//	public boolean downloadFile(String fileName, File saveLocation, Long fileHolderID){
//		long fileID = Hasher.hashPeerLong(fileName);
//		int status = networkController.getDownloadManager().
//			startDownloadAndWait(fileID, fileHolderID, saveLocation);
//		System.out.println("status: " + status);
//		if(status == DownloadManager.FINISHED)return true;
//		return false;
//	}
	
	/**associates a keyword with a file on the local machine somewhere (and on the network)*/
	public void associateKeyword(String keyword, File file){
		if(keyword == null || keyword.equals("") || file == null)
			throw new IllegalArgumentException();
		
		networkController.getKeywordManager().
			sendAssociateKeywordMessage(keyword, file);
	}

	/**associates a keyword with a file on the local machine somewhere (and on the network)*/
	public void unassociateKeyword(String keyword, File file){
		networkController.getKeywordManager().
			sendUnassociateKeywordMessage(keyword, file);
	}
	
	/** 
	 * This method will perform a keyword lookup, blocking until lookup is complete,
	 * and it will return a Vector<FileInfo>, one entry for each file with that keyword
	 */
	public Vector<FileInfo> lookUpKeyword(String keyword){
		return networkController.getKeywordManager().lookUpKeyword(keyword);
	}
	
	public void updateFingerTable (){
		networkController.getFingerTable2().adjustFingerTable();
	}
	
	public void initiateExitSequence(){
		networkController.initiateExitSequence();
	}
	
	public NetworkController getNetworkController(){return networkController;}
}