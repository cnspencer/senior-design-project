package networkOverlay;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.*;
import java.security.*;
import javax.crypto.*;


public class Server implements Runnable{
	//the exit values
	public static final int NULL = 0;
	public static final int SUCCEEDED = 1;
	public static final int ERROR = 2;
	public static final int FAILED = 3;
	
	private int exitValue = NULL;//TODO is this necessary?
	
//	private ConnectionListener mother;//necessary??? or maybe NetworkController???
	private NetworkController networkController;
	
	private Socket socket;
	private ObjectOutputStream output;
	private ObjectInputStream input;
	
	public Server(Socket soc, NetworkController nc){
		networkController = nc;
		socket = soc;
	}
	
	public void run(){
		System.out.println("starting Server");
		try{
			//answer the phone
			if(!socket.isConnected()){
				System.out.println("socket not connected");
				return;
			} else{
				output = new ObjectOutputStream(socket.getOutputStream());
				input = new ObjectInputStream(new BufferedInputStream(socket.getInputStream()));
			}
			
			//see if we need to forward of accept the message
			forwardOrAccept();
		}
		catch (IOException ex){
			System.out.println("What happened?  " + networkController.getLocalHostPort() + " connection lost to " + socket.getPort());
			ex.printStackTrace();
			exitValue = ERROR;
		}
		catch (ClassNotFoundException ex){
			ex.printStackTrace();
			exitValue = ERROR;
		}
		catch (Exception ex){
			System.out.println("Something went wrong for " + networkController.getLocalHostPort());
			ex.printStackTrace();
			exitValue = ERROR;
			
		}
		finally{
			cleanUp();
		}
	}
	
	

	//***** other stuff ***********************
	
	//just cleans up the sockets and io streams
	private void cleanUp(){
		try{
			if(output != null)
				output.close();
		}
		catch(Exception ex){
			System.out.println("Cleaning up for " + networkController.getLocalHostPort());
			ex.printStackTrace();
			
		}
		
		try{
			if(input != null)
				input.close();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		
		try{
			if(socket != null)
				socket.close();
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	//be sure to close sockets if this object gets destroyed
	@Override
	protected void finalize(){
		cleanUp();
	}
	
	//changes socket and input/output streams to work with the source of the message
	public void makeDirectConnection(Message message) throws IOException{
		//clean up the old ones
		cleanUp();
		
		socket = new Socket(message.getSourceIP(), message.getSourcePort());
		output = new ObjectOutputStream(socket.getOutputStream());
		InputStream sis = socket.getInputStream();
		input = new ObjectInputStream(sis);
		
		//also, when we make a direct connection back to someone, we need to tell them
		//because they have been waiting for our response
//		Message response = Message.getEstablishConnectionMessage(message.getFileID());
		Message response = Message.getEstablishConnectionMessage(message.getTargetID());
		output.writeObject(response);
		output.flush();
		
//		socket = socket;
		//now we should get connected back to a Client object on the original source
	}
	
	public int getExitValue(){return exitValue;}
	
	//******* Conversation methods ********************************
	
	//this method checks if we need to forward the message or if it is for us
	private void forwardOrAccept() throws IOException, ClassNotFoundException{
		Message firstMessage = null;
		try{
			firstMessage = (Message)input.readObject();
		}
		catch (IOException ex){
			ex.printStackTrace();
		}
		
		InetAddress lastIP = firstMessage.getLastIP();
		int lastPort = firstMessage.getLastPort();
		if (!networkController.getOnlineStatus()){
			if (!(firstMessage.getMessageType()==Message.MessageType.HOST_VERIFICATION)){
				System.out.println(networkController.getLocalHostPort() + " Not ONLINE!");
				return;
			}
		}
//		//I put it here since we may want to do points-To correction even if it's a message for us
//		if ((firstMessage.getMessageType()!=Message.MessageType.HOST_VERIFICATION) &&
//				(firstMessage.getMessageType()!=Message.MessageType.FIND_PRED))
////					&&
////					(firstMessage.getMessageType()!=Message.MessageType.FOUND_PRED))
//			pointsToCorrectionCheck(lastIP,lastPort);
		
		//if this is the ESTABLISH_CONNECTION message, that means the Server from another
		//host has contacted us and we need to forward this socket to a Client object waiting
		//on our machine.
		if(firstMessage.getMessageType() == Message.MessageType.ESTABLISH_CONNECTION){
			
			networkController.getConnectionManager()
				.checkIfConnectionAwaited(firstMessage.getTargetID(), socket, output, input);
			//dont want to close socket (Client wants to use it)
			//so we set it to null so that finalize() wont do it once gc runs
			socket = null;
			output = null;
			input = null;
			return;
		}
		
		Object[] targetPeer = networkController.getFingerTable2().resolveTargetPeer(firstMessage.getTargetID());
		InetAddress newIP = null;
		int newPort = -1;
		try {
		if(targetPeer[0] == FingerTable2.MYSELF){
			//it's for me, so accept the message, don't forward
			acceptConversation(firstMessage);
			if (firstMessage.isPointsToCorrectionNeeded())
				pointsToCorrectionCheck(lastIP,lastPort);
		}
		else{
			if (!firstMessage.countMessageForwards()){
				//if after forwarding the message 50 times, the message is still not delivered, 
				//drop the message and reset yourself since you are part of the problem why this 
				// message did not get delivered. 
				System.out.println("resetting: " + networkController.getLocalHostPort() + 
						"FROM SERVER: because there is some problem in how this peer is set up... " );
				networkController.reset();
				return;
			}
//			if ((firstMessage.getMessageType()!= Message.MessageType.FIND_SUCCESSOR) &&
//					(firstMessage.getMessageType()!=Message.MessageType.PROBE_MESSAGE)&&
//					(firstMessage.getMessageType()!=Message.MessageType.FOUND_SUCCESSOR))
			System.out.println(firstMessage.getMessageType() + "Message reached " + networkController.getIdentifier() + " sent by " + firstMessage.getSourceID() + " intended for " + firstMessage.getTargetID()+ " forwarding to " + (Long)targetPeer[0]);
				//we need to forward it
			//first clean up the old socket and streams
			// but before that, store the ip and port of last peer for points to
			cleanUp();
			//next make a connection to someone on the finger table (targetPeer)
			Long newID = (Long)targetPeer[0];
			newIP = (InetAddress)targetPeer[1];
			newPort = (Integer)targetPeer[2];
			socket = new Socket(newIP, newPort);
			
			ObjectOutputStream newOutput = new ObjectOutputStream(socket.getOutputStream());
			//just to prevent exceptions
			ObjectInputStream newInput = new ObjectInputStream(socket.getInputStream());
			
			System.out.println("forwarding message of type " + firstMessage.getMessageType());
			//after all the connection stuffs, we finally forward it
			firstMessage.setMyselfAsLast();
//			Client forwardClient = new Client(newID, firstMessage, networkController);
//			new Thread(forwardClient).start();
			newOutput.writeObject(firstMessage);
			}
		
		if (firstMessage.isPointsToCorrectionNeeded())
				pointsToCorrectionCheck(lastIP,lastPort);
				
		}
				
			//it will get cleaned up in the run() method
		
		catch (IOException ex){
			ForwardingFailureHandler ffh = new ForwardingFailureHandler(newIP,newPort,firstMessage,networkController);
			new Thread(ffh).start();
			exitValue = ffh.getExitStatus();
		
		}
	}

	private void acceptConversation(Message firstMessage) 
			throws IOException, ClassNotFoundException{
		
		switch (firstMessage.getMessageCategory()){
			case JOIN: {handleJoinMessageType(firstMessage);break;}
			case FILE: {fileConversation(firstMessage);break;}
			case UPDATE: {updateConversation(firstMessage);break;}
			case TEST: {testConversation(firstMessage);break;}
			case EXIT: {exitConversation(firstMessage);break;}
			case KEYWORD: keywordConversation(firstMessage);break;
			default :
				System.out.println("Received message not recognized. Type: " 
						+ firstMessage.getMessageType().toString());
				exitValue = ERROR;
		}
	}
	
	///////////////////////////////////////////////////////////////
	//////////JOIN/////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////
	
	private void handleJoinMessageType(Message message) throws IOException{
		switch (message.getMessageType()){
			case HOST_VERIFICATION: {
				//System.out.println("Server:In host veri");
				handleHostVerification(message);
				break;
				}
			case FIND_PRED:{
				handleFindPredecessor(message);	
				break;
				}	
			case FOUND_PRED:{			
				handleFoundPredecessor(message);			
				break;			
				}		
			case FIND_SUCCESSOR: {
				handleFindSuccessor(message); 
				break;
			}
			case FOUND_SUCCESSOR: {
				handleFoundSuccessor(message); 
				break;
			}
		}
	}	//Handling the subtypes
	private void handleHostVerification(Message message) throws IOException{
		if(networkController.getOnlineStatus()){
			Message reply = Message.getHostAvailableMessage(Hasher.
					hashPeerLong(message.getSourceIP()+ ":" + message.getSourcePort())); 
			output.writeObject(reply);
			output.flush();
		}
		else {
			Message reply = Message.getHostUnavailableMessage(Hasher.hashPeerLong(message.getSourceIP() 
					+ ":" + message.getSourcePort())); 
			output.writeObject(reply);
			output.flush();
		}
			
	}
	
	private void handleFindPredecessor(Message message){
		Object [] predPeer = networkController.getFingerTable2().getPredPeer(message.getTargetID()); 
		if (predPeer[0] == FingerTable2.MYSELF){
			InetAddress localIP = networkController.getLocalHostIP();
			int localPort = networkController.getLocalHostPort();
			
			Message msg =  Message.getFoundPredMessage(localIP, localPort,networkController.getFingerTable2(),
					networkController.getMySuccessorsFileKeys(message.getSourceID()),
					networkController.getMySuccessorKeywordFiles(message.getSourceID()),
					message.getSourceID());
			Client client = new Client(message.getSourceIP(), message.getSourcePort(),
					msg, networkController);
			new Thread(client).start();
			client.getExitValue();
			try{
				Thread.sleep(1000);
			}
			catch(InterruptedException ex){
				
			}
			networkController.getFingerTable2().updateFingerTable(message.getSourceIP(), message.getSourcePort());
			
			
		}
		//here it gets forwarded, but in Server it is already forwarded, so I commented it out
//		else{
//			InetAddress targetIP = (InetAddress)predPeer[1];
//			int targetPort = (Integer)predPeer[2];
//			Client client = new Client(targetIP, targetPort,
//		message, networkController);
//		new Thread(client).start();
//			networkController.getFingerTable2().updateFingerTable(message.getSourceIP(), message.getSourcePort());
//		}
	}
	
	private void handleFoundPredecessor(Message message){
		if (networkController.getJoinFlag()){
			//System.out.println("MY Pred Found........." + message.getSourceID());
			networkController.getFingerTable2().setPredecessor(message.getSourceID(),
					message.getSourceIP(), message.getSourcePort());
			networkController.getFingerTable2().setFingerTable(message.getFingerTable2());
			networkController.setFileKeys(message.getFileKeyTable());
			networkController.getKeywordManager().setKeywordFiles(message.getKeywordFilesTable());
			//System.out.println("Calling finger table adjust method from messageHandler");
			networkController.getFingerTable2().adjustFingerTable();
			networkController.setJoinFlag(false);
			networkController.informSuccessorOfMyExistance();
			System.out.println("Finished setting up peer: " + networkController.getIdentifier() + " in Hex: " + Long.toHexString(networkController.getIdentifier()));
		}
	}
	
	private void handleFindSuccessor(Message message){
		Object [] predPeer = networkController.getFingerTable2().getPredPeer(message.getSuccessorID());
		if (predPeer[0]==FingerTable2.MYSELF){
			Object [] successorInfo = networkController.getFingerTable2().getMySuccessorPeer();
			//System.out.println("Sending Successor : " + message.getSourceID());
			InetAddress successorIP;
			int successorPort; 
			Long successorID;
			if (successorInfo[0] == FingerTable2.MYSELF){
				successorIP = networkController.getLocalHostIP();
				successorPort =  networkController.getLocalHostPort();
				successorID = networkController.getIdentifier();
			}
			else {
				//System.out.println("ID of succ" + successorInfo[0]+ "port of succpeer" + successorInfo[2]);
				successorIP = (InetAddress)successorInfo[1];
				successorPort =  (Integer)successorInfo[2];
				successorID =  (Long)successorInfo[0];
			}
			Message msg = Message.getFoundSuccessorMessage(message, successorIP, successorPort, successorID);
			Client client = new Client(message.getSourceIP(),message.getSourcePort(),
					msg, networkController);
			new Thread(client).start();
			client.getExitValue();
			networkController.getFingerTable2().updateFingerTable(message.getSourceIP(), message.getSourcePort());
		}
		//else{
//			InetAddress targetIP = (InetAddress)predPeer[1];
//			int targetPort = (Integer)predPeer[2];
//			Client client = new Client(targetIP, targetPort,
//					message, networkController);
//			new Thread(client).start();
//			networkController.getFingerTable2().updateFingerTable(message.getSourceIP(), message.getSourcePort());
		//}
			
	}
	
	
	private void handleFoundSuccessor(Message message){
		//System.out.println(" FOUND SUCCESSOR: " + message.getSuccessorID());
		networkController.getFingerTable2().updateFingerTableEntry(message.getSuccessorIP(), message.getSuccessorPort(), message.getfingerTableUpdateIndex());
		networkController.getFingerTable2().removeInconsistentPeerEntries();
		
	}

	///////////////////////////////////////////////////////////////
	/////////////////////FILE///////////////////////////////////////
	///////////////////////////////////////////////////////////////
	
	public void fileConversation(Message firstMessage) 
			throws IOException, ClassNotFoundException{
		switch(firstMessage.getMessageType()){
		case PUBLISH_FILE:
			FileInfo fileInfo = firstMessage.getFileInfo();
			networkController.putFileKey(firstMessage.getFileID(), fileInfo);
			//System.out.println("new file just published (this message from Server)" + 
					//firstMessage.getFileID());
			System.out.println(Long.toHexString(firstMessage.getSourceID())+ 
				" published file: " + fileInfo.getFilePath().toString() + " with file ID: " + firstMessage.getFileID() +
				" and holder: " + fileInfo.getHolderIPAddress() + " " + fileInfo.getHolderPort());
			break;
		case RETRACT_FILE:
			handleRetractFile(firstMessage);
			break;
		case LOOKUP_FILE:
			handleLookupFile(firstMessage);
			break;
		case DOWNLOAD_FILE:
			//we already have a direct connection
	//		makeDirectConnection(firstMessage);
			handleDownloadFile(firstMessage);
			break;
		default:
			System.out.println("wrong message recieved");
			exitValue = ERROR;
		}
	}
	
	public void handleLookupFile(Message firstMessage) throws IOException{
		makeDirectConnection(firstMessage);
		Vector<FileInfo> fileInfos = networkController.getAllFileHolders(firstMessage.getFileID());
		Message reply;
		
		//if fileInfo not found
		if(fileInfos == null){
			reply = Message.getLookupFailedMessage(firstMessage.getFileID());
		}
		//as long as fileInfo was found
		else{
			 reply = Message.getSendFileHolderMessage(fileInfos);
			//respond directly (this was a bug earlier)
	//		this.makeDirectConnection(firstMessage);
		}
		output.writeObject(reply);
		output.flush();
		System.out.println("leaving handleLookupFile()");
	}
	
	public void handleRetractFile(Message firstMessage)
			throws IOException, ClassNotFoundException{
		makeDirectConnection(firstMessage);
		//TODO send a challenge message first
		FileInfo fileToRemove = networkController
			.getFileHolder(firstMessage.getFileID(), firstMessage.getFileHolderID());
	
		if(fileToRemove == null) {
			System.out.println(Long.toHexString(firstMessage.getFileHolderID()) + 
					" failed to retract file. The file key doesn't exit in the list.");
			Message retractFailedMessage = Message.getRetractFailedMessage();
			output.writeObject(retractFailedMessage);
			output.flush();
		}
		else {	
			System.out.println("about to call challengeMet()");
			//here is the method call for the challenge message
			
			//messages will continue to be sent back and forth in challengeMet()
			if(challengeMet(fileToRemove.getFileHolderPublicKey())){//here we send the challenge message
				networkController.removeFileHolder(
						firstMessage.getFileID(), firstMessage.getFileHolderID());
				System.out.println(Long.toHexString(firstMessage.getFileHolderID()) + 
						" retracted file: " + fileToRemove.getFilePath() + " with file ID: " 
						+ fileToRemove.getID());
				
				System.out.println("succeeded on retract file for " + fileToRemove.getID());
				Message retractSucceededMessage = Message.getRetractSucceededMessage();
				output.writeObject(retractSucceededMessage);
				output.flush();
			}
			else{
				System.out.println("failed on retract file for " + fileToRemove.getID());
				Message retractFailedMessage = Message.getRetractFailedMessage();
				output.writeObject(retractFailedMessage);
				output.flush();
			}
		}
	}
	
	public void handleDownloadFile(Message firstMessage) throws IOException{
		Message reply;
		UploadManager uploadManager = networkController.getUploadManager();
		//first make sure the file is available
		System.out.println("about to check with uploadmanager to upload");
		if(uploadManager.checkIfFileAvailable(firstMessage.getFileID())){
			//later we can change this back...
			//grab file info
			FileInfo fileInfo = uploadManager.getFileInfo(firstMessage.getFileID());
			BufferedInputStream bos = new BufferedInputStream(new FileInputStream(
					fileInfo.getFilePath()));
			System.out.println("check 1");
			try{
				int offset = firstMessage.getStartingIndex();
				System.out.println("check 2");
				int numberOfBytes = (int)firstMessage.getBytesToDownload();
				System.out.println("check 3");
				System.out.println("\t\tlong: " + firstMessage.getBytesToDownload() + 
						" int: " + numberOfBytes);
				System.out.println("check 4");
				
				//we loop and send a bunch of messages
					//this is the size of each packet (max size)
					int packetSize = UploadManager.MAX_UPLOAD_PACKET_SIZE;
					int numberOfPackets = (numberOfBytes / packetSize) + 1;
					int currentOffset = offset;//this gets increased for each message
					int remainingBytes = numberOfBytes;//decreased for each message

					System.out.println("check 5");
					
					//the buffer
					byte[] bytes = new byte[packetSize];
					
					//first lets skip up to where we need to be
					bos.skip(offset);
					
					//here we send the packets
					for(int i = 0; i < numberOfPackets; i++){
						System.out.println("remainingBytes: " + remainingBytes);
						System.out.println("check 6");
						System.out.println("download packet " + i + " sent.");
						//we want to send no more than packetSize
						if(remainingBytes > packetSize){
							System.out.println("\tbytes.length : " + bytes.length + 
									" packetSize : " + packetSize);
							System.out.println("check 7");
							System.out.println("bytes.length: " + bytes.length + 
									" currentOffset: " + currentOffset + 
									" packetSize: " + packetSize + 
									" file length: " + fileInfo.getFilePath().length());
							bytes = new byte[packetSize];
							bos.read(bytes);
//							bos.skip(bytes.length);
							System.out.println("check 8");
							reply = Message.getFileDataMessage(fileInfo, bytes, currentOffset);
							output.writeObject(reply);
							//now adjust loop variables
							currentOffset += packetSize;
							remainingBytes -= packetSize;
							System.out.println("check 9");
						}
						else if (remainingBytes > 0){//this is for the very last packet
							System.out.println("check 10");
							System.out.println("bytes.length: " + bytes.length + 
									" currentOffset: " + currentOffset + 
									" packetSize: " + packetSize + 
									" file length: " + fileInfo.getFilePath().length());
							bytes = new byte[remainingBytes];
							bos.read(bytes);
							reply = Message.getFileDataMessage(fileInfo, bytes, currentOffset);
							output.writeObject(reply);
							System.out.println("check 11");
//							break;
						}
					}
					
					//also tell them that it is finished
					Message finishedMessage = Message.getDownloadFinishedMessage(fileInfo.getID());
					output.writeObject(finishedMessage);
					
				output.flush();
				System.out.println("check 12");
			}
			catch(FileNotFoundException ex){
				ex.printStackTrace();
			}
			finally{
				if(bos != null){
					bos.close();
				}
			}
		}
		else{
			//we are not sharing that file right now
			reply = Message.getDownloadFailedMessage(firstMessage.getFileID());
			System.out.println("about to write upload error");
			output.writeObject(reply);
			output.flush();
		}
	}
	
	//the actual uploading
//	public void startUpload(Message firstMessage) throws IOException{
//		Message reply;//will eventually send this back
//		int sourceID = Hasher.hashPeerInt(socket.getInetAddress(), socket.getPort());
//		UploadManager uploadManager = networkController.getUploadManager();
//		//register the upload so we know we are expecting it (separate socket)
//		UploadManager.Upload upload = 
//			uploadManager.registerUpload(firstMessage.getFileID(), sourceID);
//		
//		//now do some communication
//		//a new socket will eventually come along and start up the registered Upload
//		//then the Upload will run and finish
//		System.out.println("about to block and allow upload");
//		boolean succeeded = upload.uploadSucceeded();//block here until upload finished
//		if(succeeded){
//			//if upload succeeded, return DOWNLOAD_FINISHED
//			reply = Message.getDownloadFinishedMessage(upload.getFileInfo().getID());
//			output.writeObject(reply);
//			output.flush();
//		}
//		else{
//			//if upload failed, return DOWNLOAD_FAILED
//			reply = Message.getDownloadFailedMessage(upload.getFileInfo().getID());
//			output.writeObject(reply);
//			output.flush();
//		}
//	}


	///////////////////////////////////////////////////////////////
	/////////////////////EXIT////////////////////////////////////////
	///////////////////////////////////////////////////////////////
	
	public void exitConversation(Message firstMessage){
		switch (firstMessage.getMessageType()){
		case PRED_LEAVING:{
			handlePredLeaving(firstMessage);	
			break;
			}
		case SUCCESSOR_LEAVING:{
			handleSuccessorLeaving(firstMessage);
			break;
			}
		}
	}
	
	public void handlePredLeaving(Message firstMessage){
		long predID = firstMessage.getPredID();
		InetAddress predIP = firstMessage.getPredIP();
		int predPort = firstMessage.getPredPort();
		networkController.getFingerTable2().setPredecessor(predID, predIP, predPort);
		networkController.getFingerTable2().removeFromFingerTable(firstMessage.getSourceIP(), firstMessage.getSourcePort());
		networkController.getFingerTable2().updateFingerTable(predIP, predPort);
		
	}
	
	public void handleSuccessorLeaving(Message firstMessage){
		networkController.addFileKeys(firstMessage.getFileKeyTable());
		networkController.getKeywordManager().addkeywordFiles(firstMessage.getKeywordFilesTable());
		InetAddress newSuccessorIP = firstMessage.getSuccessorIP();
		int newSuccessorPort = firstMessage.getSuccessorPort();
		//Object [] mySuccessor = networkController.getFingerTable2().getMySuccessorPeer();
		//TODO need to check if received message was really from your successor.
		networkController.getFingerTable2().removeFromFingerTable(firstMessage.getSourceIP(), firstMessage.getSourcePort());
		networkController.getFingerTable2().updateFingerTable(newSuccessorIP, newSuccessorPort);
		
	}
	
	///////////////////////////////////////////////////////////////
	/////////////////////////////UPDATE///////////////////////////////
	///////////////////////////////////////////////////////////////
	
	public void updateConversation(Message firstMessage) throws IOException{
		switch (firstMessage.getMessageType()){
		case PEER_EXISTS:{
			handlePeerExists(firstMessage);	
			break;
			}
		case FIND_NEW_PRED:{
			handleFindNewPred(firstMessage);	
			break;
		}
		case SEND_YOUR_PRED: {
			handleSendYourPred(firstMessage);
			break;
		}
		case I_AM_YOUR_PRED:{
			handleIAmYourPred(firstMessage);
			break;
		}
		case PEER_LEFT:{
			handlePeerLeft(firstMessage);
			break;
		}
		}
		
	}
		
	public void handlePeerExists (Message firstMessage){
		networkController.getFingerTable2().updateFingerTable(firstMessage.getOptimalIP(),firstMessage.getOptimalPort());
		networkController.getFingerTable2().printFingerTable();
	}
	public void handleFindNewPred (Message firstMessage){
		//TODO: security
		Object [] predPeer = networkController.getFingerTable2().getPredPeer(firstMessage.getTargetID()); 
		if (predPeer[0] == FingerTable2.MYSELF){
			InetAddress myIP = networkController.getLocalHostIP();
			int myPort = networkController.getLocalHostPort();
			Long myID = Hasher.hashPeerLong(myIP, myPort);
			networkController.getFingerTable2().updateFingerTable(firstMessage.getSourceIP(), firstMessage.getSourcePort());
			
			Message msg =  Message.getIAmYourPredMessage(myIP, myPort,myID,firstMessage.getSourceID());
			Client client = new Client(firstMessage.getSourceIP(), firstMessage.getSourcePort(),
					msg, networkController);
			new Thread(client).start();
			client.getExitValue();
			
		}
	}
	
	public void handleSendYourPred (Message firstMessage) throws IOException{
		InetAddress myIP = networkController.getFingerTable2().getMyIPAddress();
		int myPort= networkController.getFingerTable2().getMyPortNumber();
		InetAddress predIP= networkController.getFingerTable2().getMyPredIP();
		int predPort= networkController.getFingerTable2().getMyPredPort();
		Long targetID = firstMessage.getSourceID();
		Message reply = Message.getSendYourPredMessage(myIP, myPort, predIP, predPort, targetID);
		//System.out.println("Sending pred: "+ reply.getPredID());
		output.writeObject(reply);
		output.flush();
	}
	
	
	public void handleIAmYourPred(Message firstMessage){
		networkController.getFingerTable2().setPredecessor(firstMessage.getSourceID(),firstMessage.getSourceIP(), firstMessage.getSourcePort());
	}
	
	public void handlePeerLeft(Message firstMessage) {
		long fileHolderID = Hasher.hashPeerLong(firstMessage.getSourceIP()+":" + firstMessage.getSourcePort());
		FileInfo fileToRemove = networkController.getFileHolder(firstMessage.getFileID(), fileHolderID);

	if(fileToRemove != null) {
		Client predCheck = new Client(firstMessage.getSourceIP(),firstMessage.getSourcePort(), 
				Message.getTestConnectionMessage(networkController.getLocalHostIP(),networkController.getLocalHostPort(),fileHolderID), networkController);
			
			new Thread(predCheck).start();
			int exitStatus = predCheck.getExitValue();
			if (exitStatus != Client.SUCCEEDED){
				networkController.removeFileHolder(
						firstMessage.getFileID(), fileHolderID);
				System.out.println(Long.toHexString(firstMessage.getFileHolderID()) + 
						" retracted file: " + fileToRemove.getFilePath() + " with file ID: " 
						+ fileToRemove.getID());
			}
	}
//		
//		Message message = Message.getRetractFileMessage(firstMessage.getFileID(),fileHolderID);
//		try{
//			fileConversation(message);
//		}
//		catch (IOException ex){}
//		catch (ClassNotFoundException ex){}
	}
	
	
	///////////////////////////////////////////////////////////////
	/////////////////////////////TEST///////////////////////////////
	///////////////////////////////////////////////////////////////
	
	public void testConversation(Message firstMessage){
		switch (firstMessage.getMessageType()){
		case TESTSUITE_TEST:{
			break;
			}
		case TEST_CONNECTION:{
			break;
		}
		case PROBE_MESSAGE:{
			//networkController.getMessageTargetDecider().acceptProbeMessage(firstMessage);
			break;
			
		}
		default:
			System.out.println("wrong message type");
			exitValue = ERROR;	
		}
		
	}
	///////////////////////////////////////////////////////////////
	///////////////////////////KEYWORD/////////////////////////////
	///////////////////////////////////////////////////////////////
	
	private void keywordConversation(Message firstMessage)
			throws IOException, ClassNotFoundException {
		
		switch(firstMessage.getMessageType()){
		case ASSOCIATE_KEYWORD:
			associateKeywordConversation(firstMessage);
			break;
		case UNASSOCIATE_KEYWORD:
			unassociateKeywordConversation(firstMessage);
			break;
		case GET_FILES_FOR_KEYWORD:
			getFilesForKeywordConversation(firstMessage);
			break;
		default:
			System.out.println("wrong message type");
			exitValue = ERROR;	
		}
	}
	
	private void associateKeywordConversation(Message firstMessage)
			throws IOException, ClassNotFoundException {
		//first make the keyword association
		KeywordManager keywordManager = networkController.getKeywordManager();
		keywordManager.associateKeyword(firstMessage.getKeyword(), firstMessage.getFileInfo(),
				firstMessage.getSourceID(), firstMessage.getPublicKey());
		
		//connect directly
		makeDirectConnection(firstMessage);
		
		//for now, let's just assume that it always succeeds (no failures)
		Message reply = Message.getAssociateFinishedMessage();
		output.writeObject(reply);
	}
	private void unassociateKeywordConversation(Message firstMessage)
			throws IOException, ClassNotFoundException {
		//first remove the keyword association
		KeywordManager keywordManager = networkController.getKeywordManager();	
		
		//connect directly
		makeDirectConnection(firstMessage);
		//make the challenge
		
		if(keywordManager.associationExists(firstMessage.getKeyword(), 
				firstMessage.getFileInfo().getID(), firstMessage.getSourceID())
				//here we make the challenge (where a couple message are passed)
				&& challengeMet(keywordManager.getPublicKey(firstMessage.getSourceID()))){

			System.out.println("**********unassociate succeeded**********");
			keywordManager.unassociateKeyword(firstMessage.getKeyword(), 
					firstMessage.getFileInfo().getID(), firstMessage.getSourceID());
		
			//for now, let's just assume that it always succeeds (no failures)
			Message reply = Message.getUnassociateFinishedMessage();
			output.writeObject(reply);
			output.flush();
		}
		//failure on unassociate
		else{
			System.out.println("**********unassociate failed**********");
			Message reply = Message.getUnassociateFailedMessage();
			output.writeObject(reply);
			output.flush();
		}

	}
	private void getFilesForKeywordConversation(Message firstMessage)
			throws IOException, ClassNotFoundException {
		//first make the lookup
		KeywordManager keywordManager = networkController.getKeywordManager();
		Vector<FileInfo> fileInfos = keywordManager.getAllFilesForKeyword(firstMessage.getKeyword());
		
		//connect directly
		makeDirectConnection(firstMessage);
		
		//if fileInfos is null, that means there were no keyword associations, so its a failure
		Message reply = null;
		if(fileInfos == null || fileInfos.size() == 0){
			reply = Message.getGetFilesForKeywordFailedMessage();
		}
		else {
			reply = Message.getGetFilesForKeywordFinishedMessage(
					firstMessage.getKeyword(), fileInfos);
		}
		output.writeObject(reply);
	}


	
	///////////////////////////////////////////////////////////////
	///////////////////////////POINTS TO///////////////////////////
	///////////////////////////////////////////////////////////////
	
	public void pointsToCorrectionCheck(InetAddress lastIP, int lastPort){
		//System.out.println("\tIn points-to-correction");
		long lastID = Hasher.hashPeerLong(lastIP+ ":" + lastPort);
		long myID = networkController.getFingerTable2().getMyID();
		long distance = myID - lastID;
		long twoToThirtyTwo = 1L << (long)32;//(long)Math.pow(2,32);
		if (distance <0){
			distance +=twoToThirtyTwo;
		}
		int logOfDist =(int)Math.floor(Math.log(distance)/Math.log(2));
		long hop = 1L << (long)logOfDist;//(long)Math.pow(2, logOfDist);
		
		long target = (hop + lastID)%twoToThirtyTwo;
		//System.out.println("distance " + distance + " pow: " + logOfDist + " hop: " + hop + " target: "+target);
		if(!networkController.checkPredecessor())
			return;
		long myPredID = networkController.getFingerTable2().getMyPredID();
		long distanceToPred = (myID - myPredID)%twoToThirtyTwo;
		long distanceToTarget = (myID - target)%twoToThirtyTwo;
		if (distanceToPred <0)
			distanceToPred += twoToThirtyTwo;
		if (distanceToTarget <0)
			distanceToTarget += twoToThirtyTwo;
		
		System.out.println("distance to pred " + distanceToPred + " distance to target  " + distanceToTarget);
		
		if (distanceToPred < distanceToTarget){
			System.out.println(Long.toHexString(lastID) +" IP: "+ lastIP + " Port: " + lastPort + "\t NEEDS CORRECTION!.. Optimal:" + Long.toHexString(myPredID));
			//System.out.println("distance to pred " + distanceToPred + " distance to target  " + distanceToTarget);
			InetAddress myPredIP = networkController.getFingerTable2().getMyPredIP();
			int myPredPort = networkController.getFingerTable2().getMyPredPort();
			Client client = new Client(lastIP, lastPort, Message.getPeerExistsMessage(myPredIP, myPredPort, myPredID, lastID),networkController);
			new Thread(client).start();
			client.getExitValue();
		}

		//System.out.println("\tLeaving points-to-correction");
	}
	
	private boolean challengeMet(PublicKey publicKey) 
			throws IOException, ClassNotFoundException{
		System.out.println("in challengeMet()");
		try{
			//first we make a new challenge message with a secure random generator
			int challengeMessageLength = 32;//in bytes
			SecureRandom challengeMessageGenerator = new SecureRandom();
			byte[] challengeMessageOriginal = challengeMessageGenerator.generateSeed(challengeMessageLength);
			System.out.println("challenge message generated");
			
			//now encrypt the challenge message with the public key
			Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.ENCRYPT_MODE, publicKey);
			byte[] challengeMessageEncrypted = cipher.doFinal(challengeMessageOriginal);
			System.out.println("challenge message encrypted");
			
			//now send the challenge message and await the response
			Message message = Message.getChallengeMessage(challengeMessageEncrypted);
			output.writeObject(message);
			output.flush();
			
			//no get the response
			Message reply = (Message)input.readObject();
			switch(reply.getMessageType()){
			case RETRACT_FAILED:
				exitValue = FAILED;
				break;
			case CHALLENGE_REPLY:
				//now see if they are equal
				//if they are, then the other host must have decoded it with the private key
				return Arrays.equals(challengeMessageOriginal,
						reply.getChallengeMessage());
			default:
				System.out.println("wrong message type: " + reply.getMessageType());
				exitValue = ERROR;	
			}
			
			return false;
		}
		catch(NoSuchPaddingException ex){
			ex.printStackTrace();
			return false;
		}
		catch(NoSuchAlgorithmException ex){
			ex.printStackTrace();
			return false;			
		}
		catch(InvalidKeyException ex){
			ex.printStackTrace();
			return false;			
		}
		catch(IllegalBlockSizeException ex){
			ex.printStackTrace();
			return false;			
		}
		catch(BadPaddingException ex){
			ex.printStackTrace();
			return false;			
		}
	}
	
}
