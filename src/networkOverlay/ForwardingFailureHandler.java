package networkOverlay;

import java.net.*;
import java.io.*;
import java.util.concurrent.*;

public class ForwardingFailureHandler implements Runnable {

	private NetworkController networkController;
	private InetAddress targetIP =null;
	private int targetPort=-1;
	private long targetID;
	private Message messageToForward;
	private static boolean alreadyRunning;
	private static Semaphore finished = new Semaphore(0);
	private static Object lockObject = new Object();
	private int exitStatus =Client.ERROR;

	
	
	/**This constructor needs its own thread so make sure you do a Thread(FFH).start() on it. */
	public ForwardingFailureHandler(InetAddress targetIP, int targetPort, Message messageToSend, 
			NetworkController nc){
		this.networkController = nc;
		this.targetIP = targetIP;
		this.targetPort = targetPort;
		this.targetID = Hasher.hashPeerLong(targetIP + ":" + targetPort);
		this.messageToForward = messageToSend;
	}
	
	public void run() {
		System.out.println("In Forwarding Failure Handler run() method");
		synchronized(lockObject){
			if (!alreadyRunning)
				alreadyRunning=true;
			else{
				System.out.println("Already Running FF Algorithm....");
				return;
			}
		}
		
		if ((targetIP ==null)|| (targetPort ==-1)){
			finished.release();
			alreadyRunning = false;
			return;
		}
		
		if ((messageToForward.getMessageType()==Message.MessageType.HOST_VERIFICATION)||
		(messageToForward.getMessageType()==Message.MessageType.TEST_CONNECTION)){
			exitStatus = Client.ERROR;
			finished.release();
			alreadyRunning=false;
			return;
		}
		if (messageToForward.getMessageType()==Message.MessageType.DOWNLOAD_FILE){
			exitStatus = Client.ERROR;
			finished.release();
			handleFileHolderLeft(messageToForward);
			alreadyRunning = false;
			return;
		}
		Object [] mySuccessor = networkController.getFingerTable2().getMySuccessorPeer();
		
		if (messageToForward.getMessageType()==Message.MessageType.FIND_PRED){
			handleFindPredFailure(messageToForward);
			finished.release();
		} 
		else{
			if (((Long)mySuccessor[0]).equals(targetID)){
			
				exitStatus = Client.ERROR;
				finished.release();
				handleSuccessorLoss();
			}
			else {
				handleRegularLoss();
				finished.release();
			}
		}
		System.out.println("Leaving Forwarding Failure Handler run() method");
		alreadyRunning = false;
				try {}
		catch (NullPointerException ex){
			exitStatus = Client.ERROR;
			if (finished.getQueueLength()>0)
				finished.release();
			System.out.println("Contacted peer left. Can not complete forwarding failure algorithm");
		}
	}
	
	
	/**handleSuccessorLoss()
	 * This method finds a new successor and informs the successor that I am his new predecessor.
	 * The message to be forwarded is dropped.
	 */
	public void handleSuccessorLoss() throws NullPointerException {
		System.out.println("\tForwardingFailureHandler: Lost Successor");
		System.out.println("\t\tForwardingFailureHandler: DROPPING MESSAGE! :(");
		Long myID = networkController.getFingerTable2().getMyID();
		InetAddress myIP =networkController.getFingerTable2().getMyIPAddress();
		int myPort =networkController.getFingerTable2().getMyPortNumber();
		Object [] mySuccessor = networkController.getFingerTable2().getMySuccessorPeer();
		Object [] peerY = networkController.getFingerTable2().getSuccessorPeer((Long)mySuccessor[0]);
		
		Long peerYID = (Long)peerY[0];
		InetAddress peerYIP = (InetAddress)peerY[1];
		int peerYPort = (Integer)peerY[2];
		
		System.out.println("My Successor: " + (Long)mySuccessor[0] + "Next one: " + (Long)peerY[0]);
		
		Message msg = Message.getSendYourPredMessage(myIP,myPort,peerYID);
		Client client = new Client(peerYIP, peerYPort,msg, networkController);
		new Thread(client).start();
		if (client.getExitValue() == Client.ERROR) {
			System.out.println("Resetting " + networkController.getLocalHostPort() + "from LOST SUCCESSOR METHOD");
			networkController.reset();
			return;
		}
		
		Message reply = client.getReplyMessage();
		
		Long peerZID = reply.getPredID();
		InetAddress peerZIP = reply.getPredIP();
		int peerZPort = reply.getPredPort();
		
		boolean isBetween = networkController.getFingerTable2().isBetween(peerZID,myID,peerYID);
		while ((peerZID != myID) && isBetween){
			
			msg = Message.getSendYourPredMessage(myIP,myPort,peerZID);
			Client loopClient = new Client(peerZIP, peerZPort,msg, networkController);
			new Thread(loopClient).start();
			if (loopClient.getExitValue(4000) != Client.SUCCEEDED) {
				break;
			}
			System.out.println("MyID: " + Long.toHexString(myID) +"Peer Y: " + Long.toHexString(peerYID) + "Next one: " + Long.toHexString(peerZID));
			reply = loopClient.getReplyMessage();
			
			peerYID = peerZID;
			peerYIP = peerZIP;
			peerYPort = peerZPort;
			peerZID = reply.getPredID();
			peerZIP = reply.getPredIP();
			peerZPort = reply.getPredPort();
			
			isBetween = networkController.getFingerTable2().isBetween(peerZID,myID,peerYID);
		}
		System.out.println ("Finishing successor loss recovery...");
		networkController.getFingerTable2().removeFromFingerTable((InetAddress)mySuccessor[1],(Integer)mySuccessor[2]);
		networkController.getFingerTable2().updateFingerTable(peerYIP, peerYPort);
		networkController.informSuccessorOfMyExistance();
		/*

		Find the finger table entry that immediately follows all the P�s in my table. Call it Y.
		Ask Y who his predecessor is. Call it Z.
		If Y can�t be contacted either, give up. Do a leave and then rejoin the network as a new node.
		While Z is not me and is �between� me and Y�
		Ask Z who his predecessor is.
		If Z cannot be contacted, stop loop.
		Otherwise, set Y:=Z and Z:=Z�s predecessor and repeat.
		At end of loop: Choose Y as my new successor. Contact Y and tell him I�m his new predecessor.
		 */
	}

	/**handleRegularLoss()
	 * This method just removes from the finger table the peer that left and forwards the message to the next best guy.
	 */
	public void handleRegularLoss() throws NullPointerException {
		System.out.println("\tForwardingFailureHandler: Lost Regular Peer");
		networkController.getFingerTable2().removeFromFingerTable(targetIP,targetPort);
		Object [] targetPeer = networkController.getFingerTable2().resolveTargetPeer(targetID);
		if (networkController.getFingerTable2().containsOnlyMyself()){
			System.out.println("Resetting " + networkController.getLocalHostPort() + "from LOST REGULAR PEER METHOD");
			exitStatus = Client.ERROR;
			networkController.reset();
		}
		else{
			Long newTargetID;
			if (targetPeer[0] == null)
				newTargetID = networkController.getIdentifier();
			else
				newTargetID = (Long)targetPeer[0];
			System.out.println("new Target ID" + newTargetID + "... and message to forward of type: ");
			System.out.println(messageToForward.getMessageType());
			Client newClient = new Client(newTargetID, messageToForward,networkController);
			new Thread(newClient).start();
			exitStatus = newClient.getExitValue();
		}
	}


	public static boolean getRunningStatus () {
		return alreadyRunning;
		
	}
	
	public void handleFileHolderLeft(Message message) throws NullPointerException{
		System.out.println("\tForwardingFailureHandler: Handling file holder loss");
		Object [] targetPeer = networkController.getFingerTable2().resolveTargetPeer(message.getFileID());
		Message messageToSend = Message.getPeerLeft(targetIP,targetPort,message.getFileID());
		if (targetPeer[1] == null){
			targetPeer[1] = networkController.getLocalHostIP();
			targetPeer[2] = networkController.getLocalHostPort();
		}
		Client client = new Client((InetAddress)targetPeer[1],(Integer)targetPeer[2],messageToSend,networkController);
		new Thread(client).start();
		client.getExitValue();
	}
	
	public void handleFindPredFailure(Message message) {
		System.out.println("\tForwardingFailureHandler: Could not send find_pred message");
		InetAddress lastIP = targetIP;
		int lastPort = targetPort;
		Long lastID = targetID;
		Long newTargetID = null;
		Long myID = null;
		if (networkController != null){
			myID = networkController.getIdentifier();
		}
		
		while ((newTargetID != myID) && (exitStatus == Client.ERROR)){
			networkController.getFingerTable2().removeFromFingerTable(lastIP,lastPort);
			Object [] targetPeer = networkController.getFingerTable2().getPredPeer(lastID);
			if (networkController.getFingerTable2().containsOnlyMyself()){
				System.out.println("Resetting " + networkController.getLocalHostPort() + "from FIND PRED FAILURE");
				exitStatus = Client.ERROR;
				networkController.reset();
				return;
			}
			else{
				if (targetPeer[0] == null){
					exitStatus = Client.ERROR;
					//networkController.reset();
					return;
				}
				else
					newTargetID = (Long)targetPeer[0];
				
				System.out.print("new Target ID" + newTargetID + "... and message to forward of type: ");
				System.out.println(messageToForward.getMessageType());
				Client newClient = new Client(newTargetID, messageToForward,networkController);
				new Thread(newClient).start();
				exitStatus = newClient.
				getExitValue(4000);
				if (exitStatus == Client.ERROR){
					lastIP = (InetAddress)targetPeer[1];
					lastPort = (Integer)targetPeer[2];
					lastID = newTargetID;
				}
			}
		}
	}
	
	
	public int getExitStatus () {
		if (finished.availablePermits()<1)
			return Client.ERROR;
		else{
		try {
			finished.tryAcquire(30, TimeUnit.SECONDS);
			return exitStatus;
		}
		catch (InterruptedException ex){
			System.out.println(ex.toString());
			return Client.ERROR;
		}
		}
		
		
	}

}

