package networkOverlay;

import java.net.InetAddress;
import java.util.ArrayList;
//import java.util.Collection;
import java.util.Hashtable;
import java.util.Vector;
import java.util.concurrent.Semaphore;
import java.security.*;


public class NetworkController implements Runnable {
	private InetAddress localHost;
	private ConnectionListener server;
	private int portNumber;
	private ArrayList<String> messagesToDisplay;
	private Semaphore networkGUISync;
	private FingerTable2 fingerTable2;
//	private MessageHandler messageHandler;
	private boolean joinFlag;
	private boolean online;
	private boolean predFindingInProgress = true;
	private boolean dedicatedPeer = false;
//	private MessageTargetDecider mtDecider;
	
	//for authentication
	private KeyPair keyPair;
	
	//to fulfill our keyholder responsiblities
	private Hashtable<Long, Vector<FileInfo>> fileKeys;
	//this is for generating challenge messages for file retracts
	
	
	//this one handles the case when we send a message and it gets forwarded, and
	//the person who responds is someone we didnt send it to directly, so we have to
	//remember about it in this class
	private ConnectionManager connectionManager = new ConnectionManager(this);
	
	
	//this guy handles the downloads and has the hash table for it
	private DownloadManager downloadManager = new DownloadManager(this);
	
	//this guy handls the uploads and the stuff we have published
	private UploadManager uploadManager = new UploadManager(this);
	
	//this guy handles the keywords that hash to our space, and any keyword lookups the user does
	private KeywordManager keywordManager = new KeywordManager(this);
	
	//Keeps track of if this instance of NetworkController is being accessed by the test suite
	private boolean test_suite;

	public NetworkController(int port){
		this(port, false);
	}	
	
	public NetworkController(int port, boolean test_suite)
	{
		start(port,test_suite);
	}	
	public void start(int port, boolean test_suite)
	{
		online = false;
		this.test_suite = test_suite;
		
		KeyPairGenerator keyPairGenerator = null;
		try{
			keyPairGenerator = KeyPairGenerator.getInstance("RSA");
		}
		catch(NoSuchAlgorithmException ex){
			ex.printStackTrace();
			System.exit(342345);
		}
		keyPair = keyPairGenerator.generateKeyPair();

		networkGUISync = new Semaphore(0);
		messagesToDisplay = new ArrayList<String>();
		
		fileKeys = new Hashtable<Long, Vector<FileInfo>>();
//		sharedFiles = new HashMap<Integer, FileInfo>();
		
		localHost = null;
		try {
			localHost = InetAddress.getLocalHost();
		}
		catch (Exception ex) {
			System.out.println(ex.toString());
		}
//		messageHandler = new MessageHandler(this);
		initializeServer(port);
		Message.setMyIP(localHost);
		Message.setMyPort(portNumber);
		System.out.println("about to do finger table");
		fingerTable2 = new FingerTable2(localHost,portNumber,this);
		//fingerTable = new FingerTable(localHost, portNumber, fingerTable2.getHashedPeerList());
		downloadManager = new DownloadManager(this);
		initiateJoinSequence();
		System.out.println("\t\t\t\t\tmyPort..." + portNumber);
		online = true;
		//mtDecider = new MessageTargetDecider(this);
		//new Thread(mtDecider).start();
		ensurePredIsSetUp();
		
	}

	//***************************************************************
	// ******************* Server Operations ************************
	//initializeServer
	public void initializeServer(int port){
		server = new ConnectionListener(port, this);
		portNumber = server.getPort();
//		Thread serverListenerThread = new Thread(this);
//		serverListenerThread.start();
	}
	//run method for the thread created above
	public void run () {
//		while (true){
//			Message message = server.waitForMessage();
//			messageHandler.handleMessage(message);
//		}
	}
	
	
	public void initiateJoinSequence() {
		joinFlag = true;
		Client client = new Client(localHost,portNumber,
				Message.getFindPredMessage(localHost, portNumber), this);
		new Thread(client).start();
		//client.getExitValue();
	}
	public void initiateExitSequence () {
		try{
			performExitOperations();
		}
		catch (NullPointerException ex){
			server.shutdown();
			System.exit(0);
		}
		
		System.exit(0);
	}
	public void performExitOperations() throws NullPointerException{
		//mtDecider.stopRunning();
		uploadManager.retractAllFiles();
		//TODO: retract All keywords
		online = false;
		Object [] mySuccessor = fingerTable2.getMySuccessorPeer();
		Object [] myPredecessor = fingerTable2.getMyPredecessorPeer();
		// Tell pred you are leaving and give him all your keys
		Hashtable<String, Vector<FileInfo>> keywordFiles = this.getKeywordManager().getKeywordFiles();
		Message messageForPred = Message.getSuccessorLeavingMessage(
				(InetAddress)mySuccessor[1], (Integer)mySuccessor[2], fileKeys,keywordFiles, (Long)myPredecessor[0]);
		Client predClient = new Client((InetAddress)myPredecessor[1],(Integer)myPredecessor[2],messageForPred,this);
		new Thread(predClient).start();
		// Tell successor you are leaving and who his new pred is.
		Message messageForSuccessor = Message.getPredLeavingMessage(
				(InetAddress)myPredecessor[1], (Integer)myPredecessor[2], (Long)mySuccessor[0]);
		Client successorClient = new Client ((InetAddress)mySuccessor[1], (Integer)mySuccessor[2], messageForSuccessor,this);
		new Thread(successorClient).start();
		
		//here I made it so that it timeouts after 5000 milliseconds
		predClient.getExitValue(5000);
		successorClient.getExitValue(5000);
		shutdownServer();
//		System.exit(0);
	}
	
	public void reset() {
		if (true || !dedicatedPeer){
			try{
				performExitOperations();
			}
			catch (NullPointerException ex){
				server.shutdown();
			}
			finally {
				start(portNumber, test_suite);
			}
		}
			
	}

	//***************************************************************
	// ******************* GUI Communication  ***********************

	public void displayIdentifierError(String message, String identifier){
		messagesToDisplay.add("Error: wrong identifier format or size: " + identifier);
		networkGUISync.release();
		
	}
	
	public void displayMessageForwarding(String message, String identifier){
		messagesToDisplay.add("Forwarding: " + message + " to " + identifier);
		networkGUISync.release();
		
	}

	public void displayMessageReceived(String message){
		messagesToDisplay.add("Received: " + message);
		networkGUISync.release();
	}
	
	public String displayMessageOnGUI() {
		try {
			networkGUISync.acquire();
		}
		catch (InterruptedException ex) {
			//It's fine to get an exception here because
			//of course we have to interrupted it while it's waiting,
			//when we end the program.
		}
		return messagesToDisplay.remove(0);
	}

	//***************************************************************
	//***************************************************************
	

	public long getIdentifier () {
		return fingerTable2.getMyID();
	}
	
//	public FingerTable getFingerTable(){
//		return fingerTable;
//	}

	public FingerTable2 getFingerTable2(){
		return fingerTable2;
	}
	
//	public MessageHandler getMessageHandler(){
//		return messageHandler;
//	}

	//*** methods for handling file keys whose IDs lie in our space ***
	
	/**only returns the first file key, may return null if no object found, 
	 * should use getFileHolder(Long key, Long fileHolderID)*/
	@Deprecated
	public FileInfo getFileHolder(Long key) {
		//may return null
		if(fileKeys.containsKey(key))
			return fileKeys.get(key).get(0);
		else return null;
	}

	public FileInfo getFileHolder(Long key, Long fileHolderID) {
		System.out.println("proposed holder ID: " + fileHolderID);
		Vector<FileInfo> fileInfos = fileKeys.get(key);
		if (fileInfos == null)
			return null;
		for(int i = 0; i < fileInfos.size(); i++){
			System.out.println(fileInfos.get(i));
			if(fileInfos.get(i).getHolderID().equals(fileHolderID)){
				return fileInfos.get(i);
			}
			else if(fileInfos.get(i).getHolderID() == null)
				System.out.println("NULLLLL");
		}
		//if not in the table
		return null;
	}
	
	public Vector<FileInfo> getAllFileHolders(Long key){
		return fileKeys.get(key);
	}
	
	/**remembers that someone has published that file and we hold the key*/
	public void putFileKey(Long key, FileInfo value) {
//		if(fileKeys.containsKey(key)){
//			System.out.println("contains the key");
//			fileKeys.get(key).putHolder(value.getHolderIPAddress(), 
//					value.getHolderPort());
//		}
//		else{
//			System.out.println("no key");
//			fileKeys.put(key, value);
//		}
//		System.out.println("number of keys: " + fileKeys.get(key).getAllHolderIDs().size());
		if(fileKeys.containsKey(key)){
			Vector<FileInfo> fileInfos = fileKeys.get(key);
			//it has the key
			if(fileInfos == null){
				//if Vector<FileInfo> not instantiated, instatiate
				System.out.println("WARNING: for some reason we had the key but it's " +
						"Vector<FileInfo> was not instantiated");
				fileInfos = new Vector<FileInfo>();
				fileKeys.put(key, fileInfos);
			}
			
			//now add if it does not already contain the value
			boolean hasKeyHolder = false;
			//loop to check the fileInfos for the key holder
			for(int i = 0; i < fileInfos.size(); i++)
				if(fileInfos.get(i).getHolderID().equals(value.getHolderID()))
					hasKeyHolder = true;
			//finally, add it if we found no key holder for that holder ID
			//(we don't want him to add the same one twice)
			if(!hasKeyHolder)
				fileKeys.get(key).add(value);
		}
		//does not yet contain fileInfo for that ID, so make a new one
		else{
			Vector<FileInfo> newVector = new Vector<FileInfo>();
			newVector.add(value);
			fileKeys.put(key, newVector);
		}
	}
	
	public FileInfo removeFileHolder(Long fileID, Long holderID) {
		System.out.println("removing file holder");
		FileInfo returnValue = null;
		Vector<FileInfo> fileInfos = fileKeys.get(fileID);
		if(fileInfos != null && fileInfos.size() > 0){
			//now search the vector and remove the FileInfos with holderID
			for(int i = 0; i < fileInfos.size(); i++){
				if(fileInfos.get(i).getHolderID().equals(holderID))
					returnValue = fileInfos.remove(i);
			}
			//finally check if we can remove the entry in fileKeys
			if(fileInfos.size() == 0)
				fileKeys.remove(fileID);
		}
		//else, do nothing cuz there's nothing to remove
		return returnValue;
	}

	public void setFileKeys(Hashtable<Long, Vector<FileInfo>> receivedFileKeys) {
		this.fileKeys = receivedFileKeys;
	}
	public void addFileKeys(Hashtable<Long, Vector<FileInfo>> receivedFileKeys) {
		//TODO check if there are any overlapping keys btwn received and my own keys.
		//Hashtable<Long, Vector<FileInfo>> fileKeysBackUp = fileKeys;		
		this.fileKeys.putAll(receivedFileKeys);
	}
	/**This method is called whenever a new peer is added and we want to give him
	 * the file keys that lie within his space, removing them from our table.  
	 * This actually gives him any keys that we have that we shouldn't have.
	 */
	Hashtable<Long, Vector<FileInfo>>  getMySuccessorsFileKeys(Long successorID) {
		Hashtable<Long, Vector<FileInfo>> successorFileKeys = new Hashtable<Long, Vector<FileInfo>>();
		Long [] keys = new Long[fileKeys.keySet().size()];
		keys = fileKeys.keySet().toArray(keys);
		Long myID = getIdentifier();
		for (int i =0; i< keys.length;i++){
			//Integer key:keys
			Long key = keys[i];
			if (fingerTable2.isBetween(successorID, myID, key)){
				System.out.println("My successor's file key:" + key);
				successorFileKeys.put(key, fileKeys.get(key));
				fileKeys.remove(key);
			}
		}
		return successorFileKeys;
	}
	/**This method is called whenever a new peer is added and we want to give him
	 * the keywords that lie within his space, removing them from our table.  
	 * This actually gives him any keywords that we have that we shouldn't have.
	 */
	Hashtable<String, Vector<FileInfo>>  getMySuccessorKeywordFiles(Long successorID) {
		Hashtable<String, Vector<FileInfo>> successorKeywordFiles = new Hashtable<String, Vector<FileInfo>>();
		Hashtable<String, Vector<FileInfo>> keywordFiles = keywordManager.getKeywordFiles();
		String [] keys = new String[keywordFiles.keySet().size()];
		keys = keywordFiles.keySet().toArray(keys);
		Long myID = getIdentifier();
		for (int i =0; i< keys.length;i++){
			//Integer key:keys
			Long key = Hasher.hashPeerLong(keys[i]);
			if (fingerTable2.isBetween(successorID, myID, key)){
				System.out.println("My successor's keyword:" + key);
				//System.out.println("File Info " + keywordFiles.get(keys[i]));
				successorKeywordFiles.put(keys[i], keywordFiles.get(keys[i]));
				keywordFiles.remove(key);
			}
		}
		return successorKeywordFiles;
	}
	
	
	
	public InetAddress getLocalHostIP() {
		return localHost;
	}
	public int getLocalHostPort() {
		return portNumber;
	}
	
	public boolean getJoinFlag (){
		return joinFlag;
	}
	
	public void setJoinFlag (boolean flag){
		joinFlag = false;
	}
	
	public boolean getInTestSuite(){return test_suite;}
	
	//for downloadManager
	public DownloadManager getDownloadManager(){return downloadManager;}
	
	//for connectionManager
	public ConnectionManager getConnectionManager(){return connectionManager;}
	
	//for uploadManager
	public UploadManager getUploadManager(){return uploadManager;}
	
	public KeywordManager getKeywordManager(){return keywordManager;}
	
	public void informSuccessorOfMyExistance(){
		Object[] mySuccessor = fingerTable2.getMySuccessorPeer(); 
		long mySuccessorID = (Long)mySuccessor[0];
		InetAddress mySuccessorIP = (InetAddress)mySuccessor[1];
		int mySuccessorPort = (Integer)mySuccessor[2];
		System.out.println(Long.toHexString(getIdentifier()) + ":I am the pred of " + Long.toHexString(mySuccessorID));
		Message message = Message.getIAmYourPredMessage(localHost,portNumber, getIdentifier(), mySuccessorID);
		Client client = new Client(mySuccessorIP, mySuccessorPort, message, this);
		new Thread(client).start();
	}
	
	public boolean getOnlineStatus(){
		return online;
	}
	public void setDedicatedPeerTrue(){
		dedicatedPeer = true;
	}
	public void setOnlineStatus(boolean status){
		online = status;
	}
	public boolean checkPredecessor(){
		InetAddress predIP = fingerTable2.getMyPredIP();
		Integer predPort = fingerTable2.getMyPredPort();
		
		if((predIP ==null)||(predPort==null)){
			System.out.println("Predecessor is null. Not performing points-to now");
			return false;
		}
		Long predID = fingerTable2.getMyPredID();
	
		Client predCheck = new Client(predIP,predPort, 
			Message.getTestConnectionMessage(localHost,portNumber,predID), this);
		
		new Thread(predCheck).start();
		int exitStatus = predCheck.getExitValue();
		if (exitStatus == Client.SUCCEEDED){
			return true;
		}
		else {
			omgFindMyPred();
			return false;
		}
	}
	
	public void omgFindMyPred(){
		if (!predFindingInProgress){
			predFindingInProgress = true;
			System.out.println("\t\t\tIn omgFindMyPred()");
			Message messageToSend= Message.getFindNewPredMessage(localHost, portNumber);
			Client client = new Client(localHost,portNumber,messageToSend, this);
			new Thread(client).start();
			client.getExitValue();
			predFindingInProgress = false;
		}
	}
	
	public void ensurePredIsSetUp(){
		try{
			int counter = 0;
			Thread.sleep(1000);
			while (fingerTable2.getMyPredIP() ==null){
				Client client = new Client(localHost,portNumber,
						Message.getFindPredMessage(localHost, portNumber), this);
				new Thread(client).start();
				client.getExitValue();
				Thread.sleep(4000);
				if (counter == 5){
					Thread.sleep(10000);
					System.out.println("Something catastrophic has happened.. resetting again!");
					reset();
					break;
				}
				else
					counter++;
			}
			predFindingInProgress = false;
		
		}
		catch (InterruptedException ex){
			
		}
	}
	
//	public MessageTargetDecider getMessageTargetDecider (){
//		return mtDecider;
//	}
	
	public void shutdownServer (){
		setOnlineStatus(false);
		server.shutdown();
		
	}
	
//	public void gracefulExitForMessageTests(){
//		mtDecider.stopRunning();
//		online = false;
//		uploadManager.retractAllFiles();
//		//TODO: retract All keywords
//		Object [] mySuccessor = fingerTable2.getMySuccessorPeer();
//		Object [] myPredecessor = fingerTable2.getMyPredecessorPeer();
//		// Tell pred you are leaving and give him all your keys
//		Hashtable<String, Vector<FileInfo>> keywordFiles = this.getKeywordManager().getKeywordFiles();
//		Message messageForPred = Message.getSuccessorLeavingMessage(
//				(InetAddress)mySuccessor[1], (Integer)mySuccessor[2], fileKeys,keywordFiles, (Long)myPredecessor[0]);
//		Client predClient = new Client((InetAddress)myPredecessor[1],(Integer)myPredecessor[2],messageForPred,this);
//		new Thread(predClient).start();
//		// Tell successor you are leaving and who his new pred is.
//		Message messageForSuccessor = Message.getPredLeavingMessage(
//				(InetAddress)myPredecessor[1], (Integer)myPredecessor[2], (Long)mySuccessor[0]);
//		Client successorClient = new Client ((InetAddress)mySuccessor[1], (Integer)mySuccessor[2], messageForSuccessor,this);
//		new Thread(successorClient).start();
//		
//		//here I made it so that it timeouts after 5000 milliseconds
//		predClient.getExitValue(5000);
//		successorClient.getExitValue(5000);
//		//setOnlineStatus(false);
//		shutdownServer();
//		
//		
//	}
	
	public KeyPair getKeyPair(){return keyPair;}
}
