package networkOverlay;

import java.io.File;
import java.io.Serializable;
import java.net.InetAddress;
import java.security.*;

import networkOverlay.KeywordManager.KeywordFile;

/*
 * FileInfo
 * this class encapsulates all the various details that we
 * may want to communicate about a file that we are sharing
 */
@SuppressWarnings("serial")
public class FileInfo implements Serializable{
	private Long id;
	private String name;
	private long fileSize;
	//we could even put a String that gives a description of the file
	//or we could put keywords for searching etc

	//the public key of the keyholder, for retract authentication
	private PublicKey fileHolderPublicKey;
	
	//if these are null (or port 0) it means that we hold the file
	private Long holderID = 0L;
	private InetAddress holderIPAddress = null;
	private int holderPort = 0;
	
	private /*transient*/ File filePath;//just the path on the local machine
	//maybe some other transient fields to monitor its popularity or something
	
	//for a file that we have (will initialize to null, indicating "local host")
	public FileInfo(File file){
//		id = Hasher.hashPeerLong(file.getName());
		id = Hasher.hashFileContent(file);
		filePath = file;
		name = file.getName();
		fileSize = file.length();
	}
	
	//for file someone else has
	//this method is actually called whenever we are sharing and publishing a new file
	public FileInfo(File file, InetAddress holderIPAddress, int holderPort, PublicKey pk){
		this(file);
		this.holderIPAddress = holderIPAddress;
		this.holderPort = holderPort;
		fileHolderPublicKey = pk;
		
		this.holderID = Hasher.hashPeerLong(holderIPAddress, holderPort);
	}

	//for making lookups
	/**In this case, if a File object isn't given, user must give the fileID (a Long)*/
	public FileInfo(String fileName, Long fileID, InetAddress holderIPAddress, int holderPort){
		id = fileID;
		name = fileName;
		this.holderIPAddress = holderIPAddress;
		this.holderPort = holderPort;
		
		this.holderID = Hasher.hashPeerLong(holderIPAddress, holderPort);
	}

	///////set and get methods///////
	public Long getID() {return id;}
	public void setID(Long id) {this.id = id;}
	public String getName() {return name;}
	public void setName(String name) {this.name = name;}
	public long getFileSize() {return fileSize;}
	public void setFileSize(long fileSize) {this.fileSize = fileSize;}
	public File getFilePath() {return filePath;}
	public void setFilePath(File filePath) {this.filePath = filePath;}
	
	
	public InetAddress getHolderIPAddress() {
		return holderIPAddress;
	}

	
	public void putHolderIPAddress(InetAddress holderIPAddress) {
		this.holderIPAddress = holderIPAddress;
	}
	
	public int getHolderPort() {
		return holderPort;
	}	
	
	public void putHolderPort(int holderPort) {
		this.holderPort = holderPort;
	}
	
	public Long getHolderID() {
		return holderID;
	}
	
	public void putHolderID(Long holderID) {
		this.holderID = holderID;
	}
	
	public PublicKey getFileHolderPublicKey(){return fileHolderPublicKey;}
	
	@Override
	public String toString(){
		return
		"file ID: " + id +
		"\nfile name:" + name +
		"\nfile size: " + fileSize +
		"\npublic key: " + fileHolderPublicKey +
		"\nholder ID: " + holderID +
		"\nholder IP address: " + holderIPAddress +
		"\nholder port: " + holderPort +		
		"\nfile path:" + filePath;
	}
	
	@Override
	public boolean equals(Object obj){
		if(obj == null || !(obj instanceof FileInfo))
			return false;
		FileInfo other = (FileInfo)obj;
		return id.equals(other.id);
	}
}
