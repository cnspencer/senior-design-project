package networkOverlay;

/** MessageTargetDecider
 * This class decides which finger table entry a message is sent to based on probabilities of 
 * previous successful message deliveries.
 * 
 *  It trys to avoid message dropping.
 *
 */
public class MessageTargetDecider implements Runnable {

	private NetworkController networkController;
	private FingerTable2 fingerTable;
	private double [] ai = new double[32];
	private int probeMessagesSentCounter=0;
	private int probeMessagesReceivedCounter=0;

	private int regularMessagesSentCounter=0;
	private int regularMessagesReceivedCounter=0;
	
	private boolean greenLight = true;
	
	public MessageTargetDecider(NetworkController nc){
		networkController = nc;
		fingerTable = nc.getFingerTable2();
	}
	

	public void run(){
		try{
			int counter = 0;
			Thread.sleep(10000);
			while (greenLight){
				counter++;
				if (counter == 10){
					counter = 0;
					//calculateWs();
				}
					
				int fingerTableIndex = (int)(Math.random()*32);
				Long targetPeer = fingerTable.getPeerByIndex(fingerTableIndex);
				if (targetPeer == null)
					continue;
				Thread.sleep(4000);
				Message probeMessage = Message.getProbeMessage(fingerTable.getMyIPAddress(), fingerTable.getMyPortNumber(),
						fingerTableIndex, fingerTable.getMyID());
				Client client = new Client(targetPeer, probeMessage, networkController);
				probeMessagesSentCounter++;
				new Thread(client).start();
				client.getExitValue();
			}
		}
		catch (Exception ex){
			ex.printStackTrace();
		}
	}
	
	public Long getBestPeer(Long targetPeer){
		
		return 1L;
	}
	
	
	public void acceptProbeMessage (Message message) {
		if (message.getTargetID()==(long)fingerTable.getMyID()){
				//System.out.println("Received Message successfully from " + message.getfingerTableUpdateIndex()
					//	+ " : " + Long.toHexString(fingerTable.getPeerByIndex(message.getfingerTableUpdateIndex())));
				probeMessagesReceivedCounter++;
		}
		
	}
	
	public void printResults (){
		System.out.println("****************************************************************");
		System.out.println("\t\t\tREPORT FOR "+ Long.toHexString(fingerTable.getMyID()));
		System.out.println("Probe Messages Sent: "+ probeMessagesSentCounter);
		System.out.println("Probe Messages Received: "+ probeMessagesReceivedCounter);
		System.out.println("\t\t\t~END OF REPORT~");
		
		System.out.println("****************************************************************");
	}
	
	public void stopRunning (){
		greenLight = false;
	}
	
	
	
	
	
}
