//package networkOverlay;
//import java.net.*;
//public class MessageHandler {
//	
//	private NetworkController networkController;
//	
//	public MessageHandler(NetworkController nc){
//		networkController = nc;
//	}
//	
//	public void handleMessage(Message message){
//		switch (message.getMessageCategory()){
//			case JOIN: {handleJoinMessageType(message);break;}
//			case FILE: {handleFileMessageType(message);break;}
//			case UPDATE: {handleUpdateMessageType(message);break;}
//			case EXIT: {handleExitMessageType(message);break;}
//			default : {System.out.println("Received message not recognized. Type: " + message.getMessageType().toString());break;}
//			}
//	}
//	
//	private void handleJoinMessageType(Message message){
//		switch (message.getMessageType()){
//			case HOST_VERIFICATION: {System.out.println("In host veri");break;}
//			case FIND_PRED:{
//				handleFindPredecessor(message);	
//				break;
//				}	
//			case FOUND_PRED:{			
//				handleFoundPredecessor(message);			
//				break;			
//				}		
//			case FIND_SUCCESSOR: {
//				handleFindSuccessor(message); 
//				break;
//			}
//			case FOUND_SUCCESSOR: {
//				handleFoundSuccessor(message); 
//				break;
//			}
//		}
//	}
//	private void handleFileMessageType(Message message){
//		switch (message.getMessageType()){
//		
//		//*** for adding a file or removing it ***		
//		//tell the other peers that we have the file
//		case PUBLISH_FILE:
//			handlePublishFile(message);
//			break;
//		//tell other peers that we do not have the file anymore
//		case RETRACT_FILE:
//			handleRetractFile(message);
//			break;
//			
//		//*** for finding the keyholder of the file ***
//		//get the keyholder for the file
//		case LOOKUP_FILE:
//			handleLookupFile(message);
//			break;
//		//say who has the file
//		case SEND_KEYHOLDER:
//			handleSendKeyholder(message);
//			break;
//		//say that no one has published such a file
//		case LOOKUP_FAILED:
//			handleLookupFailed(message);
//			break;
//			
//		//*** for downloading the file once we know who has it ***
//		//to download (we send it right away)
//		case DOWNLOAD_FILE:
//			handleDownloadFile(message);
//			break;
//		//say that we are about to send the file information
//		case UPLOAD_FILE:
//			handleUploadFile(message);
//			break;
//		//say that we actually dont have the file, or it failed for some other reason
//		case DOWNLOAD_FAILED:
//			handleDownloadFailed(message);
//			break;
//			
//		//*** for communication while the file is downloading ***
//		//TODO: not yet implemented	
//		case CANCEL_FILE_DOWNLOAD:
//			//TODO
//			break;
//		case CANCEL_FILE_UPLOAD:
//			//TODO
//			break;		
//
//		default :
//			System.out.println("Received message not recognized. Type: " + 
//			message.getMessageType().toString() + "SubType: " + 
//			message.getMessageType().toString());
//			break;
//		}
//	}
//	
//	private void handlePublishFile(Message message){
//		//when we recieve this message, we either have to forward it to the keyholder,
//		//or if we are the keyholder, we register it
//		
//		long fileID = message.getFileInfo().getID();
//		//this method is from Charles'(my) finger table class (FingerTable) :P
////		int targetPeer = networkController.getFingerTable().resolveTarget(fileID);
//		//TODO implement this correctly
//		Object [] targetPeer = null;
//		
//		if(targetPeer == FingerTable2.MYSELF) {
//			networkController.putFileKey(fileID, message.getFileInfo());
//			
//			//if(networkController.getInTestSuite()) {
//				System.out.println(Long.toHexString(networkController.getFingerTable2().getMyID()) + " published file: " + message.getFileInfo().getFilePath() + " with file ID: " + fileID);
//			//}
//		}
//		else{
//			//TODO forward message
//		}
//	}
//	private void handleRetractFile(Message message){
//		//when we recieve this, we just take the file off our fileKey list if we can
//		//returns null if the key wasnt even in our list
//		FileInfo removedFile = networkController.removeFileKey(message.getFileInfo().getID());
//		
//		//if(networkController.getInTestSuite()) {
//			if(removedFile == null)
//			{
//				System.out.println(Long.toHexString(networkController.getFingerTable2().getMyID()) + " failed to retract file. The file key doesn't exit in the list.");
//			}
//			else
//			{
//				System.out.println(Long.toHexString(networkController.getFingerTable2().getMyID()) + " retracted file: " + removedFile.getFilePath() + " with file ID: " + removedFile.getID());
//			}
//		//}
//	}
//	private void handleLookupFile(Message message){
//		//when we recieve this, and the fileID is in our space we look it up, otherwise forward it
//
//		long fileID = message.getFileInfo().getID();
//		//this method is from Charles'(my) finger table class (FingerTable) :P
////		int targetPeer = networkController.getFingerTable().resolveTarget(fileID);
//		//TODO implement this correctly
//		Object [] targetPeer = null;
//		
//		if(targetPeer == FingerTable2.MYSELF){
//			FileInfo fileInfo = networkController.getFileKey(fileID);
//			//TODO send back a message with this info
//		}
//		else{
//			//TODO forward message
//		}
//	}
//	private void handleSendKeyholder(Message message){
//		//when we recieve this message, it means that we have recieved the keyholder
//		//for a file that we wanted
//		//now we can download it
//		
//		
//		
//	}
//	private void handleLookupFailed(Message message){
//		//TODO
//	}
//	private void handleDownloadFile(Message message){
//		//TODO
//	}
//	private void handleUploadFile(Message message){
//		//TODO
//	}
//	private void handleDownloadFailed(Message message){
//		//TODO
//	}
//	
//	
//	
//	private void handleUpdateMessageType(Message message){
//		switch (message.getMessageType()){
//		
//
//			default :
//				System.out.println("Received message not recognized. Type: " + 
//				message.getMessageType().toString() + "SubType: " + 
//				message.getMessageType().toString());
//			break;
//		
//		}
//	}
//
//
//	private void handleExitMessageType(Message message){}
//	
//	//Handling the subtypes
//	private void handleFindPredecessor(Message message){
//		Object [] predPeer = networkController.getFingerTable2().getPredPeer(message.getSourceID()); 
//		if (predPeer[0] == FingerTable2.MYSELF){
//			InetAddress localIP = networkController.getLocalHostIP();
//			int localPort = networkController.getLocalHostPort();
//			networkController.getFingerTable2().updateFingerTable(message.getSourceIP(), message.getSourcePort());
//			
//			Message msg =  Message.getFoundPredMessage(localIP, localPort,this.networkController.getFingerTable2(),
//					networkController.getMySuccessorsFileKeys(),message.getSourceID());
//			Client client = new Client(message.getSourceIP(), message.getSourcePort(),
//					msg, networkController);
//			new Thread(client).start();
//		}
//		else{
//			InetAddress targetIP = (InetAddress)predPeer[1];
//			int targetPort = (Integer)predPeer[2];
//			Client client = new Client(targetIP, targetPort,message, networkController);
//			new Thread(client).start();
//			networkController.getFingerTable2().updateFingerTable(message.getSourceIP(), message.getSourcePort());
//		}
//	}
//	
//	private void handleFoundPredecessor(Message message){
//		if (networkController.getJoinFlag()){
//			System.out.println("Pred Found........." + message.getSourceID());
//			networkController.getFingerTable2().setFingerTable(message.getFingerTable2());
//			networkController.setFileKeys(message.getFileKeyTable());
//			System.out.println("Calling finger table adjust method from messageHandler");
//			networkController.getFingerTable2().adjustFingerTable();
//			networkController.setJoinFlag(false);
//		}
//	}
//	
//	private void handleFindSuccessor(Message message){
//		Object [] predPeer = networkController.getFingerTable2().getPredPeer(message.getSuccessorID());
//		if (predPeer[0]==FingerTable2.MYSELF){
//			Object [] successorInfo = networkController.getFingerTable2().getMySuccessorPeer();
//			System.out.println("Sending Successor : " + message.getSourceID());
//			InetAddress successorIP;
//			int successorPort; 
//			if (successorInfo == FingerTable2.MYSELF){
//				successorIP = networkController.getLocalHostIP();
//				successorPort =  networkController.getLocalHostPort();
//			}
//			else {
//				System.out.println("ID of succ" + successorInfo[0]+ "port of succpeer" + successorInfo[2]);
//				successorIP = (InetAddress)successorInfo[1];
//				successorPort =  (Integer)successorInfo[2];
//			}
//			Message msg = Message.getFoundSuccessorMessage(message, successorIP, successorPort, (Long)successorInfo[0]);
//			Client client = new Client(message.getSourceIP(),message.getSourcePort(),msg, networkController);
//			new Thread(client).start();
//			networkController.getFingerTable2().updateFingerTable(message.getSourceIP(), message.getSourcePort());
//		}
//		else{
//			InetAddress targetIP = (InetAddress)predPeer[1];
//			int targetPort = (Integer)predPeer[2];
//			Client client = new Client(targetIP, targetPort,message, networkController);
//			new Thread(client).start();
//			networkController.getFingerTable2().updateFingerTable(message.getSourceIP(), message.getSourcePort());
//		}
//			
//	}
//	
//	
//	private void handleFoundSuccessor(Message message){
//		//System.out.println(" FOUND SUCCESSOR: " + message.getSuccessorID());
//		networkController.getFingerTable2().updateFingerTableEntry(message.getSuccessorIP(), message.getSuccessorPort(), message.getfingerTableUpdateIndex());
//		networkController.getFingerTable2().removeInconsistentPeerEntries();
//		
//	}
//	
//	
//	
//
//}
