package networkOverlay;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetAddress;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Vector;


public class Hasher {
	private static MessageDigest hasher;
	
	static{
		try{
			hasher = MessageDigest.getInstance("SHA-1");
		}
		catch(NoSuchAlgorithmException ex){
			ex.printStackTrace();
			System.exit(-65536);
		}	
	}
	
	//we don't really want to instantiate it
	private Hasher(){}
	


	//***************************************************************
	// ******************* Hashing Operations ***********************
	
//	public static Vector<String> hashPeerList(Vector<InetAddress> peerListIP, Vector<Integer> peerListPorts){
//		Vector<String> hashedPeerList = new Vector<String>();
//		for (int i = 0; i < peerListIP.size(); i ++) {
//			hashedPeerList.add(hashPeerString(peerListIP.get(i)+ ":" + peerListPorts.get(i)));
//		}
//		return hashedPeerList;
//	}


	//hashPeerString
	//This method will hash a String into an int hashcode
//	public static String hashPeerString(String peer){
//		byte[] inputBytes, digestBytes;		
//		inputBytes = peer.getBytes();//each character is a byte, may not work with accented chars etc.
//		digestBytes = hasher.digest(inputBytes);
//		if(digestBytes.length != 20)//160 bits from SHA-1
//			//shouldn't get thrown, so we can make it a RuntimeException
//			throw new RuntimeException("Hash Algorithm failed " + digestBytes.length);
//		
//		int result = 0;
//		//we compress the 160 bits down to 32 bits
//		//or, the 20 bytes down to 1 int
//		for(int row = 0; row < 5; row++){//five "rows" of 32 bits to me anded in
//			//basically we can split the 160 bits into 5 groups of 32 (thats what there are 5 iterations)
//			//and we XOR all of them together (that's what ^= does) may consider +=
//			//and we have to left shift to get each byte in the right part of the int
//			
//			result ^= digestBytes[4*row + 0] << 0;
//			result ^= digestBytes[4*row + 1] << 8;
//			result ^= digestBytes[4*row + 2] << 16;
//			result ^= digestBytes[4*row + 3] << 24;
//		}
//		String resultHex = "0X";
//		resultHex += Integer.toHexString(result);
//		//System.out.println(resultHex);
//		return resultHex;
//		
//	}
	
	//hashPeer
	//turns an array of bytes into a hashed int
//	public static int hashPeerInt(byte[] inputBytes){
//		byte[] digestBytes;
//		digestBytes = hasher.digest(inputBytes);
//		if(digestBytes.length != 20)//160 bits from SHA-1
//			//shouldn't get thrown, so we can make it a RuntimeException
//			throw new RuntimeException("Hash Algorithm failed " + digestBytes.length);
//		
//		int result = 0;
//		//we compress the 160 bits down to 32 bits
//		//or, the 20 bytes down to 1 int
//		for(int row = 0; row < 5; row++){//five "rows" of 32 bits to me anded in
//			//basically we can split the 160 bits into 5 groups of 32 (thats why there are 5 iterations)
//			//and we XOR all of them together (that's what ^= does) may consider +=
//			//and we have to left shift to get each byte in the right part of the int
//			
//			result ^= digestBytes[4*row + 0] << 0;
//			result ^= digestBytes[4*row + 1] << 8;
//			result ^= digestBytes[4*row + 2] << 16;
//			result ^= digestBytes[4*row + 3] << 24;
//		}
//		return result;		
//	}

	
	//turns an InetAddres and portNumber into a hashed int
//	public static int hashPeerInt(InetAddress ipAddress, int portNumber){
//		return hashPeerInt(ipAddress + ":" + portNumber);
////		byte[] ipBytes, portBytes, inputBytes;
////		ipBytes = ipAddress.getAddress();
////		portBytes = new byte[4];
////		//an int is 4 bytes, so with the following code, we simply
////		//fill portBytes with one of those bytes from portNumber
////		portBytes[0] = (byte)(0x000000ff & portNumber);
////		portBytes[1] = (byte)(0x000000ff & (portNumber >> 8));
////		portBytes[2] = (byte)(0x000000ff & (portNumber >> 16));
////		portBytes[3] = (byte)(0x000000ff & (portNumber >> 24));
////		
////		//merge 2 arrays into 1
////		inputBytes = new byte[ipBytes.length + portBytes.length];
////		for(int i = 0; i < ipBytes.length; i++){
////			inputBytes[i] = ipBytes[i];
////		}
////		for(int i = 0; i < portBytes.length; i++){
////			inputBytes[ipBytes.length + i] = portBytes[i];
////		}
////	
////		return hashPeerInt(inputBytes);
//	}	
	
	//turns a String into a hashed int
//	public static int hashPeerInt(String peer){
//		byte[] inputBytes;		
//		inputBytes = peer.getBytes();//each character is a byte, may not work with accented chars etc.
//		
//		return hashPeerInt(inputBytes);
//	}
	
//	//hashes the file content and returns the file hash
//	public static long hashFileContent(File file){
//		BufferedInputStream bis = null;
//		try{
//			System.out.println("in try");
//			bis = new BufferedInputStream(new FileInputStream(file));
//			//for now we can just read the entire file into memory
//			byte[] byteArray = new byte[(int)file.length()];
//			bis.read(byteArray);
//			
////			System.out.
//			System.out.println("about to print bytes, length: " + byteArray.length);
//			for(int i = 0; i < byteArray.length; i++){
//				//System.out.println(byteArray[i]);
//			}
//
//			System.out.println("about to call hashPeerLong");
//			return hashPeerLong(byteArray);
//		}
//		catch(FileNotFoundException ex){
//			ex.printStackTrace();
//			try{
//				bis.close();
//			}
//			catch(IOException ioex){
//				System.out.println("Can't close file");
//				ioex.printStackTrace();
//			}
//		}
//		catch(IOException ex){
//			ex.printStackTrace();
//			try{
//				bis.close();
//			}
//			catch(IOException ioex){
//				System.out.println("Can't close file");
//				ioex.printStackTrace();
//			}
//		}
//		throw new RuntimeException("Error occurred while hashing file content");
//	}	
	
	/**This is the same as the previous hashFileContent, but it uses a buffer so that it
	 * doesn't have to load the whole file all at once.  It hashes piece by piece, but I
	 * tested it and it still gives the exact same hashes as using the other method.
	 */
	public static long hashFileContent(File file){
		//this is the max buffer length
		int maxBufferLength = 4096;
		long fileLength = file.length();
		FileInputStream fis = null;
		
		//necessary to make a new hasher because we don't want another thread to update
		//this hash in the middle and skrew things up
		MessageDigest fileHasher = null;
		
		//make a special hasher just for this function
		try{
			fileHasher = MessageDigest.getInstance("SHA-1");
		}
		catch(NoSuchAlgorithmException ex){
			ex.printStackTrace();
			return -1L;
		}	
		
		try{
			System.out.println("in try");
			fis = new FileInputStream(file);
			
			long remainingBytes = fileLength;//the bytes we have left, gets decremented
			byte[] buffer = new byte[maxBufferLength];//buffer to store read bytes for processing
			
			//loop with buffer
			while(remainingBytes > 0){
				if(remainingBytes > maxBufferLength){
					fis.read(buffer);	
					fileHasher.update(buffer);
					remainingBytes -= maxBufferLength;
				}
				//this handles the very last piece that is smaller than maxBufferLength
				else{
					fis.read(buffer);	
					fileHasher.update(buffer, 0, (int)remainingBytes);
					remainingBytes = 0L;
				}
			}
			
			//complete the hashing
			byte[] digestBytes = fileHasher.digest();
			
			//finalize result
			long result = 0;
			//we compress the 160 bits down to 32 bits
			//or, the 20 bytes down to 1 int
			for(int row = 0; row < 5; row++){//five "rows" of 32 bits to me anded in
				//basically we can split the 160 bits into 5 groups of 32 (thats why there are 5 iterations)
				//and we XOR all of them together (that's what ^= does) may consider +=
				//and we have to left shift to get each byte in the right part of the int
				
				result ^= digestBytes[4*row + 0] << 0;
				result ^= digestBytes[4*row + 1] << 8;
				result ^= digestBytes[4*row + 2] << 16;
				result ^= digestBytes[4*row + 3] << 24;
			}
			if (result<0)
				return (Math.abs(result) + (long)Math.pow(2, 31));
			else
				return result;	
		}
		catch(FileNotFoundException ex){
			ex.printStackTrace();
			try{
				fis.close();
			}
			catch(IOException ioex){
				System.out.println("Can't close file");
				ioex.printStackTrace();
			}
		}
		catch(IOException ex){
			ex.printStackTrace();
			try{
				fis.close();
			}
			catch(IOException ioex){
				System.out.println("Can't close file");
				ioex.printStackTrace();
			}
		}
		throw new RuntimeException("Error occurred while hashing file content");
	}
	
	
	
	//*****************************************************************************
	// Hashing and returning a long
	

	public static Vector<Long> hashPeerListLong(Vector<InetAddress> peerListIP, Vector<Integer> peerListPorts){
		Vector<Long> hashedPeerList = new Vector<Long>();
		for (int i = 0; i < peerListIP.size(); i ++) {
			hashedPeerList.add(hashPeerLong(peerListIP.get(i)+ ":" + peerListPorts.get(i)));
		}
		return hashedPeerList;
	}
	
	
	public static long hashPeerLong(InetAddress ipAddress,int portNumber){
		return hashPeerLong(ipAddress + ":" + portNumber);
	}
	
	public static long hashPeerLong(String peer){
		byte[] inputBytes;		
		inputBytes = peer.getBytes();//each character is a byte, may not work with accented chars etc.
		
		return hashPeerLong(inputBytes);
	}
	
	public static long hashPeerLong(byte[] inputBytes){
		if(inputBytes.length == 0)
			throw new IllegalArgumentException();
		
		byte[] digestBytes;
		digestBytes = hasher.digest(inputBytes);
		if(digestBytes.length != 20)//160 bits from SHA-1
			//shouldn't get thrown, so we can make it a RuntimeException
			throw new RuntimeException("Hash Algorithm failed " + digestBytes.length);
		
		long result = 0;
		//we compress the 160 bits down to 32 bits
		//or, the 20 bytes down to 1 int
		for(int row = 0; row < 5; row++){//five "rows" of 32 bits to me anded in
			//basically we can split the 160 bits into 5 groups of 32 (thats why there are 5 iterations)
			//and we XOR all of them together (that's what ^= does) may consider +=
			//and we have to left shift to get each byte in the right part of the int
			
			result ^= digestBytes[4*row + 0] << 0;
			result ^= digestBytes[4*row + 1] << 8;
			result ^= digestBytes[4*row + 2] << 16;
			result ^= digestBytes[4*row + 3] << 24;
		}
		if (result<0)
			return (Math.abs(result) + (long)Math.pow(2, 31));
		else
			return result;		
	}
	
	// End of long operations
	//*****************************************************************************
	
	
	
	public static long convertHexToLongValue(String hexNumber) {
		long returnLong = 0;
		if (hexNumber.length() == 10) {
			returnLong = Integer.decode("0X" + hexNumber.substring(3, 10));
			long eigthHex = Integer.decode("0X" + hexNumber.charAt(2));
			eigthHex = eigthHex * 268435456;// Add the 8th number.
			returnLong += eigthHex;
		} else {
			try {
				returnLong = (long) Integer.decode(hexNumber);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return returnLong;
	}
	
	
}
