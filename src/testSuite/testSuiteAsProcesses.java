package testSuite;

import networkOverlay.*;

import java.io.*;
import java.net.InetAddress;
import java.util.Random;
import java.util.StringTokenizer;

public class testSuiteAsProcesses {
	private static Random random;
	private static long seed;
	private static int numProcesses;
	
	private static long[] longIDs;
	private static String[] hashedIDs;
	private static Process[] serviceProcesses;
	
	private static InputStream[] serviceInputStreams;
	private static OutputStream[] serviceOutputStreams;
	private static InputStream[] serviceErrorStreams;
	
	private static Thread[] readerThreads;
	private static Thread[] writerThreads;
	private static Thread[] errorThreads;
	
	private static String[] fileLocations;
	private static String[] fileKeywords;
	private static long[] fileIDs;
	
	//Keeps a tally of the amount of files that have finished being published and retracted
	private static int[] workCounter;
	
	//Keeps tally of all the randomly generated ports
	private static int[] ports;
	
	private static boolean verbose;
	
	public static void main(String args[]) throws Exception {
		while(true) {
			checkArgs(args);
			checkVerbose(args);
			createRandom(args);
			createFilesToPublish();
			createServicesProcesses();
			forceFingerTableUpdate();
			addShutdownHook();
			publishFiles();
			//We want to wait for the files to finish publishing before trying to retract them.
			//Otherwise, we get errors saying that the key doesn't exist
			waitWhilePublishing();
			associateKeywords();
			waitWhileAssociating();
			terminateSomeProcesses();
			lookupAndDownloadFiles();
			waitWhileDownloading();
			retractFiles();
			terminateServicesProcesses();
			waitForPortsToClean();
		}
	}
	
	//Make sure that a seed has been entered by the user
	private static void checkArgs(String args[]) {
		if(args.length < 1) {
			System.out.println("The program expects a seed as an argument. For example: java TestSuite 100");
			System.exit(-1);
		}
	}
	
	private static void checkVerbose(String args[]) {
		try {
			if(args[1].toLowerCase().equals("verbose")) {
				verbose = true;
			}
		}
		catch(Exception e) {
			
		}
	}
	
	//Terminate all child processes when the test suite is shut down
	//This doesn't work with the terminate button in eclipse though
	//See https://bugs.eclipse.org/bugs/show_bug.cgi?id=38016 as it's a problem with eclipse
	private static void addShutdownHook() {
		System.out.println("[API] Creating shutdown hook");
		Runtime.getRuntime().addShutdownHook(new Thread() {
		    public void run() {
				for(Process process : serviceProcesses) {
					process.destroy();
				}
		    }
		});
	}
	
	//Create our pseudo-random generator based on the inputed seed
	private static void createRandom(String args[]) {
		
		random = new Random(seed);
		//We want to create a random amount of processes, between 5 and 15
		numProcesses = random.nextInt(11) + 5;
		
		System.out.println("[API] The API is going to create " + numProcesses + " different servers.");
		
		ports = new int[numProcesses];
		seed = Long.parseLong(args[0]);
		
		//The default port is 7456 and is written in servers.txt so we initially create one with this port number
		//and all subsequent servers will connect to this server
		int port = 7456;
		ports[0] = port;
		//Create all the random ports
		for(int count = 1; count < numProcesses; count++) {
			do {
				//We want our port to be higher than 7000 just to be safe
				port = random.nextInt(100) + 7000;
			}
			while(searchArray(ports, port) >= 0);
			ports[count] = port;
		}
	}
	
	private static void createServicesProcesses() throws IOException, InterruptedException {
		longIDs					= new long[numProcesses];
		hashedIDs				= new String[numProcesses];
		
		serviceProcesses		= new Process[numProcesses];
		serviceInputStreams		= new InputStream[numProcesses];
		serviceOutputStreams	= new OutputStream[numProcesses];
		serviceErrorStreams		= new InputStream[numProcesses];
		
		readerThreads			= new Thread[numProcesses];
		writerThreads			= new Thread[numProcesses];
		errorThreads			= new Thread[numProcesses];
		
		//The first counter makes sure all servers have finished being created
		//The second counter keeps track that all files have finished publishing
		//The third counter keeps track that all downloads have finished
		//The fourth counter keeps track that all finles have finished being retracted
		//The fifth count keeps track of if a server has finished being initialized
		//The sixth counter keeps track of all the keywords that have finish being associated
		//The seventh counter keeps track of all the files that have finished downloading
		//The seventh counter keeps track of how many files have finished being looked up
		workCounter				= new int[8];
		workCounter[0]			= 0;
		workCounter[1]			= 0;
		workCounter[2]			= 0;
		workCounter[3]			= 0;
		workCounter[4]			= -1;
		workCounter[5]			= 0;
		workCounter[6]			= 0;
		workCounter[7]			= 0;
		
		Runtime runtime = Runtime.getRuntime(); 
		
		for(int count = 0; count < numProcesses; count++) {
			longIDs[count] = Hasher.hashPeerLong(InetAddress.getLocalHost(), ports[count]);
			hashedIDs[count] = Long.toHexString(longIDs[count]);
			
			System.out.println("[API] Creating server " + hashedIDs[count] + " with port " + ports[count]);
			
			serviceProcesses[count] = runtime.exec("java networkOverlay.Services TEST");
			serviceInputStreams[count] = serviceProcesses[count].getInputStream();
			serviceOutputStreams[count] = serviceProcesses[count].getOutputStream();
			serviceErrorStreams[count] = serviceProcesses[count].getErrorStream();
			serviceOutputStreams[count].write((ports[count] + "\n").getBytes());
			serviceOutputStreams[count].flush();
			
			readerThreads[count] = new ReaderThread(serviceInputStreams[count], "Stdout", hashedIDs[count], hashedIDs, 
				fileLocations, fileKeywords, fileIDs, workCounter, verbose);
			readerThreads[count].start();
			
			//Create threads to display any errors
			errorThreads[count] = new ReaderThread(serviceErrorStreams[count], "Stderr", hashedIDs[count], hashedIDs, 
				fileLocations, fileKeywords, fileIDs, workCounter, verbose);
			errorThreads[count].start();
			
			waitWhileInitializing(count);
		}
	}
	
	private static void waitWhileInitializing(int position) throws InterruptedException {
		while(workCounter[4] < position) {
			Thread.sleep(100);
		}
	}
	
	private static void forceFingerTableUpdate() {
		for(int count = 0; count < numProcesses; count++) {
			writerThreads[count] = new WriterThread(serviceOutputStreams[count], "Update", random);
			writerThreads[count].start();
		}
	}
	
	private static void createFilesToPublish() throws InterruptedException {
		fileLocations = new String[numProcesses];
		fileKeywords = new String[numProcesses];
		fileIDs = new long[numProcesses];
		File directory = new File("published_files");
		if(directory.exists() == false) {
			directory.mkdir();
		}
		
		for(int count = 0; count < numProcesses; count++) {
			File file = new File("published_files/" + count + ".txt");
			BufferedWriter writer;
			
			if(file.exists()) {
				file.delete();
			}
			try {
				file.createNewFile();
			}
			catch (IOException e) {
				System.out.println("[API] Failed to create file.");
			}
			try {
				writer = new BufferedWriter(new FileWriter("published_files/" + count + ".txt"));
				writer.write(count + "");
				writer.close();
			}
			catch (IOException e) {
				System.out.println("[API] Failed to write to file.");
			}
			
			fileLocations[count] = file.toString();
			fileKeywords[count] = file.getName();
			fileIDs[count] = new FileInfo(file).getID();
			
			Thread.sleep(random.nextInt(10) + 10);
		}
	}
	
	private static void publishFiles() throws IOException, InterruptedException {
		for(int count = 0; count < numProcesses; count++) {
			writerThreads[count] = new WriterThread(serviceOutputStreams[count], "Publish " + fileLocations[count], random);
			writerThreads[count].start();
		}
	}
	
	private static void waitWhilePublishing() throws InterruptedException {
		
		int count = 0;
		while(workCounter[1] != numProcesses) {
			Thread.sleep(100);
			count++;
			
			if(count == 100) {
				System.out.println("[API] Timed out while waiting for files to finish publishing. " + 
					"Assuming that publishing died and continuing.");
				//numProcesses = workCounter[1];
				return;
			}
		}
	}
	
	private static void associateKeywords() throws IOException, InterruptedException {
		for(int count = 0; count < numProcesses; count++) {
			writerThreads[count] = new WriterThread(serviceOutputStreams[count], "Associate " + fileLocations[count] + " " + 
				fileKeywords[count], random);
			writerThreads[count].start();
		}
	}
	
	private static void waitWhileAssociating() throws InterruptedException {
		int count = 0;
		while(workCounter[5] != numProcesses) {
			Thread.sleep(100);
			count++;
			
			if(count == 100) {
				System.out.println("[API] Timed out while waiting for keywords to finish publishing. " + 
					"Assuming that keyword association died and continuing.");
				//numProcesses = workCounter[5];
				return;
			}
		}
	}
	
	//Gracefully terminate the last peer created
	private static void terminateSomeProcesses() throws InterruptedException {
		System.out.println("[API] Gracefully terminating " + hashedIDs[numProcesses-1]);
		gracefullyTerminate(numProcesses-1, random);
		
		//Wait 2 seconds for the network to fix itself
		Thread.sleep(2000);
		
		System.out.println("[API] Non-gracefully terminating " + hashedIDs[numProcesses-2]);
		nonGracefullyTerminate(0);
		
		//Wait 2 seconds for the network to fix itself
		Thread.sleep(2000);
	}
	
	//Terminate one process gracefully and one non-gracefully
//	private static void terminateSomeProcesses() throws InterruptedException {
//		//Boolean doneNonGraceful = false;
//		Boolean doneGraceful = false;
//		
//		for(int count = 0; count < numProcesses; count++) {
//			if(/*doneNonGraceful && */doneGraceful) {
//				//Give the network some time to recover
//				Thread.sleep(1000);
//				return;
//			}
//			else {
//				if(!doneGraceful) {
//					Boolean doGraceful = (random.nextInt(2) != 0);
//					
//					if(doGraceful) {
//						System.out.println("[API] Gracefully shutting down " + hashedIDs[count]);
//						gracefullyTerminate(count, random);
//						doneGraceful = true;
//					}
//				}
////				if(!doneNonGraceful) {
////					Boolean doNonGraceful = (random.nextInt(2) != 0);
////					
////					if(doNonGraceful) {
////						System.out.println("[API] Non-gracefully shutting down " + hashedIDs[count]);
////						nonGracefullyTerminate(count);
////						doneNonGraceful = true;
////					}
////				}
//			}
//		}
//	}
	
	private static void lookupAndDownloadFiles() throws IOException, InterruptedException {
		
		File directory = new File("downloaded_files");
		if(directory.exists() == false) {
			directory.mkdir();
		}
		
		for(int count = 1; count < numProcesses-1; count++) {
			int randomPosition = random.nextInt(numProcesses);
			FileInfo fileInfo = new FileInfo(new File(fileLocations[randomPosition]));
			
			writerThreads[count] = new WriterThread(serviceOutputStreams[count], "Lookup " + fileKeywords[randomPosition], random);
				writerThreads[count].start();
				
			waitWhileLookup(count);
			
			writerThreads[count] = new WriterThread(serviceOutputStreams[count], "Download " + fileInfo.getID() + 
				" " + longIDs[randomPosition] + " downloaded_files/" + fileInfo.getName(), random);
			writerThreads[count].start();
		}
	}
	
	private static void waitWhileLookup(int position) throws InterruptedException {
		int count = 0;
		while(workCounter[7] < position) {
			Thread.sleep(100);
			count++;
			
			if(count == 100) {
				System.out.println("[API] Timed out while waiting for lookup to finish. Assuming that somthing catastrophic happened and " +
					"continuing with the test suite.");
				//numProcesses = workCounter[7];
				return;
			}
		}
	}
	
	private static void waitWhileDownloading() throws InterruptedException {
		int count = 0;
		while(workCounter[6] != numProcesses-2) {
			Thread.sleep(100);
			count++;
			
			if(count == 100) {
				System.out.println("[API] Timed out while waiting for files to finish downloading. " + 
					"Assuming that either peers ungracefully left and the downloads failed, or something catastrophic happened " + 
					"and continuing with the test suite.");
				//numProcesses = workCounter[6];
				return;
			}
		}
	}
	
	private static void retractFiles() throws IOException, InterruptedException {
		for(int count = 1; count < numProcesses-1; count++) {
			writerThreads[count] = new WriterThread(serviceOutputStreams[count], "Retract " + fileLocations[count], random);
			writerThreads[count].start();
		}
	}
	
//	private static void displayMessages() throws IOException {
//		for(int count = 0; count < numProcesses; count++) {
//			
//			//Create threads to display any messages
//			readerThreads[count] = new ReaderThread(serviceInputStreams[count], "Stdout", hashedIDs[count], hashedIDs, 
//				fileLocations, fileIDs, workCounter);
//			readerThreads[count].start();
//			
//			//Create threads to display any errors
//			errorThreads[count] = new ReaderThread(serviceErrorStreams[count], "Stderr", hashedIDs[count], hashedIDs, 
//				fileLocations, fileIDs, workCounter);
//			errorThreads[count].start();
//		}
//	}
	
	//Terminate program once all files have been retracted
	private static void terminateServicesProcesses() throws InterruptedException {
		int timer = 0;
		
		while(true) {
			if(workCounter[3] == numProcesses-2) {
				for(int count = 0; count < numProcesses; count++) {
					nonGracefullyTerminate(count);
				}
				
				//Wait for the OS to free up the port
				Thread.sleep(1000);
				break;
			}
			else {
				Thread.sleep(100);
				
			}
			if(timer == 100) {
				workCounter[3] = numProcesses-2;
				
				System.out.println("[API] Timed out while waiting for servers to finish their work. " + 
					"Assuming that something major went wrong.");
			}
			timer++;
		}
	}
	
	//Sleep while windows releases ports that the test suite was using
	private static void waitForPortsToClean() throws InterruptedException {
		System.out.println("[API] Sleeping for three seconds to let windows release ports that were in use. Hopefully this is long enough.");
		Thread.sleep(3000);
	}
	
	private static void gracefullyTerminate(int position, Random random) {
		writerThreads[position] = new WriterThread(serviceOutputStreams[position], "Exit", random);
		writerThreads[position].start();
	}
	
	private static void nonGracefullyTerminate(int position) {
		serviceProcesses[position].destroy();
	}
	
	public static int searchArray(Object[] haystack, Object needle) {
		for(int count = 0; count < haystack.length; count++) {
			if(haystack[count].equals(needle)) {
				return count;
			}
		}
		return -1;
	}
	
	public static int searchArray(int[] haystack, int needle) {
		for(int count = 0; count < haystack.length; count++) {
			if(haystack[count] == needle) {
				return count;
			}
		}
		return -1;
	}
	
	public static int searchArray(long[] haystack, long needle) {
		for(int count = 0; count < haystack.length; count++) {
			if(haystack[count] == needle) {
				return count;
			}
		}
		return -1;
	}
}

class WriterThread extends Thread {
	OutputStream input;
	String message;
	Random random;
	
	public WriterThread(OutputStream myInput, String myMessage, Random myRandom) {
		input = myInput;
		message = myMessage;
		random = myRandom;
	}
	
	public void run() {
		
		//Sleep for a random amount of time before sending the message
		try {
			Thread.sleep(random.nextInt(1000));
		} catch (InterruptedException e1) {
			System.out.println("[API] Thread failed to sleep.");
		}
		
		try {
			input.write((message + "\n").getBytes());
		} catch (IOException e) {
			System.out.println("[API] Failed to send message to child.");
		}
		
		try {
			input.flush();
		} catch (IOException e) {
			System.out.println("[API] Failed to force buffer to write to child.");
		}
	}
}

//Class used to create threads to display messages on screen
class ReaderThread extends Thread {
	InputStream output;
	String prefix;
	int[] messagesDone;
	String myHashedID;
	String[] hashedIDs;
	String[] fileLocations;
	String[] fileKeywords;
	long[] fileIDs;
	int[] workCounter;
	boolean verbose;
	
	public ReaderThread(InputStream myOutput, String myPrefix, String myMyHashedID, String[] myHashedIDs, String myFileLocations[], 
		String[] myFileKeywords, long[] myFileIDs, int[] myWorkCounter, boolean myVerbose) {
		output				= myOutput;
		prefix				= "[" + myPrefix + "] ";
		myHashedID			= myMyHashedID;
		hashedIDs			= myHashedIDs;
		fileLocations		= myFileLocations;
		fileKeywords		= myFileKeywords;
		fileIDs				= myFileIDs;
		workCounter			= myWorkCounter;
		verbose				= myVerbose;
	}
	
	public void run() {
		BufferedReader brCleanUp = new BufferedReader (new InputStreamReader (output));
		String line;
		try {
			while ((line = brCleanUp.readLine ()) != null) {
				//System.out.println (prefix + line);
				printLine(line);
				verifyInitializationCompletion(line);
				validatePublication(line);
				validateKeywordAssociation(line);
				verifyLookup(line);
				validateDownload(line);
				validateRetraction(line);
			}
		} catch (IOException e) {
			System.out.println("[API] Failed to display buffer to screen.");
		}

		try {
			brCleanUp.close();
		} catch (IOException e) {
			System.out.println("[API] Failed to close buffer.");
		}
	}
	
	private void printLine(String line) {
		
		boolean print_line = false;
		
		if(verbose) {
			print_line = true;
		}
		else {
			if(prefix.equals("[Stderr] "))
				print_line = true;
			else
			{
				if(line.indexOf("Creating services instance") > -1 || line.indexOf("My own hashed identifier:") > -1 || 
					line.indexOf("Finished setting up peer:") > -1 || line.indexOf("publishing file:") > -1 || line.indexOf("published file:") > -1 ||
					line.indexOf("associating keyword:") > -1 || line.indexOf("Associated keyword:") > -1 || line.indexOf("retracting file:") > -1 || 
					line.indexOf("retracted file:") > -1 || line.indexOf("File lookup") > -1 || line.indexOf("File download") > -1)
				{
					print_line = true;
				}
			}
		}
		
		if(print_line)
			System.out.println(prefix + line);
	}
	
	private void verifyInitializationCompletion(String line) {
		if(line.indexOf("Finished setting up peer:") > -1) {
			
			StringTokenizer token = new StringTokenizer(line);
			
			//Don't need the first four tokens
			token.nextToken();
			token.nextToken();
			token.nextToken();
			token.nextToken();
			
			System.out.println("[API] " + Long.toHexString(Long.parseLong(token.nextToken())) + " has finished being initialized.");
			workCounter[4]++;
			System.out.println("[API] Initialization counter: " + workCounter[4]);
		}
	}
	
	private void validateKeywordAssociation(String line) {
		if(line.indexOf("Associated keyword:") > -1) {
			
			StringTokenizer token = new StringTokenizer(line);
			
			//Don't need the first two tokens
			token.nextToken();
			token.nextToken();
			
			String keyword = token.nextToken();
			
			//Don't neet the next two tokens
			token.nextToken();
			token.nextToken();
			
			String filePath = token.nextToken();
			
			int keywordArrayPosition	= testSuiteAsProcesses.searchArray(fileKeywords, keyword);
			int filePathArrayPosition	= testSuiteAsProcesses.searchArray(fileLocations, filePath);
			
			if(keywordArrayPosition == filePathArrayPosition)
				System.out.println("[API] Finished associating keyword " + keyword + " with file " + filePath + 
					". The test suite verified that this is correct.");
			else if(keywordArrayPosition != -1)
				System.out.println("[API] Associated keyword " + keyword + " with file " + filePath + 
					" but it should have been associated with file " + fileLocations[keywordArrayPosition]);
			else
				System.out.println("[API] Associated keyword " + keyword + " with file " + filePath + 
					" but can't verify if this is correct as the keyword was not found in the test suite's keyword database.");
			workCounter[5]++;
			System.out.println("[API] Association counter: " + workCounter[5]);
		}
	}
	
	private void verifyLookup(String line) {
		if(line.indexOf("File lookup") > -1)
		{
			workCounter[7]++;
			System.out.println("Lookup counter: " + workCounter[7]);
		}
	}
	
	private void validateDownload(String line) {
		if(line.indexOf("File download") > -1) {
			System.out.println("[API] Starting download validation for " + myHashedID);
			
			StringTokenizer token = new StringTokenizer(line);
			
			//int hashedIDArrayPosition = testSuiteAsProcesses.searchArray(hashedIDs, myHashedID);
			
			//Don't need the first two tokens
			token.nextToken();
			token.nextToken();
			
			String status = token.nextToken();
			
			if(status.equals("failed.")) {
				//Don't need the next 8 tokens
				for(int count = 0; count < 8; count++) {
					token.nextToken();
				}
				
				long fileID	= Long.parseLong(token.nextToken());
				
				System.out.println("[API] " + myHashedID + " tried to the file with file ID " + fileID + " but the download failed.");
			}
			else {
				//Don't need the next 7 tokens
				for(int count = 0; count < 7; count++) {
					token.nextToken();
				}
				
				long fileID				= Long.parseLong(token.nextToken());
				int fileIDArrayPosition = testSuiteAsProcesses.searchArray(fileIDs, fileID);
				
				//Don't need the next token
				token.nextToken();
				
				String saveFileLocation = token.nextToken();
				
				//Don't need the next token
				token.nextToken();
				
				String downloadedFrom = Long.toHexString(Long.parseLong(token.nextToken()));
				
				if(fileIDArrayPosition == -1) {
					System.out.println("[API] " + myHashedID + " downloaded the file with file ID " + fileID + 
						" from " + downloadedFrom + ". The code reported that the download was successful, " + 
						"but the test suite doesn't have any record of this file.");
				}
				else {
					String originalFileLocation = fileLocations[fileIDArrayPosition];
					int downloadedFromPosition	= testSuiteAsProcesses.searchArray(hashedIDs, downloadedFrom);
					
					File originalFile	= new File(originalFileLocation);
					File savedFile		= new File(saveFileLocation);
					
					if(downloadedFromPosition == -1) {
						System.out.println("[API] " + myHashedID + " downloaded the file with file ID " + fileID + 
							" from " + downloadedFrom + ". The code reported that the download was successful, " + 
							"but the test suite doesn't have any record of this peer.");
					}
					else {
						BufferedReader originalFileBuffer;
						String originalFileContent = "";
						String read;
						
						try {
							originalFileBuffer = new BufferedReader(new FileReader(originalFile));							
							try {
								while((read = originalFileBuffer.readLine()) != null) {
									originalFileContent += read;
								}
							} catch (IOException e) {
								System.out.println("[API] Failed to read original file's content.");
							}
						} catch (FileNotFoundException e) {
							System.out.println("[API] Failed to read contents of the original file.");
						}
						
						BufferedReader savedFileBuffer;
						String savedFileContent = "";
						read = "";
						try {
							savedFileBuffer = new BufferedReader(new FileReader(savedFile));
							try {
								while((read = savedFileBuffer.readLine()) != null) {
									savedFileContent += read;
								}
							} catch (IOException e) {
								System.out.println("[API] Failed to read original file's content.");
							}
						} catch (FileNotFoundException e) {
							System.out.println("[API] Failed to read contents of the original file.");
						}
						
						if(originalFileContent.equals(savedFileContent)) {
							System.out.println("[API] " + myHashedID + " downloaded the file with file ID " + fileID + 
								" from " + downloadedFrom + ". The download was successful and both files have the same content.");
						} else {
							System.out.println("[API] " + myHashedID + " downloaded the file with file ID " + fileID + 
									" from " + downloadedFrom + ". The download was successful but both files don't have the same content.");
						}
					}
				}
			}
			workCounter[6]++;
			System.out.println("[API] Download counter: " + workCounter[6]);
		}
	}
	
	private void validatePublication(String line) {
		if(line.indexOf("published file:") > -1) {
			
			System.out.println("[API] Starting publication validation for " + myHashedID);
			
			StringTokenizer token = new StringTokenizer(line);
			
			String hashedID = token.nextToken();
			
			//Don't need the next two tokens
			token.nextToken();
			token.nextToken();
			
			String fileLocation = token.nextToken();
			
			//Don't need the next three tokens
			token.nextToken();
			token.nextToken();
			token.nextToken();
			
			long fileID = Long.parseLong(token.nextToken());
			
			int hashedIDArrayPosition		= testSuiteAsProcesses.searchArray(hashedIDs, hashedID);
			int fileLocationArrayPosition	= testSuiteAsProcesses.searchArray(fileLocations, fileLocation);
			int fileIDArrayPosition			= testSuiteAsProcesses.searchArray(fileIDs, fileID);
			
			if(hashedIDArrayPosition < 0 || fileLocationArrayPosition < 0 || fileIDArrayPosition < 0) {
				if(hashedIDArrayPosition < 0) {
					System.out.println("[API] Failed to validate file publication. The server with id " + hashedID + 
						" doesn't actually exist.");
				}
				
				if(fileLocationArrayPosition < 0) {
					System.out.println("[API] Failed to validate file publication. The file " + fileLocation + " doesn't actually exist.");
				}
				
				if(fileIDArrayPosition < 0) {
					System.out.println("[API] Failed to validate file publication. A file with the id " + fileID + " doesn't actually exist.");
				}
			}
			else {
				if(hashedIDArrayPosition != fileLocationArrayPosition ||  hashedIDArrayPosition != fileIDArrayPosition || 
					fileLocationArrayPosition != fileIDArrayPosition) {
					if(hashedIDArrayPosition == fileLocationArrayPosition) {
						System.out.println("[API] " + hashedID + " published file " + fileLocation + " with file ID " + 
							fileIDs[hashedIDArrayPosition] + " but the file ID was incorrectly reported as " + fileID);
					}
					else if(hashedIDArrayPosition == fileIDArrayPosition) {
						System.out.println("[API] " + hashedID + " published file " + fileLocations[hashedIDArrayPosition] + 
							" with file ID " + fileID + " but the published file was incorrectly reported as " + fileLocation);
					}
					else if(fileLocationArrayPosition == fileIDArrayPosition) {
						System.out.println("[API] " + hashedIDs[fileLocationArrayPosition] + " was supposed to publish file " + 
							fileLocation + " with file ID " + fileID + " but it was reported that the file was published by " + hashedID);
					}
					else {
						System.out.println("[API] The file was not published correctly and there is no data correlation to " + 
							"accurately tell which file should have been published and by whom.");
					}
				}
				else {
					System.out.println("[API] After validation, the API has determined that " + hashedID + " published file " + 
						fileLocation + " with file ID " + fileID + " and that this is correct.");
				}
			}
			
			workCounter[1]++;
			
			System.out.println("[API] Publish counter: " + workCounter[1]);
		}
		else if (line.equals("Error. The file does not exist.")) {
			workCounter[1]++;
			System.out.println("[API] Publish counter: " + workCounter[1]);
		}
	}
	
	private void validateRetraction(String line) {
		if(line.indexOf("retracted file:") > -1) {
			
			System.out.println("[API] Starting retraction validation for " + myHashedID);
			
			StringTokenizer token = new StringTokenizer(line);
			
			String hashedID = token.nextToken();
			
			//Don't need the next two tokens
			token.nextToken();
			token.nextToken();
			
			String fileLocation = token.nextToken();
			
			//Don't need the next three tokens
			token.nextToken();
			token.nextToken();
			token.nextToken();
			
			long fileID = Long.parseLong(token.nextToken());
			
			int hashedIDArrayPosition		= testSuiteAsProcesses.searchArray(hashedIDs, hashedID);
			int fileLocationArrayPosition	= testSuiteAsProcesses.searchArray(fileLocations, fileLocation);
			int fileIDArrayPosition			= testSuiteAsProcesses.searchArray(fileIDs, fileID);
			
			if(hashedIDArrayPosition < 0 || fileLocationArrayPosition < 0 || fileIDArrayPosition < 0) {
				if(hashedIDArrayPosition < 0) {
					System.out.println("[API] Failed to validate file retraction. The server with id " + hashedID + 
						" doesn't actually exist.");
				}
				
				if(fileLocationArrayPosition < 0) {
					System.out.println("[API] Failed to validate file retraction. The file " + fileLocation + " doesn't actually exist.");
				}
				
				if(fileIDArrayPosition < 0) {
					System.out.println("[API] Failed to validate file retraction. A file with the id " + fileID + " doesn't actually exist.");
				}
			}
			else {
				if(hashedIDArrayPosition != fileLocationArrayPosition ||  hashedIDArrayPosition != fileIDArrayPosition || 
					fileLocationArrayPosition != fileIDArrayPosition) {
					if(hashedIDArrayPosition == fileLocationArrayPosition) {
						System.out.println("[API] " + hashedID + " retracted file " + fileLocation + " with file ID " + 
							fileIDs[hashedIDArrayPosition] + " but the file ID was incorrectly reported as " + fileID);
					}
					else if(hashedIDArrayPosition == fileIDArrayPosition) {
						System.out.println("[API] " + hashedID + " retracted file " + fileLocations[hashedIDArrayPosition] + 
							" with file ID " + fileID + " but the published file was incorrectly reported as " + fileLocation);
					}
					else if(fileLocationArrayPosition == fileIDArrayPosition) {
						System.out.println("[API] " + hashedIDs[fileLocationArrayPosition] + " was supposed to retract file " + 
							fileLocation + " with file ID " + fileID + " but it was reported that the file was retracted by " + hashedID);
					}
					else {
						System.out.println("[API] The file was not retracted correctly and there is no data correlation to " + 
							"accurately tell which file should have been retracted and by whom.");
					}
				}
				else {
					System.out.println("[API] After validation, the API has determined that " + hashedID + " retracted file " + 
						fileLocation + " with file ID " + fileID + " and that this is correct.");
				}
			}
			
			workCounter[3]++;
			System.out.println("[API] Retract counter: " + workCounter[3]);
		}
		else if (line.indexOf("failed to retract file.") > -1) {
			workCounter[3]++;
			System.out.println("[API] Retract counter: " + workCounter[3]);
		}
	}
}